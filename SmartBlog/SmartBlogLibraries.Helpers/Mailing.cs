using System;
using System.Net.Mail;
using Umbraco.Core.Logging;

namespace SmartBlogLibraries.Helpers
{
	public static class Mailing
	{
		public static void SendEmail(string toAddress, string fromAddress, string subject, string body, string replyToAddress)
		{
			SmtpClient smtpClient = new SmtpClient();
			MailMessage mailMessage = new MailMessage(fromAddress, toAddress);
			mailMessage.ReplyToList.Add(replyToAddress);
			mailMessage.Subject = subject;
			mailMessage.Body = body;
			try
			{
				smtpClient.Send(mailMessage);
			}
			catch (Exception exception)
			{
				LogHelper.Error(typeof(bool), "SmartBlogLibraries has failed to send an email with subject: " + subject, exception);
			}
		}
	}
}
