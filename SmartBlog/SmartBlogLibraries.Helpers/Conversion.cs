using System;

namespace SmartBlogLibraries.Helpers
{
	public static class Conversion
	{
		public static T ToYesNo<T>(bool input)
		{
			if (typeof(T) == typeof(string))
			{
				if (input)
				{
					return (T)Convert.ChangeType("Yes", typeof(T));
				}
				return (T)Convert.ChangeType("No", typeof(T));
			}
			if (typeof(T) == typeof(char))
			{
				if (input)
				{
					return (T)Convert.ChangeType('y', typeof(T));
				}
				return (T)Convert.ChangeType('n', typeof(T));
			}
			if (typeof(T) == typeof(short) || typeof(T) == typeof(int) || typeof(T) == typeof(long))
			{
				if (input)
				{
					return (T)Convert.ChangeType(1, typeof(T));
				}
				return (T)Convert.ChangeType(0, typeof(T));
			}
			return default(T);
		}

		public static string TrySubString(string str, int start, int length, bool addEllipsis = true)
		{
			if (str.Length > start + length)
			{
				if (addEllipsis)
				{
					return str.Substring(start, length) + "...";
				}
				return str.Substring(start, length);
			}
			if (start < str.Length)
			{
				return str.Substring(start, str.Length - start);
			}
			return str;
		}
	}
}
