using System;
using System.Globalization;

namespace SmartBlogLibraries.Helpers
{
	public static class DateTime
	{
		public const string DateFormatNormal = "d MMM yyyy";

		public const string DateFormatNormalWithTime = "d MMM yyyy, HH:mm tt";

		public const string DateFormatShort = "d";

		public const string DateFormatShortWithTime = "g";

		public const string DateFormatShortWithTimeAndSeconds = "G";

		public const string DateFormatLong = "D";

		public const string DateFormatLongWithTime = "f";

		public const string DateFormatLongWithTimeAndSeconds = "F";

		public const string DateFormatOnlyTime = "t";

		public const string DateFormatOnlyTimeWithSeconds = "T";

		public const string DateFormatOnlyMonthAndYear = "y";

		public const string DateFormatOnlyMonthAndDay = "m";

		public static bool IsSameDay(System.DateTime datetime1, System.DateTime datetime2)
		{
			return datetime1.Date == datetime2.Date;
		}

		public static System.DateTime ParseTwitterTime(this string date)
		{
			return System.DateTime.ParseExact(date, "ddd MMM dd HH:mm:ss zzzz yyyy", CultureInfo.InvariantCulture);
		}
	}
}
