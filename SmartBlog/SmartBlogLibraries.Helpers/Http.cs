using System.Web;
using System.Web.SessionState;

namespace SmartBlogLibraries.Helpers
{
	public class Http
	{
		public static readonly string domain = HttpContext.Current.Request.ServerVariables["HTTP_HOST"];

		public static HttpSessionState session = HttpContext.Current.Session;

		public static string GetUrlWithDomainPrefix(string url)
		{
			if (url.StartsWith("/"))
			{
				url = url.Substring(1);
			}
			string text = $"http://{domain}/";
			if (url.StartsWith(text))
			{
				return url;
			}
			return text + url;
		}

		public static string ReplaceQueryString(string queryString, string value)
		{
			string text = "";
			string text2 = "?";
			if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString.ToString()))
			{
				text2 = "&";
			}
			if (text2 == "?")
			{
				return text2 + queryString + "=" + value;
			}
			if (string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[queryString]))
			{
				return "?" + HttpContext.Current.Request.QueryString.ToString() + text2 + queryString + "=" + value;
			}
			return "?" + HttpContext.Current.Request.QueryString.ToString().Replace(queryString + "=" + HttpContext.Current.Request.QueryString[queryString], queryString + "=" + value);
		}
	}
}
