using System;
using System.Collections.Generic;
using umbraco.BusinessLogic;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace SmartBlogLibraries.Helpers
{
	public static class Cms
	{
		public static readonly User author = new User(0);

		public static int? Id = UmbracoContext.Current.PageId;

		public static Dictionary<LogTypes, Action<string>> LogMessage = new Dictionary<LogTypes, Action<string>>
		{
			{
				LogTypes.Error,
				delegate(string message)
				{
					Log.Add(LogTypes.Error, User.GetUser(0), -1, message);
				}
			},
			{
				LogTypes.Debug,
				delegate(string message)
				{
					Log.Add(LogTypes.Debug, User.GetUser(0), -1, message);
				}
			},
			{
				LogTypes.Custom,
				delegate(string message)
				{
					Log.Add(LogTypes.Custom, User.GetUser(0), -1, message);
				}
			}
		};

		public static IContent CreateContent(string name, string documentTypeAlias, int parentId, Dictionary<string, object> properties, bool publish = false, int author = 0)
		{
			IContentService contentService = ApplicationContext.Current.Services.ContentService;
			IContent content = contentService.CreateContent(name, parentId, documentTypeAlias, author);
			foreach (string key in properties.Keys)
			{
				content.SetValue(key, properties[key]);
			}
			contentService.Save(content);
			if (publish)
			{
				contentService.PublishWithStatus(content);
			}
			return content;
		}

		public static bool DeleteContentNode(int intId)
		{
			try
			{
				IContentService contentService = ApplicationContext.Current.Services.ContentService;
				IContent byId = contentService.GetById(intId);
				contentService.UnPublish(byId);
				contentService.Delete(byId);
			}
			catch (Exception ex)
			{
				LogMessage[LogTypes.Error](ex.StackTrace);
				return false;
			}
			return true;
		}
	}
}
