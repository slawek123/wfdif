using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace SmartBlogLibraries.Helpers
{
	public static class Listing
	{
		private static readonly Random objRandom = new Random();

		private static readonly object objSyncLock = new object();

		public static int GetRandomNumber(int intMin, int intMax)
		{
			lock (objSyncLock)
			{
				return objRandom.Next(intMin, intMax);
			}
		}

		public static IPublishedContent GetNextNode(IPublishedContent objCurrent)
		{
			IEnumerable<IPublishedContent> nodeSiblings = GetNodeSiblings(objCurrent);
			int indexOf = GetIndexOf(nodeSiblings, objCurrent);
			if (indexOf == nodeSiblings.Count() - 1 || indexOf == -1 || indexOf == 0 || indexOf + 1 > nodeSiblings.Count())
			{
				return null;
			}
			return nodeSiblings.ElementAt(indexOf + 1);
		}

		public static IPublishedContent GetPreviousNode(IPublishedContent objCurrent)
		{
			IEnumerable<IPublishedContent> nodeSiblings = GetNodeSiblings(objCurrent);
			int indexOf = GetIndexOf(nodeSiblings, objCurrent);
			if (indexOf == nodeSiblings.Count() + 1 || indexOf == -1 || indexOf == 0 || indexOf - 1 < nodeSiblings.Count())
			{
				return null;
			}
			return nodeSiblings.ElementAt(indexOf - 1);
		}

		public static IEnumerable<IPublishedContent> GetNodeSiblings(IPublishedContent objCurrent, bool blnSameDoctype = true)
		{
			if (blnSameDoctype)
			{
				return objCurrent.Parent.Descendants(objCurrent.DocumentTypeAlias);
			}
			return objCurrent.Parent.Descendants();
		}

		public static int GetIndexOf(IEnumerable<IPublishedContent> colSiblings, IPublishedContent objCurrent)
		{
			try
			{
				for (int i = 0; i < colSiblings.Count(); i++)
				{
					if (colSiblings.ElementAt(i).Id == objCurrent.Id)
					{
						return i;
					}
				}
			}
			catch (Exception)
			{
			}
			return -1;
		}

		public static decimal NumberOfPages(int intNumberOfItems, int intItemsPerPage)
		{
			if (intNumberOfItems % intItemsPerPage != 0)
			{
				return Math.Ceiling((decimal)(intNumberOfItems / intItemsPerPage)) + 1m;
			}
			return Math.Ceiling((decimal)(intNumberOfItems / intItemsPerPage));
		}

		public static HtmlString RenderPaging(int intCurrentPage, int intTotalItems, int intItemsPerPage, int intPagesToShow = 7, string strClass = "", bool blnShowEllipsis = true, bool blnPagingSummary = true)
		{
			string text = "";
			if (intPagesToShow % 2 == 0)
			{
				intPagesToShow++;
			}
			int num = (intTotalItems + intItemsPerPage - 1) / intItemsPerPage;
			int num2 = (intPagesToShow - 1) / 2;
			if (num < intPagesToShow)
			{
				intPagesToShow = num;
			}
			int num3;
			if (intCurrentPage <= num2)
			{
				num3 = 1;
			}
			else
			{
				num3 = intCurrentPage - num2;
				if (intCurrentPage >= num - num2)
				{
					num3 -= num2 - (num - intCurrentPage);
					num3 = Math.Max(num3, 1);
				}
			}
			int num4;
			if (intCurrentPage >= num - num2)
			{
				num4 = num;
			}
			else
			{
				num4 = intCurrentPage + num2;
				if (intCurrentPage <= num2)
				{
					num4 += num2 - intCurrentPage + 1;
					num4 = Math.Min(num4, num);
				}
			}
			if (blnPagingSummary)
			{
				object obj = text;
				text = string.Concat(obj, "<span class=\"pagingSummary\">There are ", intTotalItems, " items on ", num, " pages.</span>");
			}
			text = text + "<nav class=\"pagination " + strClass + "\">";
			if (intCurrentPage == num3)
			{
				text += "<span class=\"pageFirst\">First</span>";
				text += "<span class=\"pagePrev\">&lt;</span>";
			}
			else
			{
				text += "<a href=\"?p=1\" class=\"pageFirst\">First</a>";
				object obj2 = text;
				text = string.Concat(obj2, "<a href=\"?p=", intCurrentPage - 1, "\" class=\"pagePrev\">&lt;</a>");
			}
			for (int i = 0; i < intPagesToShow; i++)
			{
				int num5 = i + num3;
				if (blnShowEllipsis)
				{
					if (num5 == num3 && num3 != 1)
					{
						num5 = 0;
					}
					else if (num5 == num4 && num4 != num)
					{
						num5 = 0;
					}
				}
				if (num5 == 0)
				{
					text += "<span class=\"pageItem\">&hellip;</span>";
				}
				else if (num5 == intCurrentPage)
				{
					object obj3 = text;
					text = string.Concat(obj3, "<span class=\"pageItem pageCurrent\">", num5, "</span>");
				}
				else
				{
					object obj4 = text;
					text = string.Concat(obj4, "<a href=\"?p=", num5, "\" title=\"\" class=\"pageItem\">", num5, "</a>");
				}
			}
			if (intCurrentPage == num4)
			{
				text += "<span class=\"pageNext\">&gt;</span>";
				text += "<span class=\"pageLast\">Last</span>";
			}
			else
			{
				object obj5 = text;
				text = string.Concat(obj5, "<a href=\"?p=", intCurrentPage + 1, "\" class=\"pageNext\">&gt;</a>");
				object obj6 = text;
				text = string.Concat(obj6, "<a href=\"?p=", num, "\" class=\"pageLast\">Last</a>");
			}
			text += "</nav>";
			return new HtmlString(text);
		}

		public static List<SelectListItem> CreateSelectList(string strCsvItems, bool blnIsIntegerValues)
		{
			return CreateSelectList(strCsvItems.Split(','), blnIsIntegerValues);
		}

		public static List<SelectListItem> CreateSelectList(string[] colItems, bool blnIsIntegerValues)
		{
			List<SelectListItem> list = new List<SelectListItem>();
			for (int i = 0; i < colItems.Length; i++)
			{
				list.Add(new SelectListItem
				{
					Text = colItems[i],
					Value = (blnIsIntegerValues ? i.ToString() : colItems[i])
				});
			}
			return list;
		}
	}
}
