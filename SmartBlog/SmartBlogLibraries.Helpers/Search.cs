using Examine;
using Examine.LuceneEngine.SearchCriteria;
using Examine.Providers;
using Examine.SearchCriteria;
using System.Collections.Generic;
using System.Web;

namespace SmartBlogLibraries.Helpers
{
	public static class Search
	{
		public static string GetSearchTerm()
		{
			if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["q"]))
			{
				return HttpContext.Current.Request.QueryString["q"];
			}
			return string.Empty;
		}

		public static ISearchResults SearchSite()
		{
			return ExamineManager.Instance.Search(GetSearchTerm(), useWildcards: true);
		}

		public static ISearchResults SearchSite(string searchTerm)
		{
			return ExamineManager.Instance.Search(searchTerm, useWildcards: true);
		}

		public static ISearchResults SearchSite(string searchTerm, string IndexSet)
		{
			return ExamineManager.Instance.SearchProviderCollection[IndexSet].Search(searchTerm, useWildcards: true);
		}

		public static ISearchResults SearchSite(string searchTerm, string[] searchFields, string collectionName, string indexType = "content", bool debug = false)
		{
			BaseSearchProvider baseSearchProvider = ExamineManager.Instance.SearchProviderCollection[collectionName];
			ISearchCriteria searchCriteria = baseSearchProvider.CreateSearchCriteria(indexType);
			ISearchCriteria searchParams = searchCriteria.GroupedOr(searchFields, searchTerm).Compile();
			return baseSearchProvider.Search(searchParams);
		}

		public static ISearchResults SearchSite(ISearchCriteria filter, string collectionName, bool debug = false)
		{
			BaseSearchProvider baseSearchProvider = ExamineManager.Instance.SearchProviderCollection[collectionName];
			return baseSearchProvider.Search(filter);
		}

		public static ISearchResults SearchBlog(out ISearchCriteria filter, out string searchTerm)
		{
			searchTerm = HttpContext.Current.Request.QueryString["q"];
			new List<SearchResult>();
			BaseSearchProvider baseSearchProvider = ExamineManager.Instance.SearchProviderCollection["SmartBlogSearcher"];
			ISearchCriteria searchCriteria = baseSearchProvider.CreateSearchCriteria("content");
			string[] fields = new string[3]
			{
				"title",
				"summary",
				"body"
			};
			filter = searchCriteria.GroupedOr(fields, searchTerm.MultipleCharacterWildcard(), searchTerm.Fuzzy()).Compile();
			return baseSearchProvider.Search(filter);
		}
	}
}
