using System.ComponentModel.DataAnnotations;

namespace SmartBlogLibraries.Models
{
	public class SmartBlogCommentFormModel
	{
		[DataType(DataType.Text)]
		[Display(Name = "Name")]
		[Required(ErrorMessage = "Name is required")]
		[RegularExpression("^((?!Name).)*$", ErrorMessage = "Name is required")]
		public string Name
		{
			get;
			set;
		}

		[Required(ErrorMessage = "Email is required")]
		[RegularExpression("^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\\-+)|([A-Za-z0-9]+\\.+)|([A-Za-z0-9]+\\++))*[A-Za-z0-9]+@((\\w+\\-+)|(\\w+\\.))*\\w{1,63}\\.[a-zA-Z]{2,6}$", ErrorMessage = "Invalid Email Address")]
		[DataType(DataType.Text)]
		[Display(Name = "Email")]
		public string Email
		{
			get;
			set;
		}

		[Display(Name = "Website")]
		[DataType(DataType.Text)]
		public string Website
		{
			get;
			set;
		}

		[Display(Name = "Comment")]
		[DataType(DataType.MultilineText)]
		[Required(ErrorMessage = "Comment is required")]
		[RegularExpression("^(((?!Your Comment).|(Your Comment.).|\\n)*)$", ErrorMessage = "Comment is required")]
		public string Comment
		{
			get;
			set;
		}

		[Display(Name = "Security Question")]
		[RegularExpression("^(4)*$", ErrorMessage = "Invalid security answer")]
		[DataType(DataType.Text)]
		[Required(ErrorMessage = "Security question is required")]
		public string SecurityQuestion
		{
			get;
			set;
		}
	}
}
