using SmartBlogLibraries.Helpers;
using SmartBlogLibraries.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Umbraco.Web.Mvc;

namespace SmartBlogLibraries.Controllers
{
	public class CommentFormSurfaceController : SurfaceController
	{
		[HttpPost]
		[Obsolete("New SmartBlog comment forms are posted via ajax, this should not be used!", false)]
		public ActionResult SubmitSmartBlogComment(SmartBlogCommentFormModel model)
		{
			if (!base.ModelState.IsValid)
			{
				return CurrentUmbracoPage();
			}
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("name", model.Name);
			dictionary.Add("email", model.Email);
			dictionary.Add("website", base.Server.HtmlEncode(model.Website));
			dictionary.Add("message", base.Server.HtmlEncode(model.Comment));
			Dictionary<string, object> properties = dictionary;
			Cms.CreateContent(model.Name, "Comment", base.CurrentPage.Id, properties);
			XmlDocument config = Global.GetConfig();
			if (!string.IsNullOrEmpty(config.GetElementsByTagName("moderatorCommentEmail")[0].InnerText))
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.AppendLine("This is an automated message, please do not reply.");
				stringBuilder.AppendLine();
				stringBuilder.AppendLine("---------------------------------------------------");
				stringBuilder.AppendLine();
				stringBuilder.AppendLine("Text is as follows:");
				stringBuilder.AppendLine();
				stringBuilder.AppendLine("Name: " + model.Name);
				stringBuilder.AppendLine("Email: " + model.Email);
				stringBuilder.AppendLine("Website: " + model.Website);
				stringBuilder.AppendLine("Comment: " + model.Comment);
				stringBuilder.AppendLine();
				stringBuilder.AppendLine("Regards,");
				stringBuilder.AppendLine("Support");
				Mailing.SendEmail(config.GetElementsByTagName("moderatorCommentEmail")[0].InnerText, "support@" + Http.domain, "New Comment - " + System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"], stringBuilder.ToString(), model.Email);
			}
			base.TempData.Add("SubmissionMessage", "Your comment was successfully added.");
			return RedirectToCurrentUmbracoPage();
		}
	}
}
