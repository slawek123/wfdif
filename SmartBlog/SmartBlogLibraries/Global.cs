using System;
using System.Xml;
using Umbraco.Web;

namespace SmartBlogLibraries
{
	public static class Global
	{
		public static UmbracoHelper objUmbHelper = new UmbracoHelper(UmbracoContext.Current);

		private static XmlDocument objConfig = null;

		public static XmlDocument GetConfig()
		{
			if (objConfig == null)
			{
				objConfig = new XmlDocument();
				objConfig.Load(AppDomain.CurrentDomain.BaseDirectory + "/config/SmartBlog.config");
			}
			return objConfig;
		}
	}
}
