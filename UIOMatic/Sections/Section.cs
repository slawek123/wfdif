﻿using umbraco.businesslogic;
using umbraco.interfaces;

namespace UIOMatic.Sections
{
    [Application(Constants.ApplicationAlias, "IP Geo Filter", "icon-globe-europe---africa large", 15)]
    public class Section : IApplication { }

}