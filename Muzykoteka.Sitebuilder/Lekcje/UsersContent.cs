﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder.Lekcje
{
    [DocumentType(
             IconUrl = "icon-users-alt",
        AllowedTemplates = new string[] { "Uzytkownicy" },
             AllowedChildNodeTypes = new Type[] {
                 typeof(Users),
                 typeof(NaukaArtykul),
                 typeof(Film),
             }
       )]
    class Uzytkownicy : AbstractPage
    {

    }
    [DocumentType(
             IconUrl = "icon-umb-users",
             AllowedTemplates = new string[] { "Kategoria" },
        DefaultTemplate = "Kategoria",
             AllowedChildNodeTypes = new Type[] {
                 typeof(Lekcja),
             }
       )]
    class Users : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.MemberPicker, Name = "Użytkownik", Tab = TabNames.Dane)]
        public string UserId { get; set; }
    }
    [DocumentType(
             IconUrl = "icon-umb-users",
        DefaultTemplate = "Kategoria",
             AllowedTemplates = new string[] { "Kategoria" },
             AllowedChildNodeTypes = new Type[] {
                 typeof(Uczniowie),
             }
       )]
    class Lekcja : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Nazwa Lekcji", Tab = TabNames.Dane)]
        public string NazwaLekcji { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Artykuły", Tab = TabNames.Dane)]
        public string Artykuly { get; set; }
    }
    [DocumentType(
             IconUrl = "icon-umb-users",
        DefaultTemplate = "Kategoria",
             AllowedTemplates = new string[] { "Kategoria" }
       )]
    class Uczniowie : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "E-mail", Tab = TabNames.Dane)]
        public string Email { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Code", Tab = TabNames.Dane)]
        public string Code { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Status", Tab = TabNames.Dane)]
        public string Status { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.DatePickerWithTime, Name = "Data Ważności", Tab = TabNames.Dane)]
        public string DataWaznosc { get; set; }
    }
}
