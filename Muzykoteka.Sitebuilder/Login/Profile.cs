﻿using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
     IconUrl = "icon-folder",
     AllowedTemplates = new string[] { "Profile" }
)]
    public class Profile : DocumentTypeBase
    {

    }
}
