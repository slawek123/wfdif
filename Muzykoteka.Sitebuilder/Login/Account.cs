﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
     IconUrl = "icon-folder",
     AllowedTemplates = new string[] { "Acount" },
          AllowedChildNodeTypes = new Type[] {
              typeof(Login),
              typeof(Logout),
              typeof(LoginError),
              typeof(LoginCallback),
              typeof(DeleteAccount),
              typeof(ForgotPassword),
              typeof(Register),
              typeof(RegisterExternal),
              typeof(RestPassword),
              typeof(Profile),
              typeof(ChangePassword)
          }
    )]
    public class Account : DocumentTypeBase
    {

    }
}
