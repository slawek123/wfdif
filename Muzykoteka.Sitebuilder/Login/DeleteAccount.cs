﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
     IconUrl = "icon-folder",
     AllowedTemplates = new string[] { "DeleteAccount" }
)]
    public class DeleteAccount : DocumentTypeBase
    {
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Tresc", Mandatory = true, Tab = TabNames.Dane)]
        public string tresc { get; set; }

    }
}
