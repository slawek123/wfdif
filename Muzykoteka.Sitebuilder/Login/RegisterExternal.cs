﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
     IconUrl = "icon-folder",
     AllowedTemplates = new string[] { "RegisterExternal" }
)]
    public class RegisterExternal : DocumentTypeBase
    {
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "PickerHome", Name = "Regulamin", Tab = TabNames.Ustawienia)]
        public string regulamin { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "PickerHome", Name = "Polityka prywatności", Tab = TabNames.Ustawienia)]
        public string polityka { get; set; }
    }
}
