﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedTemplates = new string[] { "Artykul" }
    )]
    public class ArtykulGlowna : AbstractDataObject
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Dane)]
        public string tytul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "LeadEditor", Name = "Lead", Tab = TabNames.Dane)]
        public string lead { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Polecany ?", Tab = TabNames.Dane)]
        public string polecany { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.ContentPicker, Name = "Artykul", Mandatory=true, Tab = TabNames.Dane)]
        public string artykul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.DatePicker, Name = "Data Publikacji", Description = "Pozostaw pustą, jeśli ma być brana pod uwagę data utworzenia artykułu", Tab = TabNames.Dane)]
        public string dataPublikacji { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Label, Name = "Licznik odwiedzin", Tab = TabNames.Licznik)]
        public string licznikOdwiedzin { get; set; }
    }
}
