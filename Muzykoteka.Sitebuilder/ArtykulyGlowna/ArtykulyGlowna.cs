﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedChildNodeTypes = new Type[] { typeof(ArtykulGlowna) }          
    )]
    public class ArtykulyGlowna : AbstractDataObject
    {

    }
}
