﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{    
    [DocumentType(
          IconUrl = "icon-keyhole"        
    )]
    public class SekcjaChroniona : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Dane)]
        public string tytul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Lead", Tab = TabNames.Dane)]
        public string lead { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Opis", Tab = TabNames.Dane)]
        public string opis { get; set; }      
    }
}
