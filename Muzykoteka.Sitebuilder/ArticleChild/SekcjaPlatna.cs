﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{    
    [DocumentType(
          IconUrl = "icon-keyhole"        
    )]
    public class SekcjaPlatna : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Merchello Product Selector", Name = "Produkt", Tab = TabNames.Dane)]
        public string produkt { get; set; }       
    }
}
