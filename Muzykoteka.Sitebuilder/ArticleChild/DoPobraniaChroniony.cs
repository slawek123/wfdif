﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{    
    [DocumentType(
          IconUrl = "icon-download-alt"        
    )]
    public class DoPobraniaChroniony : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Multiple Media Picker", Name = "Do pobrania", Tab = TabNames.Dane)]
        public string doPobrania { get; set; }
    }
}
