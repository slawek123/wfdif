﻿using Muzykoteka.Sitebuilder;
using System;
using System.Linq;
using Vega.USiteBuilder;

namespace Nina.Sitebuilder.DocumentTypes
{
    [DocumentType(IconUrl = "icon-folder")]
    class ProjektInternetowy : AbstractDataObject
    {
        [DocumentTypeProperty(UmbracoPropertyType.TextboxMultiple, Name = "Lead", Tab = TabNames.Dane)]
        public string lead { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageProject", Name = "Logo", Tab = TabNames.Dane)]
        public string logo { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Dane)]
        public string Tytul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Link", Tab = TabNames.Dane)]
        public string link { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageCropperProjektIntTlo", Name = "Tło", Tab = TabNames.Zdjecie)]
        public string tlo { get; set; }
    }
}
