﻿using Muzykoteka.Sitebuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vega.USiteBuilder;

namespace Nina.Sitebuilder.DocumentTypes
{
    [DocumentType(
        IconUrl = "icon-folder",
        AllowedChildNodeTypes = new[] { typeof(ProjektInternetowy) })]
    class Projekty : AbstractDataObject
    {
    }
}
