﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-movie",
          Mixins = new[] {
            typeof(DRMData)
          },
          AllowedTemplates = new string[] { "StreamVOD" }
    )]
    public class StreamVOD : AbstractDataObject
    {
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Poster", Name = "Stopklatka", Tab = TabNames.Dane)]
        public string poster { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "DASH KID", Tab = TabNames.Inne)]
        public string DASH_KID { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Dash Seed", Tab = TabNames.Inne)]
        public string DASH_SEED { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "HLS KID", Tab = TabNames.Inne)]
        public string HLS_KID { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "inputPath", Tab = TabNames.Inne)]
        public string inputPath { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Id", Tab = TabNames.Inne)]
        public string IncomingId { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "status", Tab = TabNames.Inne)]
        public string status { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Multiple Media Picker", Alias = "subttitles", Name = "Napisy", Tab = TabNames.Dane)]
        public string napisy { get; set; }
    }
}
