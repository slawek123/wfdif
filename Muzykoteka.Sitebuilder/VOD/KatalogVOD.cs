﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    //icon-movie
    [DocumentType(
          IconUrl = "icon-folder",
          AllowAtRoot = true,          
          AllowedChildNodeTypes = new Type[] {
              typeof(StreamVOD),
              typeof(StreamAOD),
              typeof(StreamVOD360),
              typeof(KatalogVOD),                                        
          }      
    )]
    public class KatalogVOD : AbstractDataObject
    {       
    }
}
