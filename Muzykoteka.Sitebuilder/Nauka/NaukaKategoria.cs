﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedTemplates = new string[] { "ListaZFiltrem" },
          AllowedChildNodeTypes = new Type[] { typeof(NaukaArtykul), typeof(NaukaGra) }
    )]
    public class NaukaKategoria : Podkategoria
    {
        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Dla nauczyciela", Tab = TabNames.Dane)]
        public string dlaNauczyciela { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Dla ucznia", Tab = TabNames.Dane)]
        public string dlaUcznia { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Dla studenta", Tab = TabNames.Dane)]
        public string dlaStudenta { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Pokaż w Lekcji", Tab = TabNames.Dane)]
        public string pokazLekcji { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Pokaż wszystkie", Tab = TabNames.Dane)]
        public string PokazWszystkie { get; set; }
    }
}
