﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedTemplates = new string[] { "Kategoria" },
          AllowedChildNodeTypes = new Type[] { typeof(NaukaKategoria) }          
    )]
    public class Nauka : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Opis", Tab = TabNames.Dane)]
        public string opis { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "VideoMacro", Name = "Video", Tab = TabNames.Media)]
        public string videoEmbed { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name="Box",Tab =TabNames.Media)]
        public string Box { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageHome", Name ="Plakat",Tab = TabNames.Media)]
        public string Plakat { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name ="Pokaż Social Media", Tab = TabNames.Ustawienia)]
        public string PokazSocialMedia { get; set; }
    }
}
