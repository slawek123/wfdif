﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedTemplates = new string[] { "NaukaArtykul" },
          AllowedChildNodeTypes = new Type[] {
              typeof(SekcjaChroniona),
              typeof(DoPobraniaChroniony),
              typeof(SekcjaPlatna)
          }        
    )]
    public class NaukaArtykul : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Dane)]
        public string tytul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Nagłówek", Tab = TabNames.Dane)]
        public string naglowek { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "LeadEditor", Name = "Lead na kaflu", Tab = TabNames.Dane)]
        public string lead { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "LeadEditor", Name = "Lead w artykule", Tab = TabNames.Dane)]
        public string leadArtykul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Opis", Tab = TabNames.Dane)]
        public string opis { get; set; }
        
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Multiple Media Picker", Name = "Do pobrania", Tab = TabNames.DoPobrania)]
        public string doPobrania { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Dla nauczyciela", Tab = TabNames.Dane)]
        public string dlaNauczyciela { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Dla ucznia", Tab = TabNames.Dane)]
        public string dlaUcznia { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Dla studenta", Tab = TabNames.Dane)]
        public string dlaStudenta { get; set; }



        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Przedmiot", Name = "Przedmiot", Tab = TabNames.Dane)]
        public string przedmiot { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Szkola", Name = "Szkoła", Tab = TabNames.Dane)]
        public string rodzajSzkoly { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Klasy", Name = "Klasy", Description = "Nazwy klas dla szkół wybranych wyżej", Tab = TabNames.Dane)]
        public string klasy { get; set; }


        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageArtykul", Name = "Zdjecie", Tab = TabNames.Zdjecie)]
        public string zdjecie { get; set; }


        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "List View - Content", Name = "Sekcje chronione", Tab = TabNames.Chronione)]
        public string sekcje { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Pokaż w Lekcji",Tab =TabNames.Chronione)]
        public string pokazLekcji { get; set; }
    }
}
