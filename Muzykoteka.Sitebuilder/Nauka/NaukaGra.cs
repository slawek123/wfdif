﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-game",
          AllowedTemplates = new string[] { "NaukaGra" }
    )]
    public class NaukaGra : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Dane)]
        public string tytul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Nazwa gry", Tab = TabNames.Dane)]
        public string nazwaGry { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "LeadEditor", Name = "Lead na kaflu", Tab = TabNames.Dane)]
        public string lead { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageArtykul", Name = "Zdjecie", Tab = TabNames.Zdjecie)]
        public string zdjecie { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Dla nauczyciela", Tab = TabNames.Dane)]
        public string dlaNauczyciela { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Dla ucznia", Tab = TabNames.Dane)]
        public string dlaUcznia { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Dla studenta", Tab = TabNames.Dane)]
        public string dlaStudenta { get; set; }
    }
}
