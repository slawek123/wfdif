﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder"                    
    )]
    public class Podkategoria : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "MediaIkona", Name = "Ikona", Tab = TabNames.Dane)]
        public string ikona { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Numeric, Name = "Liczba artykułów 'zobacz również'", Description = "Liczba artykułów 'zobacz również' JEŚLI generowana losowo.", Tab = TabNames.ZobaczRowniez)]
        public string liczbaZobaczRowniez { get; set; }
    }
}
