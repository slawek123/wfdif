﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedTemplates = new string[] { "szukaj" }          
    )]
    public class Szukaj : AbstractPage
    {

    }
}
