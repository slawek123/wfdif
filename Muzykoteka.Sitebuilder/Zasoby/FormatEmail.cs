﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(Alias = "eMail")]
    class FormatEmail : AbstractDataObject
    {

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Nagłówek", Alias = "naglowek", Mandatory = false, Tab = TabNames.Email)]
        public string Naglowek { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Treść", Alias = "tresc", Description = " %NAME% - imię wpisane przez użytkownika, %URL% - Adres udostępnianej streści %LEKCJA% - nazwa lekcji, %DATAWYKONANIA%- data ważności lekcji %TEXT% - Treść wpisana przez użytkownika", Mandatory = false, Tab = TabNames.Email)]
        public string Tresc { get; set; }
    }
}
