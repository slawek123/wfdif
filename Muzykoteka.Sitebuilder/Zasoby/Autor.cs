﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(IconUrl = "icon-umb-users")]
    class Autor : AbstractDataObject
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Imie", Tab = TabNames.Dane)]
        public string Imie { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Nazwisko", Tab = TabNames.Dane)]
        public string Nazwisko { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Other, Name = "Zdjęcie", OtherTypeName = "ImageProdukt", Tab = TabNames.Dane)]
        public string AuthorImage { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Informacje", Tab = TabNames.Dane)]
        public string AboutAuthor { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Facebook", Tab = TabNames.Dane)]
        public string Facebook { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Twitter", Tab = TabNames.Dane)]
        public string Twitter { get; set; }
    }
}
