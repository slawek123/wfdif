﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-tools",        
        AllowedChildNodeTypes = new[] { 
            typeof(KeyName)
        })]
    class Slownik : AbstractDataObject
    {
    }
}
