﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-message",
        AllowedChildNodeTypes = new[] {
            typeof(FormatEmail),
        },
        Alias = "eMaile")]
    class FormatyEmaili : AbstractDataObject
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Adres E-Mail", Mandatory = true, Tab = TabNames.Email)]

        public string Email { get; set; }
    }
}
