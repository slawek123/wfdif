﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-tools",
        AllowAtRoot = true,
        AllowedChildNodeTypes = new[] {
            typeof(Slowniki),
            typeof(FormatyEmaili),
            typeof(Autorzy),
        })]
    class Zasoby : AbstractDataObject
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "WIDEVINE: DRM", Tab = TabNames.DRM)]
        public string widevide_cert_url { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "WIDEVINE: URL servera licencji", Tab = TabNames.DRM)]
        public string widevide_license_url { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "FAIRPLAY: DRM", Tab = TabNames.DRM)]
        public string fairplay_cert_url { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "FAIRPLAY: URL servera licencji", Tab = TabNames.DRM)]
        public string fairplay_license_url { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "FAIRPLAY: URL certyfikatu", Tab = TabNames.DRM)]
        public string fairplay_localcert_url { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "PLAYREADY: DRM", Tab = TabNames.DRM)]
        public string playready_cert_url { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "PLAYREADY: URL servera licencji", Tab = TabNames.DRM)]
        public string playready_license_url { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Repeatable textstring", Name = "Adres IP", Description = "Adres IP z którego będą przychodzić informacje o dodawanych plikach", Tab = TabNames.Ustawienia)]
        public string AdresIp { get; set; }
    }
}
