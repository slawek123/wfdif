﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(IconUrl = "icon-users", AllowedChildNodeTypes = new[] { typeof(Autor) })]
    class Autorzy : AbstractDataObject
    {

    }
}
