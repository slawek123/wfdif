﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-tools",
        AllowedChildNodeTypes = new[] {
            typeof(Slownik),
        })]
    class Slowniki : AbstractDataObject
    {
    }
}
