﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedTemplates = new string[] { "ListaZFiltrem" },
          AllowedChildNodeTypes = new Type[] { typeof(Film) }
    )]
    public class Utwory : Podkategoria
    {       
        
    }
}
