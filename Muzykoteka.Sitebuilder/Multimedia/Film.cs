﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{    
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedTemplates = new string[] { "Film" },
          AllowedChildNodeTypes = new Type[] {
              typeof(SekcjaChroniona),
              typeof(DoPobraniaChroniony),
              typeof(SekcjaPlatna)
          }
    )]
    public class Film : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Dane)]
        public string tytul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "KategoriaWiekowa", Name = "Kategoria wiekowa", Tab = TabNames.Dane)]
        public string kategoriaWiekowa { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Kategoria", Name = "Kategoria", Tab = TabNames.Dane)]
        public string kategoria { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Reżyser", Tab = TabNames.Dane)]
        public string rezyser { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Rok produkcji", Tab = TabNames.Dane)]
        public string rok_produkcji { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Czas trwania", Tab = TabNames.Dane)]
        public string duration { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Kraj", Tab = TabNames.Dane)]
        public string kraj { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Ekipa", Name = "Ekipa", Tab = TabNames.Dane)]
        public string ekipa { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Obsada", Name = "Obsada", Tab = TabNames.Dane)]
        public string obsada { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "LeadEditor", Name = "Lead na kaflu", Tab = TabNames.Dane)]
        public string lead { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "LeadEditor", Name = "Lead w artykule", Tab = TabNames.Dane)]
        public string leadArtykul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Opis", Tab = TabNames.Dane)]
        public string opis { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Galeria", Tab = TabNames.Dane)]
        public string opis2 { get; set; }


        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageArtykul", Name = "Zdjęcie", Tab = TabNames.Media)]
        public string zdjecie { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "VideoMacro", Name = "Video", Tab = TabNames.Media)]
        public string videoEmbed { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Podpis Video", Tab = TabNames.Media)]
        public string podpisVideo { get; set; }


        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "PickerZobaczRowniez", Name = "Artykuły", Tab = TabNames.ZobaczRowniez)]
        public string zobaczRowniez { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Multiple Media Picker", Name = "Do pobrania", Tab = TabNames.DoPobrania)]
        public string doPobrania { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "List View - Content", Name = "Sekcje chronione", Tab = TabNames.Chronione)]
        public string sekcje { get; set; }
    }
}
