﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedTemplates = new string[] { "Kategoria" },
          AllowedChildNodeTypes = new Type[] { typeof(Utwory) }
    )]
    public class Multimedia : AbstractPage
    {
    }
}
