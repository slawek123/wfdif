﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(IconUrl = "icon-tools")]
    public class DRMData : AbstractDataObject
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Stream DASH", Tab = TabNames.Dane)]
        public string streamURL_DASH { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Klucz DASH", Tab = TabNames.Dane)]
        public string kluczDASH { get; set; }


        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Stream HLS", Tab = TabNames.Dane)]
        public string streamURL_HLS { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Klucz HLS", Tab = TabNames.Dane)]
        public string kluczHLS { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "IV HLS", Tab = TabNames.Dane)]
        public string ivectorHLS { get; set; }
    }
}
