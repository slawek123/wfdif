﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    public enum TabNames
    {
        [TabName("SEO")]
        Seo,
        [TabName("Dane")]
        Dane,
        [TabName("Zobacz rownież/FAQ")]
        Zobacz_rowniez_FAQ,
        [TabName("Do pobrania")]
        DoPobrania,
        [TabName("Zdjecie")]
        Zdjecie,
        [TabName("Zobacz również")]
        ZobaczRowniez,
        [TabName("Transmisja")]
        Transmisja,
        [TabName("Artykuł")]
        Artykul,
        [TabName("FAQ")]
        FAQ,
        [TabName("Freshmail")]
        Freshmail,
        [TabName("Licznik")]
        Licznik,
        [TabName("Artykuły")]
        Artykuly,
        [TabName("Linki uzupełniające")]
        LinkiUzupelniajace,
        [TabName("Wersja czarno-biała")]
        WersjaCzarnoBiała,
        [TabName("E-mail")]
        Email,
        [TabName("Media")]
        Media,
        [TabName("DRM")]
        DRM,
        [TabName("Powitanie")]
        Przywitanie,
        [TabName("Ustawienia")]
        Ustawienia,
        [TabName("Chronione")]
        Chronione,
        [TabName("Opis produktu")]
        OpisProduktu,
        [TabName("Inne")]
        Inne                
    }
}
