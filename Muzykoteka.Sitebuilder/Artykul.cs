﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder"
    )]
    public class Artykul : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Dane)]
        public string tytul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "LeadEditor", Name = "Lead", Tab = TabNames.Dane)]
        public string lead { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Treść", Tab = TabNames.Dane)]
        public string tresc { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Tags, Name = "Tagi", Tab = TabNames.Dane)]
        public string tagi { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.DatePicker, Name = "Data Publikacji", Description = "Pozostaw pustą, jeśli ma być brana pod uwagę data utworzenia artykułu", Tab = TabNames.Dane)]
        public string dataPublikacji { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "PickerZobaczRowniez", Name = "Artykuły", Tab = TabNames.ZobaczRowniez)]
        public string zobaczRowniez { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Numeric, Name = "Liczba artykułów 'zobacz również'", Description = "Liczba artykułów 'zobacz również' jeśli generowana losowo.", Tab = TabNames.ZobaczRowniez)]
        public string liczbaZobaczRowniez { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Multiple Media Picker", Name = "Do pobrania", Tab = TabNames.DoPobrania)]
        public string doPobrania { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Epoka", Name = "Epoka", Tab = TabNames.Dane)]
        public string epoka { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageArtykul", Name = "Zdjecie", Tab = TabNames.Zdjecie)]
        public string zdjecie { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Pokaż zdjęcie w artykule", Tab = TabNames.Zdjecie)]
        public string pokazZdjecie { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageArtykulDuze", Name = "Zdjecie w artykule", Tab = TabNames.Zdjecie)]
        public string zdjecieArtykulowe { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Podpis", Tab = TabNames.Zdjecie)]
        public string podpisZdjeciaArtykulowego { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Alt", Tab = TabNames.Zdjecie)]
        public string altZdjeciaArtykulowego { get; set; }
    }
}
