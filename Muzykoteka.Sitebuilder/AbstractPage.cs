﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType]
    public class AbstractPage : DocumentTypeBase
    {
        [DocumentTypeProperty(UmbracoPropertyType.TextboxMultiple, Name = "Meta description", Description = "", Tab = TabNames.Seo)]
        public string MetaDescription { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Hide from search", Description = "", Tab = TabNames.Seo)]
        public string SearchHide { get; set; }
    }
    [DocumentType(IconUrl = "", AllowAtRoot = false, AllowedTemplates = new string[] { "QR" }, DefaultTemplate = "QR", Name = "QR")]
    public class QRCode : DocumentTypeBase
    {

    }
}
