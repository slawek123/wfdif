﻿using System;
using System.Linq;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder.Kolekcja
{
    [DocumentType(
    IconUrl = "icon-folder",
        AllowedTemplates = new string[] { "Okladka"}
        )]
    public class Okladka : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Dane)]
        public string tytul { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageArtykul", Name = "Zdjęcie", Tab = TabNames.Dane)]
        public string zdjecie { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "LeadEditor", Name = "Lead", Tab = TabNames.Dane)]
        public string lead { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Cena", Tab = TabNames.Dane)]
        public string Cena { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Cena Promocyjna", Tab = TabNames.Dane)]
        public string CenaPromocyjna { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Produkcja", Tab = TabNames.Dane)]
        public string Produkcja { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Rok Wydania", Tab = TabNames.Dane)]
        public string RokWydania { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Opis", Tab = TabNames.Dane)]
        public string Opis { get; set; }

    }
}
