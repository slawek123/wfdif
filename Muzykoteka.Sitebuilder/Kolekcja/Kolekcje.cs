﻿using System;
using System.Linq;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder.Kolekcja
{
    [DocumentType(
     IconUrl = "icon-folder",
     AllowedTemplates = new string[] { "ListaZFiltrem" },
     AllowedChildNodeTypes = new Type[] {
         typeof(Film),
         typeof(Okladka)
     }
)]
    public class Kolekcje : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Multi Collection Picker", Name = "Polecane Kolekcje", Tab = TabNames.Media)]
        public string PolecaneKolekcje { get; set; }
    }
}
