﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder.Kolekcja
{
    [DocumentType(
          IconUrl = "icon-folder",
          AllowedTemplates = new string[] { "Kategoria" },
          AllowedChildNodeTypes = new Type[] { typeof(Kolekcje) }
    )]
    public class Kolekcja : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Opis", Tab = TabNames.Dane)]
        public string opis { get; set; }
    }
}
