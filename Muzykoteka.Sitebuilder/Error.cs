﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{

    [DocumentType (
        IconUrl = "icon-settings",
        AllowedTemplates = new string[] { "Error" })]        
    class Error : DocumentTypeBase
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Dane)]
        public string ertytul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Treść", Tab = TabNames.Dane)]
        public string ertresc { get; set; }
    }
}
