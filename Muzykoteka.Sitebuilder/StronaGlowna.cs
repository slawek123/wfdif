﻿using Muzykoteka.Sitebuilder.Kolekcja;
using Muzykoteka.Sitebuilder.Lekcje;
using Nina.Sitebuilder.DocumentTypes;
using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-home",
          AllowAtRoot = true,
          AllowedTemplates = new string[] { "Home" },
          AllowedChildNodeTypes = new Type[] {
              typeof(Nauka),
              typeof(Multimedia),
              typeof(Schowek),
              typeof(Pozostale),
              typeof(Szukaj),
              typeof(Error),
              typeof(Regulamin),
              typeof(Projekty),
              typeof(Kolekcja.Kolekcja),
              typeof(Sklep),
              typeof(Uzytkownicy),
              typeof(QRCode),
              typeof(Account)
          }
    )]
    public class StronaGlowna : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageHome", Name = "Zdjecie", Tab = TabNames.Artykul)]
        public string zdjecieArtykul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "PickerHome", Name = "Artykuł", Tab = TabNames.Artykul)]
        public string artykul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Włącz transmisję", Tab = TabNames.Transmisja)]
        public string transmisjaOn { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Tytuł", Tab = TabNames.Transmisja)]
        public string transmisjaTytul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TextboxMultiple, Name = "Embed", Tab = TabNames.Transmisja)]
        public string transmisjaEmbed { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageHome", Name = "Zdjęcie", Tab = TabNames.Transmisja)]
        public string transmisjaZdjecie { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Baner", Name = "Baner", Tab = TabNames.Transmisja)]
        public string baner { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Opis", Tab = TabNames.Transmisja)]
        public string transmisjaOpis { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "PickerHome", Name = "Link", Tab = TabNames.Transmisja)]
        public string transmisjaLink { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Wersja czarno-biała", Tab = TabNames.WersjaCzarnoBiała)]
        public string isGrayscale { get; set; }


        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageArtykul", Name = "Zdjęcie domyślne", Tab = TabNames.Dane)]
        public string zdjecieDomyslne { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Multi Image Picker", Name = "Plakaty", Tab = TabNames.Dane)]
        public string Plakaty { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Ostatnio Dodane", Tab = TabNames.Ustawienia)]
        public string Ostatnio_Dodane { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Nad Banerem", Tab = TabNames.Ustawienia)]
        public string NadBanerem { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Powitanie", Tab = TabNames.Przywitanie)]
        public string Powitanie { get; set; }
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Opis", Tab = TabNames.Przywitanie)]
        public string OpisPowitanie { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "Multiple Media Picker", Name = "Belka logotypów", Tab = TabNames.Ustawienia)]
        public string logotypy { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "PickerZobaczRowniez", Name = "Wyróżnione", Tab = TabNames.Dane)]
        public string Wyrozniony { get; set; }
    }
}
