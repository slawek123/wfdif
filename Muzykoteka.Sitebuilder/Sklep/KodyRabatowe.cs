﻿using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-coin-euro",
        AllowAtRoot = true,
        AllowedChildNodeTypes = new [] {            
            typeof(KodRabatowy),
            typeof(KodyRabatowe),
        })]
    class KodyRabatowe : AbstractDataObject
    {                
    }
}
