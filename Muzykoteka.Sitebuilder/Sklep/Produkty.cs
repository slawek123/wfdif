﻿using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-coin-euro",
        AllowedChildNodeTypes = new [] {      
            typeof(ProduktKoszulka),
            typeof(ProduktDVD),
            typeof(ProduktKsiazka),
        })]
    class Produkty : AbstractDataObject
    {                
    }
}
