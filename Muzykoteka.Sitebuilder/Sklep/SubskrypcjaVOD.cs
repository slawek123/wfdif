﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-gift")]
    class SubskrypcjaVOD : AbstractDataObject
    {
        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "ImageProdukt", Name = "Zdjęcie", Tab = TabNames.OpisProduktu)]
        public string zdjecie { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.ContentPicker, Name = "Artykuł", Tab = TabNames.OpisProduktu)]
        public string artykul { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Other, OtherTypeName = "LeadEditor", Name = "Lead", Tab = TabNames.OpisProduktu)]
        public string lead { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Opis", Tab = TabNames.OpisProduktu)]
        public string opis { get; set; }
    }
}
