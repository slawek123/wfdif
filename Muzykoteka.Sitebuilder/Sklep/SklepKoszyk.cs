﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-shopping-basket",
        AllowedTemplates = new string[] { "SklepKoszyk" })]

    public class SklepKoszyk : AbstractPage
    {
        
    }
}
