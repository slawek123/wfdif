﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-shopping-basket",
        AllowedTemplates = new string[] { "SklepMojeZamowienia" })]

    public class SklepMojeZamowienia : AbstractPage
    {
        
    }
}
