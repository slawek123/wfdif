﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-gift"
        )]
    class KodRabatowy : AbstractDataObject
    {      
        [DocumentTypeProperty(UmbracoPropertyType.Numeric, Name = "Wartość", Description = "Wartość rabatu", Tab = TabNames.Dane)]
        public string kwota { get; set; }
    }
}
