﻿using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-folder",
        AllowedTemplates = new string[] { 
            "SklepRegulamin"          
        })]
    class Dostawy : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Dostawy", Tab = "Dostawy")]
        public string opis { get; set; }
    }
}
