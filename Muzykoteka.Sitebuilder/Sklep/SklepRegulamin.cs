﻿using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-folder",
        AllowedTemplates = new string[] { 
            "SklepRegulamin"          
        })]
    class SklepRegulamin : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Regulamin", Tab = "Regulamin")]
        public string opis { get; set; }
    }
}
