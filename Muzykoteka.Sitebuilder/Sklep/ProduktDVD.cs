﻿using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-gift",
        AllowedTemplates = new string[] { "Produkt" } )]    
    class ProduktDVD : Produkt
    {       
    }
}
