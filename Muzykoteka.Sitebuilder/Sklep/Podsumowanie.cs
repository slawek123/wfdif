﻿using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-folder",
        AllowedTemplates = new string[] { 
            "Podsumowanie"
        })]
    class Podsumowanie : AbstractPage
    {
        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Odbiór osobisty", Tab = "Odbiór osobisty")]
        public string odbior { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Dot Pay - powodzenie", Tab = "Dot Pay - OK")]
        public string dotpay_ok { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Dot Pay - błąd", Tab = "Dot Pay - ERROR")]
        public string dotpay_err { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Pobranie", Tab = "Pobranie")]
        public string pobranie { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Przelew", Tab = "Przelew")]
        public string przelew { get; set; }
    }
}
