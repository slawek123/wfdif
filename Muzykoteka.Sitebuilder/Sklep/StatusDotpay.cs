﻿using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
        IconUrl = "icon-folder",
        AllowedTemplates = new string[] { 
            "StatusDotpay"
        })]
    class StatusDotpay : AbstractPage
    {
    }
}
