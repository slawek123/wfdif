﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
         IconUrl = " icon-store",       
         AllowAtRoot = true,
         AllowedTemplates = new string[] { "Sklep" },
         AllowedChildNodeTypes = new Type[] {
             typeof(SklepKoszyk),
             typeof(Pozycja),
             typeof(Checkout),
             typeof(Podsumowanie),
             typeof(SklepRegulamin),
             typeof(Dostawy),
             typeof(StatusDotpay),
             typeof(SklepMojeZamowienia)
         }
   )]
    public class Sklep : AbstractPage
    {
        
    }
}
