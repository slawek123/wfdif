﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
              IconUrl = "icon-umb-users large",
              AllowedTemplates = new string[] { "EditProfil" }
        )]
    public class EditProfile : AbstractPage
    {

    }
}
