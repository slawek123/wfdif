﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",          
          AllowedTemplates = new string[] { "Pozostale", "Kontakt" }          
    )]
    public class PozostaleArtykul : Artykul
    {
    }
}
