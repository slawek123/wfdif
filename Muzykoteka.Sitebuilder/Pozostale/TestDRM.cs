﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{    
    [DocumentType(
          IconUrl = "icon-folder",
          Mixins = new[] {
            typeof(DRMData)
          },
          AllowedTemplates = new string[] { "TestDRM" }
    )]
    public class TestDRM : AbstractPage
    {

    }
}
