﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(IconUrl = "icon-book-alt-2", AllowedTemplates = new string[] {"Artykul"})]
    public class Regulamin : AbstractPage
{
    [DocumentTypeProperty(UmbracoPropertyType.RichtextEditor, Name = "Treść", Tab = TabNames.Artykul)]
    public string Tresc { get; set; }
}
}
