﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder"
    )]
    public class FAQ : AbstractDataObject
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Pytanie", Tab = TabNames.FAQ)]
        public string pytanie { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TextboxMultiple, Name = "Odpowiedź", Tab = TabNames.FAQ)]
        public string odpowiedz { get; set; }
    }
}
