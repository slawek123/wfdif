﻿using System;
using Vega.USiteBuilder;

namespace Muzykoteka.Sitebuilder
{
    [DocumentType(
          IconUrl = "icon-folder",          
          AllowedChildNodeTypes = new Type[] { typeof(PozostaleArtykul), typeof(TestDRM),typeof(Regulamin), typeof(LoginCallback) }
    )]
    public class Pozostale : AbstractPage
    {

    }
}
