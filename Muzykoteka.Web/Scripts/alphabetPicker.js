$.widget("widgets.AlphabetPicker", {
    options: {
        FilterField: '',
        nullText: '',
        widthUpdated: false
    },


    _create: function () {

        var me = this;

        var combo = this.element,
            drop = this.element.find('.js-drop'),
            list = this.element.find('.js-list'),
            items = this.element.find('.js-list ul li'),
            button_asc = this.element.find('.asc'),
            button_desc = this.element.find('.desc');

        this.UpdateWidth();

        drop.on("click", function (e) {
            var cmb = $(this).parent();            
            if (cmb.hasClass('up')) {
                cmb.removeClass('up');
                cmb.addClass('down');
            }
            else {               
                cmb.removeClass('down');
                cmb.addClass('up');
            }
        });

                
        items.on("click", function (e) {
            
            var item = $(this);
            var isAnySelected = true;        

            if (item.hasClass('selected')) {
                item.removeClass('selected');
                var selected = me.element.find('.selected');
                if (!selected.length) {
                    isAnySelected = false;
                }
            }
            else {
                item.addClass('selected');
            }

            drop.toggleClass('selected-any', isAnySelected);
            
            me._trigger("SelectionChanged", null, { sender: me });      
        });
        

        button_asc.on("click", function (e) {
            var btn = $(this);
            if (btn.hasClass('selected')) {
                btn.toggleClass('selected', false);
                var selected = me.element.find('.selected');
                if (!selected.length) {
                    drop.toggleClass('selected-any', false);
                }
            }
            else {
                btn.toggleClass('selected', true);
                drop.toggleClass('selected-any', true);
                var button_desc = me.element.find('.desc');
                if (button_desc.hasClass('selected')) {
                    button_desc.removeClass('selected');
                }
            }
            me._trigger("SelectionChanged", null, { sender: me });
        });

        button_desc.on("click", function (e) {
            var btn = $(this);
            if (btn.hasClass('selected')) {
                btn.toggleClass('selected', false);
                var selected = me.element.find('.selected');
                if (!selected.length) {
                    drop.toggleClass('selected-any', false);
                }
            }
            else {                
                btn.toggleClass('selected', true);
                drop.toggleClass('selected-any', true);
                var button_asc = me.element.find('.asc');
                if (button_asc.hasClass('selected')) {
                    button_asc.removeClass('selected');
                }
            }
            me._trigger("SelectionChanged", null, { sender: me });
        });
    },

    GetSort : function () {
        var button_asc = this.element.find('.asc');
        if (button_asc.hasClass('selected')) {
            return 1;
        }
        var button_desc = this.element.find('.desc');
        if (button_desc.hasClass('selected')) {
            return -1;
        }
        return 0;
    },
    
    UpdateWidth: function () {        
        if (!this.options.widthUpdated) {            
            var combo = this.element,
                drop = this.element.find('.js-drop'),
                list = this.element.find('.js-list');

            var max_width = Math.max(drop.outerWidth(), list.outerWidth());
            if (max_width > 0) {
                combo.css('width', max_width + 20);
                list.css('width', max_width + 20);
                this.options.widthUpdated = true;
            }
        }
    },
    
    _hide: function () {
        var cmb = this.element;
        if (cmb.hasClass('down')) {
            cmb.removeClass('down');
            cmb.addClass('up');
        }
    },


    Hide: function () {
        this._hide();
    },


    ClearSelection: function () {
        var drop = this.element.find('.js-drop');
        this.element.find('.selected').removeClass('selected');
        drop.toggleClass('selected-any', false);
    },


    SetSelection: function (selectedValues) {        
        var drop = this.element.find('.js-drop');
        var items = this.element.find('li');
        this.element.find('.selected').removeClass('selected');
        if (selectedValues.length > 0) {
            var aselected = selectedValues.split(',');
            var sort = parseInt(aselected[0]);       
            if (sort == 1) {
                var button_asc = this.element.find('.asc');
                button_asc.toggleClass('selected', true);
            }
            else if (sort == -1) {
                var button_desc = this.element.find('.desc');
                button_desc.toggleClass('selected', true);
            }
            items.each(function () {
                var v = $(this).attr("value");
                if (selectedValues.indexOf(v) != -1) {
                    $(this).addClass('selected');
                    isAnySelected = true;
                }
            });
            drop.toggleClass('selected-any', true);
        }
        else {
            drop.toggleClass('selected-any', false);
        }
    },

    GetSelectedValues: function () {
        var list = this.element.find('.js-list');
        var selected = list.find('.selected');
        var vals = $.map(selected, function (li) {
            return $(li).attr("value");
        });
        var sort = this.GetSort();
        vals.unshift(sort);
        return vals;
    },
   
    GetFilterExpr: function () {
        var list = this.element.find('.js-list');
        var selected = list.find('.selected');
        var all_items = this.element.find('li');
        if (!selected.length) {
            return '';
        }
        else {
            var vals = '';
            selected.each(function () {
                var v = $(this).attr("value");
                if (v) {
                    if (vals.length)
                        vals += ',';
                    vals += v;
                }
            });
            if (vals.length <= 0)
                return '';
            else
                return this.options.FilterField + ' lt (' + vals + ')';            
        }    
    }
});

$(document).mouseup(function (e) {
    var container = $('.js-alphapicker');

    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
       $('.js-alphapicker').AlphabetPicker('Hide');
    }
});