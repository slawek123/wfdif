$.widget("widgets.ToggleReceiver", {
    options: {

    },


    _create: function () {

        var me = this;

        var teacher = this.element.find('.teacher'),
            pupil = this.element.find('.pupil'),
            student = this.element.find('.student'),
            close = this.element.find('.close');

        student.on("click", function (e) {
            var li = $(this);
            if (!li.hasClass('selected')) {
                pupil.removeClass('selected');
                teacher.removeClass('selected');
                li.addClass('selected');
                me._saveClick(3, li);
            }
        });

        teacher.on("click", function (e) {
            var li = $(this);
            if (!li.hasClass('selected')) {
                pupil.removeClass('selected');
                student.removeClass('selected');
                li.addClass('selected');
                me._saveClick(1);
            }
        });

        pupil.on("click", function (e) {
            var li = $(this);
            if (!li.hasClass('selected')) {
                teacher.removeClass('selected');
                student.removeClass('selected');
                li.addClass('selected');
                me._saveClick(2, li);
            }
        });

        if (close.length) {
            close.on("click", function (e) {
                me.element.css('display', 'none');
            });
        }


    },

    FitDocument: function () {
        var h = $(document).height();
        this.element.css('height', h - 126);
    },

    _saveClick: function (receiver) {
        console.log(receiver);
        console.log(this);
        var temp = this;
        event.preventDefault();
        if (typeof isToggleReceiverResponse !== 'undefined') {
            $.ajax({
                url: '/vowosadmin/surface/ToggleReceiver/SetIsTeacher/?isTeacher=' + receiver,
                success: function () {
                    var value
                    switch (receiver) {
                        case 1:

                    }
                    location.href = $(temp.element[0]).find(".selected a")[0].href;
                    if (isToggleReceiverResponse == 2) {
                        // location.reload();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                },
                cache: false
            });
        }
    }
});
