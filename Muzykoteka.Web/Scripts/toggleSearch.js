$.widget("widgets.ToggleSearch", {

    _create: function () {
        this.element.on("click", function (e) {            
            $('#master-search').MasterSearch('Show');
            var item = $('#master-search .writer');
            item.focus();
        });      
    },  
});

$(function () {
    $('.js-master-search-toggle').ToggleSearch();    
});

