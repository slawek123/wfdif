$.widget("widgets.DelFromClipboard", {
    options: {

    },

    _create: function () {

        var me = this;

        var contentid = this.element.data('contentid');

        this.element.on("click", function (e) {
            $.ajax({
                url: '/vowosadmin/surface/ClipboardSurface/DelContent/?id=' + contentid,
                dataType: 'json',
                cache: false,
                success: function (data) {
                    $('#' + data).hide('fast', function () {
                        $('#' + data).remove();
                        var item = $(".js-clpb-counter");
                        var text = item[0].innerText;
                        item.text(text - 1);
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });
        });
    },
});
