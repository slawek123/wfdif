$.widget("widgets.ComboBox", {
    options: {
        isMultiselect: false,
        FilterField: '',
        nullText: '',
        widthUpdated: false,
        JsPopup: false,
    },


    _create: function () {

        var me = this;

        var combo = this.element,
            drop = this.element.find('.js-drop'),
            list = this.element.find('.js-list'),
            items = this.element.find('.js-list ul li');

        this.UpdateWidth();

        drop.on("click", function (e) {
            var cmb = $(this).parent();
            if (cmb.hasClass('up')) {
                cmb.removeClass('up');
                cmb.addClass('down');
            }
            else {
                cmb.removeClass('down');
                cmb.addClass('up');
            }
        });


        items.on("click", function (e) {
            var item = $(this);
            var isAnySelected = true;

            if (!me.options.isMultiselect && item.hasClass('selected')) {
                item.removeClass('selected');
                drop.toggleClass('selected-any', false);
                drop.find('.js-cmbtext').text(me.options.nullText);
                me._trigger("SelectionChanged", null, { sender: me });
            }
            else {
                if (!me.options.isMultiselect) {
                    me.element.find('.selected').removeClass('selected');
                }

                if (item.hasClass('selected')) {
                    item.removeClass('selected');
                    var selected = me.element.find('.selected');
                    if (!selected.length) {
                        isAnySelected = false;
                    }
                }
                else {
                    item.addClass('selected');
                }

                drop.toggleClass('selected-any', isAnySelected);

                if (!me.options.isMultiselect) {
                    drop.find('.js-cmbtext').text(item.text());
                }
                if (!me.options.JsPopup) {
                    me._trigger("SelectionChanged", null, { sender: me });
                }
            }
        });

    },

    UpdateWidth: function () {
        if (!this.options.widthUpdated) {
            var combo = this.element,
                drop = this.element.find('.js-drop'),
                list = this.element.find('.js-list');

            var max_width = Math.max(drop.outerWidth(), list.outerWidth());
            if (max_width > 0) {
                combo.css('width', max_width + 20);
                list.css('width', max_width + 20);
                this.options.widthUpdated = true;
            }
        }
    },
    ResizeWidth: function () {
        var combo = this.element,
            drop = this.element.find('.js-drop'),
            list = this.element.find('.js-list');
        var filter = $(".filter");
        var width = filter.width();
        if (width > 0) {
            combo.css('width', width);
            list.css('width', width);
            this.options.widthUpdated = true;
        }
    },
    _hide: function () {
        var cmb = this.element;

        if (cmb.hasClass('down')) {
            cmb.removeClass('down');
            cmb.addClass('up');
        }
    },


    Hide: function () {
        this._hide();
    },

    ClearSelection: function () {
        if (this.element[0].parentNode.id != "EduCat") {
            var selected = this.element.find('.selected').each(function () {
                $(this).removeClass('selected');
            });
            var drop = this.element.find('.js-drop');
            drop.toggleClass('selected-any', false);

            if (!this.options.isMultiselect) {
                drop.find('.js-cmbtext').text(this.options.nullText);
            }
            this._trigger("SelectionChanged", null, { sender: this });
        }
    },


    SetSelection: function (selectedValues) {

        var items = this.element.find('.js-list ul li');
        var drop = this.element.find('.js-drop');
        var isAnySelected = false;

        this.element.find('.selected').removeClass('selected');

        items.each(function () {
            var v = $(this).attr("data-value");
            if (selectedValues.indexOf(parseInt(v)) != -1) {
                $(this).addClass('selected');
                isAnySelected = true;
            }
        });

        drop.toggleClass('selected-any', isAnySelected);
    },


    GetSelectedValues: function () {
        var selected = this.element.find('.selected');
        var vals = $.map(selected, function (li) {
            return $(li).attr("data-value");
        });
        return vals;
    },

    GetSelectedTexts: function () {
        var selected = this.element.find('.selected');
        var vals = $.map(selected, function (li) {
            return $(li).text();
        });
        return vals;
    },

    GetFilterExpr: function () {
        var selected = this.element.find('.selected');
        var all_items = this.element.find('li');
        if (!selected.length) {
            return '';
        }
        else {
            if (selected.length == 1) {
                return this.options.FilterField + '=' + selected.attr("data-value");
            }
            else {
                //drewniane
                var vals = '';
                selected.each(function () {
                    var v = $(this).attr("data-value");
                    if (v) {
                        if (vals.length)
                            vals += ',';
                        vals += v;
                    }
                });
                return this.options.FilterField + ' in (' + vals + ')';
            }
        }
    }
});

$(document).mouseup(function (e) {
    var container = $('.js-combo');
    $.each(container, function (index, item){
        const value = $(item);
        if (!value.is(e.target) && value.has(e.target).length === 0) {
            $(value).ComboBox('Hide');
        }
    })
});