$.widget("widgets.ClearClipboard", {
    options: {

    },

    _create: function () {
                       
        this.element.on("click", function (e) {
            $.ajax({
                url: '/vowosadmin/surface/ClipboardSurface/DeleteAll',
                dataType: 'json',
                cache: false,
                success: function (data) {
                    $('.js-clpb-counter').text(data);
                    ReloadChildList();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });

        });
    },
});


$(function () {
    $('.js-clear-clipboard').ClearClipboard();
});