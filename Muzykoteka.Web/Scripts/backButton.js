$.widget("widgets.BackButton", {
    options: {

    },

    _create: function () {
                       
        var starting_history_len = window.history.length;

        this.element.on("click", function (e) {          

            window.history.go(starting_history_len - window.history.length - 1);

        });
    },
});


$(function () {
    $('.js-back-button').BackButton();
});