$.widget("widgets.AddToClipboard", {
    options: {

    },

    _create: function () {

        var contentid = this.element.data('contentid');

        this.element.on("click", function (e) {

            $(this).toggleClass("added");

            $.ajax({
                url: '/vowosadmin/surface/ClipboardSurface/AddContent/?id=' + contentid,
                dataType: 'json',
                cache: false,
                success: function (data) {
                    $('.js-clpb-counter').text(data);
                    $('.js-clpb-counter').effect("slide", 500);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });

        });
    },
});
