(function($){
	
	var scenariuszePopupView = function(options){
		
		var that = {
			$el : $(document.createElement('form'))
		};
		that = $.extend(that, options);
		
        that.$el.addClass('scenariusze-form def_popup');
        
        that.$el.html($('#scenariusze-popup-template').html());		
                
		that.$el.find('.close-button').on('click', function(){
			that.close();
		});

        that.$el.on('click', function () {
            that.close();
        });

		var $body = $('body');
		        
        that.render = function () {
            that.$el.find('.fi-lead-container').css('max-height', document.documentElement.clientHeight - 40);
            that.$el.find('.fi-lead-container').css('max-width', document.documentElement.clientWidth - 40);
			$body.append(that.$el);
			$body.addClass('popup-active');
			
			setTimeout(function(){
				$body.addClass('show-fade');
				that.$el.addClass('visible');
			}, 50);
			window.popups.scenariuszePopupActive = true;						
		};
		
		that.close = function(){
			that.$el.removeClass('visible');
			setTimeout(function(){
				that.$el.remove();
			}, 1000);			
			window.popups.scenariuszePopupActive = false;
			
			$body.removeClass('show-fade');
			setTimeout(function(){
				$body.removeClass('popup-active');
			}, 500);			
		};
		
		return that;		
	};
	
	window.popups = window.popups || {};
    window.popups.scenariuszePopup = scenariuszePopupView;
	window.popups.scenariuszePopupActive = false;
					
	$(function () {
        var $trigger = $('.js-scenariuszeInfo');
	    $trigger.on('click', function (ev) {
            ev.preventDefault();            
	        if (!window.popups.scenariuszePopupActive) {                
                var popup = scenariuszePopupView();
	            popup.render();
	        }
	    });
	});
	
})(jQuery);