$.widget("widgets.HomeFilterToggle", {
    options: {

    },


    _create: function () {                

        var items = this.element.find('.field');
        var recommended = this.element.find('.js-recommended');
        var newest = this.element.find('.js-newest');
        var mostviewed = this.element.find('.js-mostviewed');

        recommended.on("click", function (e) {
            items.removeClass('selected');
            $(this).addClass('selected');
            FilterChildList("polecany==1", "ds");
        });

        newest.on("click", function (e) {
            items.removeClass('selected');
            $(this).addClass('selected');
            FilterChildList('', "dd");
        });

        mostviewed.on("click", function (e) {
            items.removeClass('selected');
            $(this).addClass('selected');
            FilterChildList('', "dv");
        });
    },
});
