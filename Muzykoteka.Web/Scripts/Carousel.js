﻿var scaling = 1;
var currentSliderCount = 0;
var showCount = 0;
var controlsWidth = 0;
var scollWidth = [
    { "Name": " ", "Value": 0 }
];
var videoWidth = 0;


$(document).ready(function () {
    initSliders(true);
    initItem();
    MoviePossition();
});
$(window).resize(function () {
    initSliders(false);
    initItem();
    MoviePossition();
});
function MoviePossition() {
    var Movie = $("#Video");
    var windowHeight = window.innerHeight * 0.8;
    var windowWidth = windowHeight * (16 / 9);
    if (windowWidth > window.innerWidth) {
        windowWidth = window.innerWidth;
        windowHeight = windowWidth / (16 / 9);
    }
    var margin = (window.innerWidth - windowWidth - 20) / 2;
    Movie.height(windowHeight);
    Movie.width(windowWidth);
}
function initSliders(first) {
    var win = $(".Movies");
    var sliderFrame = $(".slider-frame");
    var sliderContainer = $(".slider-container");
    var slide = $(".slide");
    var SliderImage = $(".SliderImage");

    var windowWidth = win.width();
    if (windowWidth < 767) {
        showCount = 1;
    } else if (windowWidth < 1200) {
        showCount = 2;
    } else {
        showCount = 4;
    }
    //    else if (windowWidth < 1920) {
    //    showCount = 4;
    //} else
    //    showCount = 6;
    var descriptionHeight = 0;
    videoWidth = (((windowWidth - (controlsWidth * 2)) / showCount));
    var videoHeight = videoWidth ;
    var videoWidthDiff = (videoWidth * scaling) - videoWidth;
    var videoHeightDiff = (videoHeight * scaling) - videoHeight;
    var SliderHeight = (videoHeight + descriptionHeight);
    sliderFrame.height(videoHeight * scaling + descriptionHeight);
    sliderFrame.width(windowWidth - controlsWidth * 2);
    sliderContainer.height(videoHeight * scaling + descriptionHeight);
    sliderContainer.css("top", (videoHeightDiff / 2));

    slide.height(SliderHeight);
    slide.width(videoWidth);
    SliderImage.height(videoHeight);
    //$(".slide").mouseover(function () {
    //    $(this).css("width", (videoWidth * scaling));
    //    $(this).css("height", videoHeight * scaling + descriptionHeight);
    //    $(this).css("top", -(videoHeightDiff / 2));
    //    var item = $(this).find(".SliderImage")[0];
    //    item.style.height = videoHeight * scaling;
    //    if ($("#" + this.parentNode.id + " > .slide").index($(this)) == 0 || ($("#" + this.parentNode.id + " > .slide").index($(this))) % (showCount) == 0) {
    //    }
    //    else if (($("#" + this.parentNode.id + " > .slide").index($(this))) % (showCount) == 0 && $("#" + this.parentNode.id + " > .slide").index($(this)) != 0) {
    //        $(this).parent().css("margin-left", -(videoWidthDiff - controlsWidth));
    //    }
    //    else {
    //        $(this).parent().css("margin-left", - (videoWidthDiff));
    //    }
    //})
    //    .mouseout(function () {
    //        $(this).css("width", videoWidth * 1);
    //        $(this).css("height", videoHeight * 1 + descriptionHeight);
    //        $(this).css("top", 0);
    //        var item = $(this).find(".SliderImage")[0];
    //        item.style.height = videoHeight * 1;
    //        $(this).parent().css("margin-left", 0);
    //    });
    for (var i = 0; i < sliderContainer.length; i++) {
        var item = $(sliderContainer[i]);
        var videoCount = item.children().length;
        var SliderContainerWidth = ((videoWidth) * videoCount) + videoWidthDiff;
        var add = true;
        if (first) {
            item.on('touchstart', function (e) {
                xDown = e.originalEvent.touches[0].pageX;
            })
                .on('touchend', function (e) {
                    xUp = e.originalEvent.changedTouches[0].pageX;
                    if (xDown > xUp)
                        Slide($(this), false);
                    else if (xDown < xUp)
                        Slide($(this), true);
                });
        }
        for (var j = 0; j < scollWidth.length; j++)
            if (scollWidth[j].Name == item[0].id) {
                if (scollWidth[j].Value != 0) {
                    SlideAction(item[0], scollWidth[j].Value * videoWidth)
                }
                add = false;
                break;
            }
        if (add) {
            scollWidth.push({ "Name": item[0].id, "Value": 0 });
        }
        item.width(SliderContainerWidth);
    }
}
function Slide(item, left) {
    var scrollWidth;
    for (var i = 0; i < scollWidth.length; i++) {
        if (scollWidth[i].Name == item[0].id) {
            scrollWidth = scollWidth[i].Value;
            var btn;
            var btnCurrent;
            if (left) {
                btn = $(item[0].parentNode.parentNode.children[2]);
                btnCurrent = $(item[0].parentNode.parentNode.children[0]);
                if (scrollWidth - showCount >= 0) {
                    scollWidth[i].Value = scrollWidth = scrollWidth - showCount;
                }
                else {
                    scollWidth[i].Value = scrollWidth = 0;
                    btnCurrent.addClass("Disabled");
                }
            }
            else {
                btn = $(item[0].parentNode.parentNode.children[0]);
                btnCurrent = $(item[0].parentNode.parentNode.children[2]);
                if (!btnCurrent.hasClass("Disabled")) {
                    var childs = $("#" + item[0].id + " > .slide").length;
                    scollWidth[i].Value = scrollWidth = scrollWidth + showCount;
                    if (scrollWidth + 3 > childs - 1) {
                        btnCurrent.addClass("Disabled");
                        scollWidth[i].Value = scrollWidth = scrollWidth - (showCount - (childs - scrollWidth));
                    }
                }
            }

            SlideAction(item, scrollWidth * (videoWidth));
            btn.removeClass("Disabled")
            break;
        }
    }
}
function SlideAction(item, scrollWidth) {
    item.animate({
        left: - scrollWidth
    }, 0);
}
function initItem() {
    var win = $(".CategoryMovies > .MoviesItem");
    var Mov = $(".MoviesItem > .Mov");
    var windowWidth = win.width();
    var Count = showCount;
    var videoWidth = (windowWidth / Count);
    var videoHeight = Math.round(videoWidth / (16 / 9));
    Mov.height(videoHeight);
    Mov.width(videoWidth);
}
function SetAlternative(item, heigh, width) {
    item.style.height = heigh;
    item.style.width = width;

}