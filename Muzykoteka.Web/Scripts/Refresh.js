﻿
var saveditem;
var WaitingSave;
function TimeRefresh() {
    location.reload();
}
function HideFooter() {
    if (!document.getElementsByClassName("footer")[0].classList.toggle("FooterHide")) {
        TimeRefresh();
    }
}
function Refresh(Popup) {
    setTimeout(function () {
        var item = $(Popup);
        var ok = item.find(".ok");
        var input = item.find("input");
        var textArea = item.find("textarea");
        var form = item.find(".form");
        ok.addClass("hide");
        input.val();
        textArea.val();
        form.removeClass("hide");
    }, 2000);
}
function ShowPop(name, hideFooter) {
    var item = document.getElementById(name);
    if (item) {
        item.classList.toggle("Show");
        if (hideFooter) {
            HideFooter();
            Refresh(item);
        }
    }
    $("#" + name + " .Ok").html("");
    $("#" + name + " .text-danger").html("");
}