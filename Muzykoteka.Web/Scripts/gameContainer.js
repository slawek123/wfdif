$.widget("widgets.GameContainer", {
    options: {

    },

    _create: function () {
       
        var ifr = this.element;
        
        ifr.on("load", function (e) {                        
            this.contentWindow.focus();            
        });

        ifr.mouseenter(function () {       
            this.contentWindow.focus();
        });
    },
});



$(function () {
    $('.js-game-container').GameContainer();
});



