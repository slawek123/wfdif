﻿
function CloseCookie() {
    CookiePop.style.display = 'none';
    SetCookie("CookiePop=true", 365 * 24 * 60 * 60 * 1000);
}
function SetCookie(name, time) {
    const date = new Date();
    date.setTime(date.getTime() + time);
    document.cookie = name + "; expires=" + date.toUTCString()+"; path=/;";
}
function CheckCookiePop() {
    var PopCookie = getCookie("CookiePop");
    if (PopCookie) {
        CookiePop.style.display = "none";
    }
} function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}