
function WireLoginPostMessage(sso_host) {
    window.addEventListener('message', event => {
        // if (event.origin.startsWith(sso_host)) {            
        var k = event.data[0];
        var v = event.data.substr(2);
        console.log(event.data);
        switch (k) {
            case 'e'://external login
            case 'r'://register
                document.location = sso_host + v;
                break;
            case 'c'://close
                var frame = document.getElementById("frmLogin");
                frame.parentNode.removeChild(frame);
                $('body').removeClass('popup-active show-fade loading');
                break;
            case 'o'://logout                    
                //document.location.reload();
                document.location = "/";
                break;
        }
        // }
    });
}

function HideSSO() {
    var frmLogin = document.getElementById("frmLogin");
    frmLogin.parentNode.removeChild(frmLogin);
    $('body').removeClass('popup-active show-fade loading');
}

function ReloadAfterLogin() {
    document.location.reload();
}

$.widget("widgets.LoginFrameLink", {
    _create: function () {

        this.element.on("click", function (e) {
            var isMacLike = /(Mac|iPhone|iPod|iPad)/i.test(navigator.platform);
            var isIOS = /(iPhone|iPod|iPad)/i.test(navigator.platform);
            if (isMacLike || isIOS) {
                console.log(window.location);
                window.location.href = "/login?returnUrl=" + window.location.pathname;
            }
            else {
                var $body = $('body');
                $body.addClass('popup-active show-fade loading');
                var FrameContainer = document.createElement("div");
                var loginframe = document.createElement("iframe");
                FrameContainer.setAttribute("id", "frmLogin");
                loginframe.setAttribute("src", "/vowosadmin/Surface/Account/ExternalLogin?frame=1&returnUrl=/stopka/signin");
                loginframe.setAttribute("scrolling", "no");
                loginframe.setAttribute("allowtransparency", "true");
                var link = document.createElement("a");
                link.setAttribute("onclick", "HideSSO();return false;")
                link.setAttribute("href", "#");
                FrameContainer.className = "login-frame";
                var icon = document.createElement("img");
                icon.setAttribute("src", "/Images/close-fs.png");
                link.append(icon);
                FrameContainer.append(link);
                FrameContainer.append(loginframe);
                $body.append(FrameContainer);
                loginframe.onload = function () {
                    $body.removeClass('loading');
                };
            }
        });
    },
});

$(function () {
    $('.js-loginFrame').LoginFrameLink();
});






$.widget("widgets.LogoutFrameLink", {
    _create: function () {

        this.element.on("click", function (e) {
            //document.cookie = "yourAuthCookie=;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/";
            location.reload();
            var $body = $('body');
            //$body.addClass('popup-active show-fade loading');
            var FrameContainer = document.createElement("div");
            FrameContainer.setAttribute("id", "frmLogin");
            var logoutframe = document.createElement("iframe");
            logoutframe.onload = function () {
                $body.removeClass('loading');
            };
            logoutframe.setAttribute("id", "frmLogin");
            logoutframe.setAttribute("src", "/vowosadmin/Surface/Account/Logout");
            logoutframe.setAttribute("scrolling", "no");
            logoutframe.setAttribute("allowtransparency", "true");
            logoutframe.className = "login-frame";
            var link = document.createElement("a");
            link.setAttribute("onclick", "HideSSO();return false;")
            link.setAttribute("href", "#");
            FrameContainer.className = "login-frame";
            var icon = document.createElement("img");
            icon.setAttribute("src", "/Images/close-fs.png");
            link.append(icon);
            FrameContainer.append(link);
            FrameContainer.append(logoutframe);
            $body.append(FrameContainer);
        });
    },
});

$(function () {
    $('.js-logoutFrame').LogoutFrameLink();
});
