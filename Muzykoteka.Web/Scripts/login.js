var token;
var ref_path;
var skip_login_counter = false;

function InitLogin(_token, _ref_path) {      
    token = _token;
    ref_path = _ref_path;   
    window.addEventListener("message", receiveToken, false);
}

function receiveToken(event) {
  
    if (event.origin !== ssourl)
        return;

    if (event.data == 'reload') {
        skip_login_counter = true;
        $frame = $('#login_frame');
        $frame.attr('src', $frame.attr('src'));        
		return;
    }

    if (event.data == 'logout') {
        if (token.length > 0) {
            $('#login_token').val('');
            $('#loginform').submit();
        }
        return;
    }

    if (event.data.length <= 0) {
        //console.log('Empty login token');
        return;
    }

    if (token != event.data) {       
        //console.log(ssoapi + '/v1/User/GetUserData?platformCodename=www&tokenValue=' + event.data + '&source=' + ref_path);
        ShowLoggingOn();
        $.ajax({
            url: ssoapi + '/v1/User/GetUserData?platformCodename=www&tokenValue=' + event.data + '&source=' + ref_path,
            dataType: 'json',
            cache: false,
            success: function (data) {                                              
                var email = '';
                var success = false;
                $.each(data, function (index, element) {                    
                    if (index == 'UserData') {
                        $.each(element, function (index, el) {
                            if (index == 'Login') {
                                email = el;
                            }
                        })
                    }
                    if (index == 'Success') {
                        success = element;
                    }
                });
                if (success && email.length > 0) {                    
                    $('#login_token').val(event.data);
                    $('#login_email').val(email);                    
                    skip_login_counter ? LoginNReload() : StartReloadCountdown();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {                              
                console.log(thrownError);
            }
        });
    }
}

var reload_countdown;

function UpdateReloadCounter() {
    reload_countdown -= 1;
    $('.login_countdown').text(reload_countdown);
    if (reload_countdown <= 0)
        LoginNReload();
    else {        
        setTimeout(UpdateReloadCounter, 1000);
    }        
}

function LoginNReload() {
    $('#loginform').submit();
}

function StartReloadCountdown() {        
    reload_countdown = 6;
    UpdateReloadCounter();
    $('.login_reload').fadeIn();    
}

function ShowLoggingOn() {
    $('.login_link').hide();
    $('.logging_on').show();
}

function OpenSSOPopup(url) {
    var w = 600;
    var h = 800;
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    window.open(url, "SSOPopup", "width=" + w + ",height=" + h + ",toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=" + left + ",top=" + top).focus();
}
