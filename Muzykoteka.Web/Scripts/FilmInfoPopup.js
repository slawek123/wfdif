(function($){

	MoviePopupAdd();
	
})(jQuery);
function MoviePopupAdd() {

	var filminfoPopupView = function (options) {

		var that = {
			$el: $(document.createElement('form'))
		};
		that = $.extend(that, options);

		that.$el.addClass('filminfo-form def_popup');

		that.$el.html($('#filminfo-popup-template').html());

		that.$el.find('.fi-title').html(options.title);

		that.$el.find('.fi-lead').html(options.lead.length <= 0 ? "(brak opisu)" : options.lead);

		that.$el.find('.close-button').on('click', function () {
			that.close();
		});

		that.$el.on('click', function () {
			that.close();
		});

		var $body = $('body');

		that.render = function () {
			that.$el.find('.fi-lead-container').css('max-height', document.documentElement.clientHeight - 140);
			that.$el.find('.fi-lead-container').css('max-width', document.documentElement.clientWidth - 40);
			$body.append(that.$el);
			$body.addClass('popup-active');

			setTimeout(function () {
				$body.addClass('show-fade');
				that.$el.addClass('visible');
			}, 50);
			window.popups.filminfoPopupActive = true;
		};

		that.close = function () {
			that.$el.removeClass('visible');
			setTimeout(function () {
				that.$el.remove();
			}, 1000);
			window.popups.filminfoPopupActive = false;

			$body.removeClass('show-fade');
			setTimeout(function () {
				$body.removeClass('popup-active');
			}, 500);
		};

		return that;
	};

	window.popups = window.popups || {};
	window.popups.filminfoPopup = filminfoPopupView;
	window.popups.filminfoPopupActive = false;

	$(function () {
		var $trigger = $('.js-filminfo');
		$trigger.on('click', function (ev) {
			ev.preventDefault();
			if (!window.popups.filminfoPopupActive) {
				var popup = filminfoPopupView({ lead: $(this).data("lead"), title: $(this).data("title") });
				popup.render();
			}
		});
	});
}