$.widget("widgets.MasterSearch", {
    options: {
        filter1: '',
        filter2: '',
        filter1Text: '',
        filter2Text: '',
        h: 0
    },

    _create: function () {

        var me = this;

        var submit = this.element.find('.submit-msearch'),
            input = this.element.find('input[name="s"]'),
            clearf = this.element.find('.searchFiltersNone'),
            close = this.element.find('.close'),
            writer = this.element.find(".writer");


        this.element.on("submit", function (e) {
            if (!input.val().length) {
                e.preventDefault();
            }
        });

        close.on('click', function (e) {
            me.element.css('display', 'none');
        });

        submit.on("click", function (e) {
            input.val(encodeURI(writer.val()));
            me.element.submit();
        });

        clearf.on('click', function (e) {
            me.element.find('.js-combo').ComboBox('ClearSelection');
            $(this).toggleClass('selected', true);
        });
        writer.keypress(function (e) {
            if (e.keyCode == 13) {
                input.val(encodeURI(writer.val()));
                me.element.submit();
            }
        });

        if (this.options.filter1 != null && this.options.filter1.length > 0) {
            this.element.find('input[name="t"]').val(String(this.options.filter1).replace(/,/g, '|'));
            me.element.find('#fltSearchType').ComboBox('SetSelection', this.options.filter1);
        }
        if (this.options.filter2 != null && this.options.filter2.length > 0) {
            this.element.find('input[name="e"]').val(String(this.options.filter2).replace(/,/g, '|'));
            me.element.find('#fltSearchEpoka').ComboBox('SetSelection', this.options.filter2);
        }
    },

    Show: function () {
        this.options.h = $(window).height();
        this.element.css('display', 'inline');
        this.element.find('.js-combo').ComboBox('UpdateWidth');
        this.element.find('input[name="s"]').focus();
    },

    Filter1Changed: function (sender) {
        this.options.filter1Text = sender.GetSelectedTexts();
        this.element.find('.searchFiltersNone').toggleClass('selected', false);
        this.element.find('input[name="t"]').val(String(sender.GetSelectedValues()).replace(/,/g, '|'));
    },

    Filter2Changed: function (sender) {
        this.options.filter2Text = sender.GetSelectedTexts();
        this.element.find('.searchFiltersNone').toggleClass('selected', false);
        this.element.find('input[name="e"]').val(String(sender.GetSelectedValues()).replace(/,/g, '|'));
    },

    Refresh: function (k, e) {
        FilterByEK(k, e);
    },
    Hide: function () {
        this.element.css('display', 'none');
    }
});



$(function () {
    $('.js-search-input').MasterSearch();
});
$(document).mouseup(function (e) {
    var container = $('.js-search-input');
    $.each(container, function (index, item) {
        const value = $(item);
        if (!value.is(e.target) && value.has(e.target).length === 0) {
            $(value).MasterSearch('Hide');
        }
    })
});