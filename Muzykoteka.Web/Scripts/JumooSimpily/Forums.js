﻿//
// Jumoo.Simpily.Forums - Javascript helpers and editor setup
//
$(document).ready(function () {


    $(".post-delete").click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        var postId = $(this).data("postid");
        deletePost(postId)
    });
});

function deletePost(postId) {
    if (window.confirm("Na pewno chcesz usunąć ten post?")) {
        $.ajax({
            url: '/VowosAdmin/Api/SimpilyForumsApi/DeletePost/' + postId,
            type: 'DELETE',
            success: function (data) {
                $('#post_' + postId).fadeOut();
            },
            error: function (data) {
                alert("Przepraszymy, Wystąpił błąd")
            }
        });
    }
}