$.widget("widgets.FaqToggle", {
    options: {

    },


    _create: function () {                

        var qst = this.element.find('.js-question');

        qst.on("click", function (e) {
            var faqsingle = $(this).parent();
            faqsingle.toggleClass("expanded");
            var isExpanded = faqsingle.hasClass("expanded");
            $(this).attr("aria-expanded", isExpanded);
        });
    },
});
