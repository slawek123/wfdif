$.widget("widgets.Gallery", {
    options: {
        thumbWidth: 433,
        thumbSmallWidth: 410,
        sepWidth: 23,
        thumbHeight: 257,
        nullText: '',
    },


    _create: function () {

        var me = this;

        var nav = this.element.find('.nav'),
            nav_prev = this.element.find('.js-prev'),
            nav_next = this.element.find('.js-next'),
            thumbnails = this.element.find('.js-thumbnails'),
            thumbnailsOuter = this.element.find('.js-thumbnailsOuter'),
            showfs = this.element.find('.js-fullscreen'),
            closefs = this.element.find('.js-closefs'),
            thumb = this.element.find('.js-thumb'),
            counterTotal = this.element.find('.js-counter-total');

        if (thumb.length >= 1) {
            this.options.thumbSmallWidth = (thumb.first().outerWidth());
            this.options.thumbWidth = this.options.thumbSmallWidth + this.options.sepWidth;
        }

        var item_count = thumb.length;
        var thum_width = item_count * me.options.thumbWidth;
        thumbnails.css('width', thum_width);

        counterTotal.text(item_count);

        showfs.on("click", function (e) {
            me.ToggleFullscreen();
        });

        closefs.on("click", function (e) {
            me.ToggleFullscreen();
        });

        nav_next.on("click", function (e) {
            var left = parseInt(thumbnails.css('left'));
            var thumbOuterWidth = thumbnailsOuter.width();
            var thum_width = item_count * me.options.thumbWidth;
            var right_overflow = thum_width + left - thumbOuterWidth - me.options.sepWidth;
            if (right_overflow > 0) {
                left = (right_overflow > me.options.thumbWidth - me.options.sepWidth ? left - me.options.thumbWidth : thumbOuterWidth - thum_width + me.options.sepWidth);
                thumbnails.animate({
                    left: left
                }, 300, "swing");
                me._updateCounter(left);

            }
        });

        nav_prev.on("click", function (e) {
            var left = parseInt(thumbnails.css('left'));
            if (left < 0) {
                left = left + me.options.thumbWidth;
                if (left > 0)
                    left = 0;
                thumbnails.animate({
                    left: left
                }, 300, "swing");
                me._updateCounter(left);
            }
        });

        thumb.on("click", function () {
            var idx = $(this).index() / 2;
            me.ToggleFullscreen(idx);
        });

        var navVisible = 0;

        $(window).resize(function () {
            thumbnails.css('left', 0);
            if (thumbnailsOuter.width() > thum_width) {
                if (navVisible >= 0) {
                    navVisible = -1;
                    nav.toggleClass('visible', false);
                }
            }
            else {
                if (navVisible <= 0) {
                    navVisible = 1;
                    nav.toggleClass('visible', true);
                }
            }
        });

        setTimeout(function () {
            nav.toggleClass('visible', thumbnailsOuter.outerWidth() < thum_width);
        }, 1);
    },

    _updateCounter: function (left) {
        var idx = -Math.round(left / this.options.thumbWidth) + 1;
        this.element.find('.js-counter-current').text(idx);
    },

    OnFSOutClick: function (e) {
        var container = $('.fs-content');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('.js-gallery').Gallery('ToggleFullscreen');
        }
    },

    ToggleFullscreen: function (idx) {
        
        var thumbnailsOuter = this.element.find('.js-thumbnailsOuter');
        var fscontent = this.element.find('.fs-content');
        var next = this.element.find('.next');
        var prev = this.element.find('.prev');
        var thumbs = this.element.find('.js-thumb');
        var thumbnails = this.element.find('.js-thumbnails');
        var nav = this.element.find('.nav');      
        if (!idx)
            idx = parseInt(this.element.find('.js-counter-current').text()) - 1;        
        if (this.element.hasClass('fullscreen')) {                        
            this.element.appendTo(this.sourceParent);
            this.element.removeClass('fullscreen');
            thumbnailsOuter.css('height', this.options.thumbHeight);
            fscontent.css('width', '').css('height', '');
            next.css('top', 0);
            prev.css('top', 0);
            thumbs.css('width', this.options.thumbSmallWidth).css('height', this.options.thumbHeight);
            thumbs.find('img').css('max-height', this.options.thumbHeight).css('max-width', this.options.thumbSmallWidth);
            this.options.thumbWidth = this.options.thumbSmallWidth + this.options.sepWidth;
            var item_count = thumbs.length;
            var thum_width = item_count * this.options.thumbWidth;
            thumbnails.css('width', thum_width);

            $(document).off('mouseup', this.OnFSOutClick);

            nav.toggleClass('visible', thumbnailsOuter.width() < thum_width);            
        }
        else {
            this.sourceParent = this.element.parent();
            this.element.appendTo('body');
            this.element.addClass('fullscreen');
            var win = $(window);
            var gallery = this.element.find('.js-gallery');
            var MarginWidth;
            var ButtonsWidth;
            var mobileOffset;
            if (win.width() < 1200) {
                MarginWidth = 0;
                ButtonsWidth = 0;
                mobileOffset = 75;
                this.element.find('.js-closefs').css("width", 25);
            }
            else {
                mobileOffset = 0;
                ButtonsWidth = 70;
                MarginWidth = 40;
            }
            var top_w = win.width() - MarginWidth;
            var top_h = win.height() - MarginWidth;
            var w, h;
            if (top_w / top_h < 16 / 9) {
                w = top_w;
                h = Math.round(w * 9 / 16);
            }
            else {
                if (win.width() < 1200)
                    $(".fullscreen").css("margin-top", 30);
                h = top_h - mobileOffset;
                w = Math.round(h * 16 / 9);
            }

            var hthumb = h - ButtonsWidth;
            fscontent.css('width', w).css('height', h);
            thumbnailsOuter.css('height', hthumb);
            next.css('top', (hthumb - 106) / 2);
            prev.css('top', (hthumb - 106) / 2);
            thumbs.css('width', w).css('height', hthumb);
            thumbs.find('img').css('max-height', hthumb).css('max-width', w);
            this.options.thumbWidth = w;
            var item_count = thumbs.length;
            var thum_width = item_count * this.options.thumbWidth;
            thumbnails.css('width', thum_width);

            $(document).on('mouseup', this.OnFSOutClick);

            nav.toggleClass('visible', item_count > 0);
        }

        var left;        
        if (idx) {
            left = -idx * this.options.thumbWidth;
        }
        else {
            left = parseInt(thumbnails.css('left'));            
            left = Math.round(left / this.options.thumbWidth) * this.options.thumbWidth;
            if (left > 0)
                left = 0;            
        }
        var thumbOuterWidth = thumbnailsOuter.width();
        if (left < thumbOuterWidth - thum_width)
            left = thumbOuterWidth - thum_width;
        thumbnails.css('left', left);

        this._updateCounter(left);
    }
});
