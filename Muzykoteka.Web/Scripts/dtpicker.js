$.widget("widgets.MZDatePicker", {
    options: {
        isMultiselect: false,
        FilterField: '',
        nullText: '',
    },


    _create: function () {

        var me = this;

        var combo = this.element,
            drop = this.element.find('.js-drop'),
            list = this.element.find('.js-list'),
            date_od = this.element.find('.dtOd'),
            date_do = this.element.find('.dtDo');
            
        
        var max_width = Math.max(drop.outerWidth(), list.outerWidth());
        combo.css('width', max_width + 20);
        list.css('width', max_width + 20);
        
        date_od.datepicker({
            dateFormat: "yy-mm-dd",
            dayNamesMin: ["Nd", "Pn", "Wt", "&#346;r", "Cz", "Pt", "So"],
            monthNames: ["Stycze&#324;", "Luty", "Marzec", "Kwiecie&#324;", "Maj", "Czerwiec", "Lipiec", "Sierpie&#324;", "Wrzesie&#324;", "Pa&#378;dziernik", "Listopad", "Grudzie&#324;"],
            nextText: "Nast&#281;pny",
            prevText: "Poprzeni",            
            onSelect: function (sdate, obj) {
                var dateAsObject = $(this).datepicker('getDate');
                drop.toggleClass('selected-any', dateAsObject != null);
                me._trigger("DateFromChanged", null, { date: dateAsObject });                
            },
        });
        
        date_do.datepicker({
            dateFormat: "yy-mm-dd",
            dayNamesMin: ["Nd", "Pn", "Wt", "&#346;r", "Cz", "Pt", "So"],
            monthNames: ["Stycze&#324;", "Luty", "Marzec", "Kwiecie&#324;", "Maj", "Czerwiec", "Lipiec", "Sierpie&#324;", "Wrzesie&#324;", "Pa&#378;dziernik", "Listopad", "Grudzie&#324;"],
            nextText: "Nast&#281;pny",
            prevText: "Poprzeni",
            onSelect: function (sdate, obj) {
                var dateAsObject = $(this).datepicker('getDate');
                drop.toggleClass('selected-any', dateAsObject != null);
                me._trigger("DateToChanged", null, { date: dateAsObject });                
            },
        });

        drop.on("click", function (e) {
            var cmb = $(this).parent();
            if (cmb.hasClass('up')) {
                cmb.removeClass('up');
                cmb.addClass('down');
            }
            else {
                cmb.removeClass('down');
                cmb.addClass('up');
            }
        });        
    },


    _hide: function () {
        var cmb = this.element;

        if (cmb.hasClass('down')) {
            cmb.removeClass('down');
            cmb.addClass('up');
        }
    },


    Hide: function () {
        this._hide();
    },


    ClearSelection: function () {        
        var drop = this.element.find('.js-drop');
        drop.toggleClass('selected-any', false);
        var date_od = this.element.find('.dtOd');
        var date_do = this.element.find('.dtDo');                        
        date_od.val('');
        date_do.val('');
    },


    SetSelection: function (selectedValues) {        
        var date_od = this.element.find('.dtOd').datepicker("getDate");
        var date_do = this.element.find('.dtDo').datepicker("getDate");
        var ar = selectedValues.split(',');
        var selectedAny = false;
        if (ar.length == 2) {
            if (ar[0].length > 0) {                
                date_od.setTime(ar[0]);
                selectedAny = true;
            }
            if (ar[1].length > 0) {
                date_do.setTime(ar[0]);
                selectedAny = true;
            }
        }
        var drop = this.element.find('.js-drop');
        drop.toggleClass('selected-any', selectedAny);
    },


    GetSelectedValues: function () {
        var date_od = this.element.find('.dtOd').datepicker("getDate");
        var date_do = this.element.find('.dtDo').datepicker("getDate");
        var ret = '';
        if (date_od != null) {
            ret += date_od.getTime();
        }
        ret += ',';
        if (date_do != null) {            
            ret += date_do.getTime();
        }        
        return ret;
    },
   
    GetFilterExpr: function () {

        var date_od = this.element.find('.dtOd').datepicker("getDate");
        var date_do = this.element.find('.dtDo').datepicker("getDate");
        var ret='';
        if (date_od != null) {            
            date_od.setHours(0);
            date_od.setMinutes(0);
            date_od.setSeconds(0);
            date_od.setMilliseconds(0);
            ret = this.options.FilterField + '>=' + date_od.getTime();
        }
        if (date_do != null) {            
            date_do.setHours(23);
            date_do.setMinutes(59);
            date_do.setSeconds(59);
            date_do.setMilliseconds(999);
            if (ret.length > 0)
                ret += '|';
            ret += this.options.FilterField + '<=' + date_do.getTime();
        }
        return ret;
    }
});

$(document).mouseup(function (e) {
    
    var container = $('.js-dtpicker');
    var containerDTP = $('.ui-datepicker');
    if (!container.is(e.target) && container.has(e.target).length === 0 && !containerDTP.is(e.target) && containerDTP.has(e.target).length === 0)    
    {
        $('.js-dtpicker').MZDatePicker('Hide');
    }    
});