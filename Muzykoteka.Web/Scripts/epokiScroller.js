$.widget("widgets.EpokiScroller", {
    options: {

    },


    _create: function () {

        var slider = this.element.find('#slider'),
            panel_outer = this.element.find('.epoki'),
            panel = this.element.find('.epoki-inner'),
            epoki = panel.find('.epoka');

        var nav_prev = this.element.find('.nav-prev'),
            nav_next = this.element.find('.nav-next');

        var ewidth = epoki.length * ($(epoki[0]).width() + 19);

        var mobile_item_w = 324;

        var e_mobile_width = epoki.length * 322;

        panel.width(2 * ewidth);

        slider.slider({
            slide: function (event, ui) {
                var out_w = panel_outer.width();
                if (out_w > ewidth) {
                    panel.css('left', 0);
                }
                else {
                    panel.css('left', -ui.value * (ewidth - out_w) / 100);
                }
            }
        });


        nav_next.on("click", function (e) {
            var out_w = panel_outer.width();
            if (out_w > e_mobile_width) {
                panel.css('left', 0);
            }
            else {
                var max = out_w - e_mobile_width - 12;
                var left = parseInt(panel.css('left'));
                var new_left = left - mobile_item_w;
                if (new_left < max)
                    new_left = max;
                panel.animate({ "left": new_left }, 300);
            }
        });

        nav_prev.on("click", function (e) {
            var out_w = panel_outer.width();
            if (out_w > e_mobile_width) {
                panel.css('left', 0);
            }
            else {
                var left = parseInt(panel.css('left'));
                var new_left = left + mobile_item_w;
                if (new_left > 0)
                    new_left = 0;
                panel.animate({ "left": new_left }, 300);
            }
        });
    },
});
