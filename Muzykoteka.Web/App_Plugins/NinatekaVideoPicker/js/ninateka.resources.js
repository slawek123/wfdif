angular.module('umbraco.resources').factory('ninatekaResource', 
    function ($q, $http, umbRequestHelper) {
        return {            
            getCategories: function () {
                return umbRequestHelper.resourcePromise(
                    $http.get("backoffice/vw/NinatekaEntity/GetCategories"),
                    'Server call failed for getting ninateka categories');
            },
            
            getByCodename: function (codename) {
                return umbRequestHelper.resourcePromise(
                    $http.get( "backoffice/vw/NinatekaEntity/GetByCodename?codename=" + codename),
                    'Server call failed for getting ninateka entities');
            },

            getAll: function (options) {

                var defaults = {
                    pageSize: 0,
                    pageNumber: 0,
                    filter: '',
                    categories: '',
                    orderDirection: "Ascending",
                    orderBy: ''
                };
                if (options === undefined) {
                    options = {};
                }

                angular.extend(defaults, options);

                options = defaults;

                if (options.orderDirection === "asc") {
                    options.orderDirection = "Ascending";
                }
                else if (options.orderDirection === "desc") {
                    options.orderDirection = "Descending";
                }

                return umbRequestHelper.resourcePromise(
                    $http.get( "backoffice/vw/NinatekaEntity/GetAll?"
                        + umbRequestHelper.dictionaryToQueryString([
                            { pageNumber: options.pageNumber },
                            { pageSize: options.pageSize },
                            { orderBy: options.orderBy },
                            { orderDirection: options.orderDirection },
                            { filter: options.filter },
                            { categories: options.categories },                            
                        ])),
                    'Server call failed for getting ninateka entities');

            }
        };
    }
); 