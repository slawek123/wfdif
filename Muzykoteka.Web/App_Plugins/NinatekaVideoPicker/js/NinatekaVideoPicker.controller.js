﻿angular.module("umbraco").controller("vw.NinatekaVideoPicker.Controller",
    function ($scope, $rootScope, $routeParams, ninatekaResource) {
               
        $scope.listViewResultSet = {
            totalPages: 0,
            items: []
        };

        $scope.lastSelected;

        $scope.categories = [];
        
        $scope.pagination = [];

        $scope.options = {
            //selectedCodename: 0,
            pageSize: 10,
            pageNumber: ($routeParams.page && Number($routeParams.page) != NaN && Number($routeParams.page) > 0) ? $routeParams.page : 1,
            filter: '',
            categories: '',
            orderBy: '2',
            orderDirection: 'asc',
        };

        $scope.actionInProgress = false;

        $scope.onCategoryChange = function () {
            $scope.options.categories = '';
            _.each($scope.categories, function (e, i) {
                if (e.checked) {
                    $scope.options.categories += e.Id + ',';
                }
            });
        };

        $scope.selectCheck = function (record) {
                            
            console.log("SELECT CHECK");

            if (!record.selected) {
                $scope.lastSelected = null;
                //$scope.options.selectedCodename = null;
                $scope.model.value = null;
                $rootScope.$broadcast("ninatekav.changed", { metadata: null });
            }
            else {
                if ($scope.lastSelected && $scope.lastSelected.Id != record.Id) {
                    $scope.lastSelected.selected = false;
                }
                $scope.lastSelected = record;                
                $scope.model.value = record.Codename + '|' + record.Title;
                $rootScope.$broadcast("ninatekav.changed", { metadata: record.Metadata });
            }            
        };

        $scope.select = function (record) {         
            record.selected = !record.selected;
            $scope.selectCheck(record);
        };

        $scope.isSortDirection = function (col, direction) {
            return $scope.options.orderBy.toUpperCase() == col.toUpperCase() && $scope.options.orderDirection == direction;
        };

        $scope.next = function () {
            if ($scope.options.pageNumber < $scope.listViewResultSet.totalPages) {
                $scope.options.pageNumber++;
                $scope.reloadView();
            }
        };

        $scope.goToPage = function (pageNumber) {
            $scope.options.pageNumber = pageNumber + 1;
            $scope.reloadView();
        };

        $scope.sort = function (field, allow) {
            if (allow) {
                $scope.options.orderBy = field;

                if ($scope.options.orderDirection === "desc") {
                    $scope.options.orderDirection = "asc";
                }
                else {
                    $scope.options.orderDirection = "desc";
                }

                $scope.reloadView();
            }
        };

        $scope.prev = function () {
            if ($scope.options.pageNumber > 1) {
                $scope.options.pageNumber--;
                $scope.reloadView();
            }
        };

        $scope.enterSearch = function ($event) {
            $($event.target).next().focus();
        }

        $scope.reloadCategories = function () {
            ninatekaResource.getCategories().then(function (data) {
                $scope.categories = data;
            });
        };

        $scope.refresh = function () {
            $scope.reloadView ();
        };

        $scope.getSelectedCodename = function () {
            var idx = $scope.model.value.indexOf('|');
            if (idx > 0) 
                return $scope.model.value.substr(0, idx);            
            else
                return null;
        };

        $scope.reloadView = function () {                       

            $scope.actionInProgress = true;

            ninatekaResource.getAll($scope.options).then(function (data) {

                $scope.listViewResultSet = data;
                
                if ($scope.listViewResultSet.items && $scope.model.value) {                    
                    var selectedCodename = $scope.getSelectedCodename();
                    _.each($scope.listViewResultSet.items, function (e, index) {
                        if (e.Codename == selectedCodename) {
                            e.selected = true;
                            $scope.lastSelected = e;
                            return false;
                        }
                    });
                }

                if ($scope.options.pageNumber > $scope.listViewResultSet.totalPages) {
                    $scope.options.pageNumber = $scope.listViewResultSet.totalPages;                   
                    $scope.reloadView();
                }

                $scope.pagination = [];
                
                if ($scope.listViewResultSet.totalPages <= 10) {
                    for (var i = 0; i < $scope.listViewResultSet.totalPages; i++) {
                        $scope.pagination.push({
                            val: (i + 1),
                            isActive: $scope.options.pageNumber == (i + 1)
                        });
                    }
                }
                else {                    
                    var maxIndex = $scope.listViewResultSet.totalPages - 10;                    
                    var start = Math.max($scope.options.pageNumber - 5, 0);                    
                    start = Math.min(maxIndex, start);
                    for (var i = start; i < (10 + start) ; i++) {
                        $scope.pagination.push({
                            val: (i + 1),
                            isActive: $scope.options.pageNumber == (i + 1)
                        });
                    }                    
                    if (start > 0) {
                        $scope.pagination.unshift({ name: "First", val: 1, isActive: false }, { val: "...", isActive: false });
                    }                    
                    if (start < maxIndex) {
                        $scope.pagination.push({ val: "...", isActive: false }, { name: "Last", val: $scope.listViewResultSet.totalPages, isActive: false });
                    }
                }

                $scope.actionInProgress = false;
            });
        };

        $scope.loadSelected = function (codename) {
            $scope.actionInProgress = true;
            ninatekaResource.getByCodename(codename).then(function (data) {

                $scope.listViewResultSet = data;

                if ($scope.listViewResultSet.items.length > 0) {
                    $scope.listViewResultSet.items[0].selected = true;
                }

                $scope.pagination = [];

                $scope.actionInProgress = false;
            });
        };

        $scope.reloadCategories();

        if ($scope.model.value) {
            $scope.loadSelected($scope.getSelectedCodename());
        }        
});
