﻿angular.module("umbraco").controller("UIOMatic.FieldFilters.Dropdown",
    function ($scope, uioMaticObjectResource) {

        uioMaticObjectResource.getFilterLookup($scope.property.typeAlias, $scope.property.keyPropertyName, $scope.property.key).then(function (response) {
            $scope.items = response;
            for (var i = 0; i < $scope.items.length; i++) {
                var item = $scope.items[i];
                if (item.value == true || item.value == false) {
                    if (item.value)
                        item.value = "Tak";
                    else
                        item.value = "Nie";
                }
            }
        });

    });