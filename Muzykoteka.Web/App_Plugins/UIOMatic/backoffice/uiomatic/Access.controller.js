﻿angular.module("umbraco")
    .controller("uioMatic.ObjectAccessController",
        function ($scope, uioMaticObjectResource, navigationService, treeService) {
            $scope.Active = function (type, id, Access) {
                var arr = [];
                arr.push(id);
                uioMaticObjectResource.ActiveByIds(type, arr, Access).then(function () {
                    navigationService.hideNavigation();
                });
            };
        });