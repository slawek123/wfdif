﻿angular.module("umbraco").controller("KeyValueEditorController", function ($scope, dialogService) {

    $scope.model.value = $scope.model.value || [];

    if (!($scope.model.value instanceof Array))
        $scope.model.value = [];

    $scope.$on("ninatekav.changed", function (event, data) {
        if (data.metadata != null) {
            $scope.model.value = [];
            for (var item in data.metadata) {
                $scope.model.value.push({ key: data.metadata[item][0], value: data.metadata[item][1] });
            }
        }
    });

    $scope.addKeyValue = function () {

        dialogService.open({ template: '/App_Plugins/KeyValueEditor/KeyValueEditorDialogView.html', show: true, callback: done, dialogData: null });

        function done(data) {
            if (data.key && data.value)
                $scope.model.value.push({ key: data.key, value: data.value });
        }

    };

    $scope.editKeyValue = function (item) {

        dialogService.open({ template: '/App_Plugins/KeyValueEditor/KeyValueEditorDialogView.html', show: true, callback: done, dialogData: { key: item.key, value: item.value } });

        function done(data) {
            if (data.key && data.value) {

                for (var i = 0; i < $scope.model.value.length; i++) {
                    if ($scope.model.value[i].key == item.key) {
                        $scope.model.value[i] = data;
                        break;
                    }
                }
            }
        }

    };

    $scope.deleteKeyValue = function (item) {
        var idx = $scope.model.value.indexOf(item);
        if (idx > -1) {
            $scope.model.value.splice(idx, 1);
        }                
    };

    $scope.moveUpKeyValue = function (item) {
        var idx = $scope.model.value.indexOf(item);
        if (idx > 0) {         
            var r = $scope.model.value[idx-1];
            $scope.model.value[idx - 1] = $scope.model.value[idx];
            $scope.model.value[idx] = r;
        }
    };

    $scope.moveDownKeyValue = function (item) {
        var idx = $scope.model.value.indexOf(item);
        if (idx < $scope.model.value.length-1)
        {
            var r = $scope.model.value[idx + 1];
            $scope.model.value[idx + 1] = $scope.model.value[idx];
            $scope.model.value[idx] = r;
        }
    };
});