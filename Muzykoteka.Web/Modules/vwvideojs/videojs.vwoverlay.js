var VjsButton = videojs.getComponent('Button');
var FBFButton = videojs.extend(VjsButton, {
    constructor: function(player, options) {
        VjsButton.call(this, player, options);
        this.player = player;
        this.frameTime = 1/options.fps;
        this.step_size = options.value;
        this.on('click', this.onClick);
    },

    onClick: function() {        
        this.player.pause();             
        var position;
        switch (this.step_size) {
            case -1:
            case 1:
                position = this.player.currentTime() + this.frameTime * this.step_size;
                break;

            case -2:
                position = 0;
                break;
            case 2:
                position = this.player.duration();                
                break;
        }        
        this.player.currentTime(position);
    },
});

function vwoverlay(options) {
    var player = this,
        frameTime = 1 / 30; // assume 30 fps

    player.getFPS = function () {
        return options.fps;
    }
  
    player.ready(function() {
        options.steps.forEach(function(opt) {
            player.controlBar.addChild(            
                new FBFButton(player, {
                    el: videojs.dom.createEl(
                        'button',
                        {
                            className: 'vjs-res-button vjs-control vjs-button',
                            innerHTML: '<span class="vjs-icon-placeholder ' + opt.clname + '"></span>',     
                            title: opt.title,
                        },
                        {
                            role: 'button', type: 'button'
                        }
                    ),
                    value: opt.step,
                    fps: options.fps,
                }),
            {}, opt.index);
        });
    });
}
videojs.registerPlugin('vwoverlay', vwoverlay);
