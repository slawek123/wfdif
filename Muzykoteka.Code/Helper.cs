﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Newtonsoft.Json;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Muzykoteka.Code.Services;
using System.Xml.XPath;
using umbraco;
using System.Web.Configuration;
using System.Web;

namespace Muzykoteka.Code
{

    public enum StoreLinkType
    {
        Rules,
        Shipments
    };

    public class MKeyVal
    {
        public object Id { get; set; }
        public string Name { get; set; }
    };
    public static class Helper
    {
        public static List<KeyValuePair<object, object>> GetSlownik(string name)
        {
            var cached = System.Web.HttpContext.Current.Cache["sl_" + name];
            if (cached != null)
            {
                return (List<KeyValuePair<object, object>>)cached;
            }
            else
            {
                var ret = UmbracoContext.Current.ContentCache.GetAtRoot()
                          .Where(x => x.DocumentTypeAlias == "Zasoby")
                          .FirstOrDefault()
                          .Children
                          .Where(x => x.DocumentTypeAlias == "Slowniki")
                          .FirstOrDefault()
                          .Children
                          .Where(x => x.Name.ToLower() == name.ToLower())
                          .FirstOrDefault()
                          .Children
                          .Select<IPublishedContent, KeyValuePair<object, object>>(x => new KeyValuePair<object, object>(x.Id, x.Name))
                          .ToList();
                System.Web.HttpContext.Current.Cache["sl_" + name] = ret;
                return ret;
            }
        }

        /*
        public static string GetSlownikEntry(string slownik, int id)
        {
            return UmbracoContext.Current.ContentCache.GetAtRoot()
              .Where(x => x.DocumentTypeAlias == "Zasoby")
              .FirstOrDefault()
              .Children
              .Where(x => x.DocumentTypeAlias == "Slowniki")
              .FirstOrDefault()
              .Children
              .Where(x => x.Name == slownik)
              .FirstOrDefault()
              .Children
              .Where(x => x.Id == id)
              .Select<IPublishedContent, string>(x => x.Name)
              .SingleOrDefault();
        }
        */
        public static IEnumerable<string> GetSlownikEntries(string slownik, string ids)
        {
            if (String.IsNullOrEmpty(ids))
            {
                return new List<string>();
            }
            else
            {
                int[] lstINTs = ids.Split(',').Select<string, int>(x => Convert.ToInt32(x)).ToArray();

                return UmbracoContext.Current.ContentCache.GetAtRoot()
                  .Where(x => x.DocumentTypeAlias == "Zasoby")
                  .FirstOrDefault()
                  .Children
                  .Where(x => x.DocumentTypeAlias == "Slowniki")
                  .FirstOrDefault()
                  .Children
                  .Where(x => x.Name == slownik)
                  .FirstOrDefault()
                  .Children
                  .Where(x => lstINTs.Contains(x.Id))
                  .Select<IPublishedContent, string>(x => x.Name);
            }
        }


        public static int GetCurrentReceiver()
        //0=brak wyboru
        //1=nauczyciel
        //2=uczen
        //3=student
        {
            int ret = 0;
            if (System.Web.HttpContext.Current.Session["receiver"] != null)
            {
                ret = (int)System.Web.HttpContext.Current.Session["receiver"];// == 1 ? 1 : 2;
            }
            else
            {
                var rcv = System.Web.HttpContext.Current.Request.Cookies["receiver"];
                if (rcv != null && rcv.Value != null)
                {
                    ret = (rcv.Value == "2" ? 2 :
                            (rcv.Value == "3" ? 3 :
                            1));
                    System.Web.HttpContext.Current.Session["receiver"] = ret;
                }
            }
            return ret;
        }


        public static List<KeyValuePair<object, object>> GetMainCategories()
        {
            var cached = System.Web.HttpContext.Current.Cache["maincategories"];
            if (cached != null)
                return (List<KeyValuePair<object, object>>)cached;
            else
            {
                var ret = UmbracoContext.Current.ContentCache
                    .GetAtRoot()
                    .FirstOrDefault()
                    .Children(x => x.DocumentTypeAlias == "Aktualne"
                        || x.DocumentTypeAlias == "Wiedza"
                        || x.DocumentTypeAlias == "Nauka"
                        || x.DocumentTypeAlias == "OMuzyceLista"
                        || x.DocumentTypeAlias == "Multimedia"
                        || x.DocumentTypeAlias == "Kanon")
                        .Select<IPublishedContent, KeyValuePair<object, object>>(x => new KeyValuePair<object, object>(x.Id, x.Name == "Wiecej o muzyce" ? "Więcej o muzyce" : x.Name))
                    .ToList();
                System.Web.HttpContext.Current.Cache["maincategories"] = ret;
                return ret;
            }
        }


        public static int GetVisitCounter(int nodeId)
        {
            int? viewCount = null;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("GetHitCounter", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("node_id", System.Data.SqlDbType.Int).Value = nodeId;
                    conn.Open();
                    viewCount = (int?)cmd.ExecuteScalar();
                }
            }
            return viewCount == null ? 0 : (int)viewCount;
        }


        public static bool IsIPAllowedByCountry()
        {
            if (!System.Web.HttpContext.Current.IsDebuggingEnabled)
            {
                var ca = System.Web.HttpContext.Current.Session["country_access"];
                if (ca != null)
                {
                    return (bool)ca;
                }
            }
            bool ret = true;
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (!string.IsNullOrEmpty(ip) && ip != "127.0.0.1")
            {
                var re = new System.Text.RegularExpressions.Regex(@"(\d+)\.(\d+)\.(\d+)\.(\d+)");
                var m = re.Match(ip);
                if (m.Success)
                {
                    byte b1 = Convert.ToByte(m.Groups[1].Value);
                    byte b2 = Convert.ToByte(m.Groups[2].Value);
                    byte b3 = Convert.ToByte(m.Groups[3].Value);
                    byte b4 = Convert.ToByte(m.Groups[4].Value);
                    long long_ip = ((b1 << 24) | (b2 << 16) | (b3 << 8) | b4) & 0x00000000FFFFFFFF;

                    using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("GetIsIPAllowedByCountry", conn))
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.Add("ip", System.Data.SqlDbType.BigInt).Value = long_ip;
                            conn.Open();
                            ret = ((bool?)cmd.ExecuteScalar() == true);
                        }
                    }
                }
            }
            System.Web.HttpContext.Current.Session["country_access"] = ret;
            return ret;
        }


        public static void IncrVisitCounter(int nodeId)
        {
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("UpdateHitCounter", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("node_id", System.Data.SqlDbType.Int).Value = nodeId;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }


        public static Dictionary<string, string> ConvertToDict(string jsonString)
        {
            var retval = new Dictionary<string, string>();
            dynamic pairs = JsonConvert.DeserializeObject<dynamic>(jsonString);
            if (pairs != null)
            {
                foreach (var pair in pairs)
                {
                    retval.Add((string)pair.key, (string)pair.value);
                }
            }
            return retval;
        }


        public static IEnumerable<IPublishedContent> GetRandomSeeAlso(IPublishedContent page)
        {
            string tagi = page.GetPropertyValue<string>("tagi");
            string[] atagi = (tagi == null ? null : tagi.Split(','));

            if (page.DocumentTypeAlias == "ArtykulGlowna")
            {
                page = UmbracoContext.Current.ContentCache.GetById(page.GetPropertyValue<int>("artykul"));
            }

            IPublishedContent parent = page.Parent;
            IEnumerable<IPublishedContent> found_list = null;

            int toTake = 4;
            if (page.HasValue("liczbaZobaczRowniez"))
            {
                toTake = page.GetPropertyValue<int>("liczbaZobaczRowniez");
            }
            else if (parent.HasValue("liczbaZobaczRowniez"))
            {
                toTake = parent.GetPropertyValue<int>("liczbaZobaczRowniez");
            }

            if (atagi != null)
            {
                found_list = parent.Children.Where(x => x.Id != page.Id && x.GetPropertyValue<string>("tagi") != null && x.GetPropertyValue<string>("tagi").Split(',').Any(atagi.Contains)).OrderByDescending(x => x.CreateDate).Take(toTake);
            }
            if (found_list == null)
            {
                found_list = parent.Children.Where(x => x.Id != page.Id).OrderByDescending(x => x.CreateDate).Take(toTake);
            }
            else if (found_list.Count() < toTake)
            {
                int[] found_ids = found_list.Select<IPublishedContent, int>(x => x.Id).ToArray();
                var all_list = parent.Children.Where(x => x.Id != page.Id && !found_ids.Contains(x.Id)).OrderByDescending(x => x.CreateDate).Take(toTake - found_list.Count());
                found_list = found_list.Union(all_list);
            }
            return found_list;
        }


        public static bool IsFirstEntry()
        {
            bool bFirstEntry = System.Web.HttpContext.Current.Session.IsNewSession;
            if (!bFirstEntry)
                return false;
            var c = System.Web.HttpContext.Current.Request.Cookies["firstEntry"];
            if (c == null)
            {
                var cn = new System.Web.HttpCookie("firstEntry", "1");
                cn.Expires = DateTime.Now.AddDays(30);
                System.Web.HttpContext.Current.Response.Cookies.Add(cn);
                return true;
            }
            return false;
        }


        //------------------------    
        // GREYSCALE
        //------------------------  
        public static bool IsGreyscale()
        {
            bool isGrayscale;
            if (!System.Web.HttpContext.Current.Items.Contains("SiteGreyscale"))
            {
                isGrayscale = UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault().GetPropertyValue<bool>("isGrayscale");
                System.Web.HttpContext.Current.Items.Add("SiteGreyscale", isGrayscale);
            }
            else
            {
                isGrayscale = (bool)System.Web.HttpContext.Current.Items["SiteGreyscale"];
            }
            return isGrayscale;
        }




        //------------
        //Format rozmiaru pliku
        //------------
        public static string FormatBytes(long bytes)
        {
            var x = ((Math.Round(Convert.ToDecimal(bytes) / 1048576, 1)).ToString() + "MB");
            var y = ((Math.Round(Convert.ToDecimal(bytes) / 1024, 1)).ToString() + "KB");
            return (bytes >= 1048576) ? x : y;
        }


        //------------
        //Bezpieczne wyciągnięcie wartosci z edytora KeyValuePair (duplikaty kluczy)
        //------------                
        public static Dictionary<string, string> SafeParseDictionaryKV(IPublishedContent content, string propertyName)
        {
            var prop = content.GetProperty(propertyName);
            if (prop != null && prop.DataValue != null)
            {
                string strdata = prop.DataValue.ToString();
                if (!String.IsNullOrEmpty(strdata))
                {
                    var ret = new Dictionary<string, string>();
                    dynamic pairs = JsonConvert.DeserializeObject<dynamic>(strdata);
                    if (pairs != null)
                    {
                        foreach (var pair in pairs)
                        {
                            string key = (string)pair.key;
                            string test_key = key;
                            int idx = 1;
                            while (ret.ContainsKey(test_key))
                            {
                                test_key = key + "#" + (idx++) + "#";
                            }
                            ret.Add(test_key, (string)pair.value);
                        }
                    }
                    return ret;
                }
            }
            return null;
        }


        //------------------------    
        // SKLEP
        //------------------------ 
        public static IPublishedContent GetStoreRoot()
        {
            return UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault(x => x.DocumentTypeAlias == "Sprzedaz").Children.FirstOrDefault(x => x.Name == "Produkty");
        }

        public static string GetProductCategory(Merchello.Web.Models.VirtualContent.IProductContent productContent)
        {
            switch (productContent.DocumentTypeAlias)
            {
                case "ProduktKsiazka":
                    return "Ksiażki";
                case "ProduktKoszulka":
                    return "Koszulki";
                case "ProduktDVD":
                    return "DVD";
                case "SubskrypcjaVOD":
                    return "VOD";
                default:
                    return "";
            }
        }


        public static string GetStoreLink(IPublishedContent content, StoreLinkType type)
        {
            var sklep = UmbracoContext.Current.ContentCache.GetAtRoot().DescendantsOrSelf("Sklep").FirstOrDefault();
            var search_node_type = (type == StoreLinkType.Rules ? "SklepRegulamin" :
                                     (type == StoreLinkType.Shipments ? "Dostawy" :
                                     ""));
            var rnode = sklep.Children.Where(x => x.DocumentTypeAlias == search_node_type).FirstOrDefault();
            if (rnode != null)
                return rnode.Url;
            else
                return "";

            /*
            var store_node = content;
            while (store_node != null && store_node.Parent != null && store_node.DocumentTypeAlias != "Sklep")
            {
                store_node = store_node.Parent;
            }
            if (store_node == null || store_node.DocumentTypeAlias != "Sklep")
            {
                return "";
            }
            else
            {
                var search_node_type = (type == StoreLinkType.Rules ? "SklepRegulamin" :
                                       (type == StoreLinkType.Shipments ? "Dostawy" :
                                       ""));
                var rnode = store_node.Children.Where(x => x.DocumentTypeAlias == search_node_type).FirstOrDefault();
                if (rnode != null)
                    return rnode.Url;
                else
                    return "";
            }
            */
        }

        public static Color GenerateColor(string Name)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(Name));
                var hash = "#" + BitConverter.ToString(data).Replace("-", string.Empty).Substring(0, 6);
                Color color = System.Drawing.ColorTranslator.FromHtml(hash);
                return color;
            }
        }

        public static void UpdateStudent(int Uczenid, IPublishedContent content)
        {
            var contestServices = ApplicationContext.Current.Services.ContentService;
            var item = contestServices.GetById(Uczenid);
            var value = item.GetValue<string>("status");
            if (!string.IsNullOrEmpty(value))
            {
                value += ',';
            }
            else
            {
                value = "";
            }
            if (!value.Contains(content.Id.ToString()))
            {
                item.SetValue("status", $"{value}{content.Id}:Uczeń odwiedził {content.Name}:Dnia {DateTime.Now.ToString("g")}");
                contestServices.Save(item);
                var test = contestServices.Publish(item);
            }
        }
        public static bool IsDRMOff(string code, IPublishedContent Model)
        {
            bool isDrmOff = false;
            if (!string.IsNullOrEmpty(code))
            {
                var root = Model.AncestorOrSelf(1);
                var Content = root.Descendants("Uczniowie").FirstOrDefault(x => x.GetKey().ToString() == code);
                if (Content != null)
                {
                    if (Content.Parent.HasValue("Artykuly"))
                    {
                        var date = Content.GetPropertyValue<DateTime>("dataWaznosc");
                        if (date > DateTime.Now)
                        {

                            var Artykuly = Content.Parent.GetPropertyValue<string>("Artykuly");
                            isDrmOff = Artykuly.Contains(Model.Id.ToString());
                            if (isDrmOff)
                            {
                                Muzykoteka.Code.Helper.UpdateStudent(Content.Id, Model);
                            }

                        }

                    }

                }
            }
            return isDrmOff;
        }

        public static bool MemberHaveAccess(string Path)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            return umbracoHelper.MemberHasAccess(Path);
        }

        public static string ParseOpisTransmisji(string opis)
        {
            //przewin do aktualnej pozycji
            //<p><strong>
            //<br/>
            var rg = new System.Text.RegularExpressions.Regex(@"<p>(\d\d:\d\d)|<p><strong>(\d\d:\d\d)|<br />(\d\d:\d\d)|<br/>(\d\d:\d\d)|<br>(\d\d:\d\d)"
                        + @"<p>(\d\d\d\d-\d\d-\d\d \d\d:\d\d)|<p><strong>(\d\d\d\d-\d\d-\d\d \d\d:\d\d)|<br />(\d\d\d\d-\d\d-\d\d \d\d:\d\d)|<br/>(\d\d\d\d-\d\d-\d\d \d\d:\d\d)|<br>(\d\d\d\d-\d\d-\d\d \d\d:\d\d)"
                            , System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            var m = rg.Match(opis);
            string czas = "";
            var now = DateTime.Now;
            var lastIndex = 0;
            bool bFound = false;
            while (m.Success && !bFound)
            {
                for (int g = 1; g < m.Groups.Count; g++)
                {
                    if (!String.IsNullOrEmpty(m.Groups[g].Value))
                    {

                        DateTime current;
                        string s = m.Groups[g].Value;
                        try
                        {
                            if (s.Length > 5)
                            {
                                //yyyy-mm-dd hh:mm
                                current = new DateTime(
                                    Int32.Parse(s.Substring(0, 4)),
                                    Int32.Parse(s.Substring(5, 2)),
                                    Int32.Parse(s.Substring(8, 2)),
                                    Int32.Parse(s.Substring(11, 2)),
                                    Int32.Parse(s.Substring(11, 2)),
                                    Int32.Parse(s.Substring(14, 2)),
                                    0);
                            }
                            else
                            {
                                //hh:mm
                                current = new DateTime(now.Year, now.Month, now.Day,
                                    Int32.Parse(s.Substring(0, 2)),
                                    Int32.Parse(s.Substring(3, 2)),
                                    0);

                            }
                        }
                        catch
                        {
                            continue;
                        }

                        if (current > now)
                        {
                            bFound = true;
                        }
                        else
                        {
                            lastIndex = m.Index;
                        }
                        break;
                    }
                }
                m = m.NextMatch();
            }
            if (bFound)
            {
                return "<ul class='teraz'><li>TERAZ</li></ul>" + opis.Substring(lastIndex);
            }
            else
            {
                return opis;
            }
        }
        public static IEnumerable<IPublishedContent> GetAnalytics(string Level1)
        {
            string startupPath = AppDomain.CurrentDomain.BaseDirectory;
            GoogleAnalyticsAPI analyticsAPI = new GoogleAnalyticsAPI(Path.Combine(startupPath, "cert", "WFDiF-8cf1dccf1fce.p12"), "wfdif-online@wfdif-284210.iam.gserviceaccount.com");
            var profiles = analyticsAPI.GetAvailableProfiles();
            if (profiles != null)
            {
                var profile = profiles.FirstOrDefault(x => x.WebPropertyId == "UA-164380546-1");
                if (profile != null)
                {
                    DateTime baseDate = DateTime.Today;
                    var test = analyticsAPI.GetAnalyticsData(profile.Id,
                        new string[] { "ga:pagePath", "ga:pagePathLevel1", "ga:pagePathLevel3" },
                        new string[] { "ga:totalEvents" },
                        string.IsNullOrEmpty(Level1) ? null : new string[] { $"ga:pagePathLevel1=={Level1}" },
                        new string[] { "-ga:totalEvents" },
                        baseDate.AddMonths(-1),
                        baseDate,
                        1,
                        30);
                    if (test != null && test.Rows.Count() > 0)
                    {
                        var temp = test.Rows.Select(x => x[0]);
                        return GetNodeIdFromUrl(temp);
                    }
                }
            }
            return null;
        }
        public static List<IPublishedContent> GetNodeIdFromUrl(IEnumerable<string> urls)
        {
            List<IPublishedContent> Nodes = new List<IPublishedContent>();
            foreach (var url in urls)
            {

                var temp = UmbracoContext.Current.ContentCache.GetByRoute(url);
                if (temp != null && !Nodes.Any(x => x.Id == temp.Id))
                    Nodes.Add(temp);
            }
            return Nodes;
        }

        public static Guid GetMerchelloConstant_Status_Paid()
        {
            return Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Paid;
        }

        public static Guid GetMerchelloConstant_Status_Cancelled()
        {
            return Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Cancelled;
        }

        public static Guid GetMerchelloConstant_Status_Fraud()
        {
            return Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Fraud;
        }

        public static Guid GetMerchelloConstant_Status_Partial()
        {
            return Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Partial;
        }

        public static Guid GetMerchelloConstant_Status_Unpaid()
        {
            return Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Unpaid;
        }

        public static string GetSub(IEnumerable<IPublishedContent> subttitles)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var subttitle in subttitles)
            {
                sb.Append($"{{language:'{subttitle.GetPropertyValue<string>("langName")}',url:'{subttitle.Url}'}},");
            }

            return sb.ToString();
        }
        public static IEnumerable<IPublishedContent> Pastylki(IEnumerable<IPublishedContent> category)
        {
            List<IPublishedContent> items = new List<IPublishedContent>();
            foreach (var item in category)
            {
                items.AddRange(item.Children.Where(x => x.IsVisible() && !x.GetPropertyValue<bool>("searchHide", false)
                ).OrderBy("UpdateDate desc").Take(6));
            }
            items = items.OrderByDescending(x => x.UpdateDate).ToList();
            return items;
        }
        
    }

}
