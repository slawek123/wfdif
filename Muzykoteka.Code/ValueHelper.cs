﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Muzykoteka.Code
{
    public static class ValueHelper
    {

        public static IEnumerable<SelectListItem> provinceName = new List<SelectListItem>()
        {
            new SelectListItem()
            {
                Text="Wybierz Województwo",
                Value=" ",
                Selected=true
            },
            new SelectListItem()
            {
                Text="dolnośląskie",
                Value="dolnośląskie",
            },
            new SelectListItem()
            {
                Text="kujawsko-pomorskie",
                Value="kujawsko-pomorskie",
            },
            new SelectListItem()
            {
                Text="lubelskie",
                Value="lubelskie",
            },
            new SelectListItem()
            {
                Text="lubuskie",
                Value="Lubuskiego",
            },
            new SelectListItem()
            {
                Text="łódzkie",
                Value="łódzkie",
            },
            new SelectListItem()
            {
                Text="małopolskie",
                Value="małopolskie",
            },
            new SelectListItem()
            {
                Text= "mazowieckie",
                Value= "mazowieckie",
            },
            new SelectListItem()
            {
                Text="opolskie",
                Value="opolskie",
            },
            new SelectListItem()
            {
                Text="podkarpackie",
                Value="podkarpackie",
            },
            new SelectListItem()
            {
                Text="podlaskie",
                Value="podlaskie",
            },
            new SelectListItem()
            {
                Text="pomorskie",
                Value="pomorskie",
            },
            new SelectListItem()
            {
                Text="śląskie",
                Value="śląskie",
            },
            new SelectListItem()
            {
                Text="świętokrzyskie",
                Value="świętokrzyskie",
            },
            new SelectListItem()
            {
                Text="warmińsko-mazurskie",
                Value="warmińsko-mazurskie",
            },
            new SelectListItem()
            {
                Text= "wielkopolskie",
                Value= "wielkopolskie",
            },
            new SelectListItem()
            {
                Text="zachodniopomorskie",
                Value="zachodniopomorskie",
            },
        };
        public static IEnumerable<SelectListItem> InstitutionTypeName = new List<SelectListItem>()
        {
            new SelectListItem()
            {
                Text="Wybierz Typ instytucji",
                Value=" ",
                Selected=true,
            },
            new SelectListItem()
            {
                Text="Dom kultury",
                Value="Dom kultury",
            },
            new SelectListItem()
            {
                Text="Inna",
                Value="Inna",
            },
            new SelectListItem()
            {
                Text="Liceum",
                Value="Liceum",
            },
            new SelectListItem()
            {
                Text= "Szkoła podstawowa",
                Value= "Szkoła podstawowa",
            },
            new SelectListItem()
            {
                Text="Technikum",
                Value="Technikum",
            },
            new SelectListItem()
            {
                Text="Uczelnia wyższa",
                Value="Uczelnia wyższa",
            }
        };
        public static IEnumerable<SelectListItem> GetSelectProvince(string name) => provinceName.Select(x => { x.Selected = x.Value == name; return x; });
        public static IEnumerable<SelectListItem> GetSelectInstitutionTypeName(string name) => InstitutionTypeName.Select(x => { x.Selected = x.Value == name; return x; });
        public static string MakeSSOUrl(this HtmlHelper value, string provider, string ExternalUrl)
        {
            //return $"/vowosadmin/Surface/Account/ExternalLogin?provider={provider}&returnUrl=/stopka/signin";
            var baseUrl = new UriBuilder(WebConfigurationManager.AppSettings["sso_host"]);
            baseUrl.Path = "/connect/authorize";
            var queryObject = new Dictionary<string, string>()
            {
                { "client_id", WebConfigurationManager.AppSettings["sso_client_id"] },
                { "redirect_uri", WebConfigurationManager.AppSettings["sso_redirect"] },
                { "response_type", "code" },
                { "scope", "openid profile offline_access email roles IdentityServerApi waitingrole" },
                { "nonce", "x" },
                { "external", provider },
                { "prompt", "login" },
                { "registerext", ExternalUrl },
            };
            var queryList = queryObject.Select(x => HttpUtility.UrlEncode(x.Key) + "=" + HttpUtility.UrlEncode(x.Value));
            baseUrl.Query = string.Join("&", queryList);
            return baseUrl.ToString();
        }
        public static IDictionary<string, string> QueryToString(string query)
        {
            var dict = HttpUtility.ParseQueryString(query);
            return dict.Cast<string>().ToDictionary(k => k, v => dict[v]);
        }
    }
}
