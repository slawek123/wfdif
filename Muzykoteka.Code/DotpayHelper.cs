﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using System.IO;
using System.Security.Cryptography;
using Merchello.Core.Models;
using Merchello.Core;
using System.Configuration;
using Umbraco.Core.Logging;



namespace Muzykoteka.Code
{     
    public static class DotpayHelper
    {

        //------------------------    
        // Build Dotpay URL
        //------------------------   
        public static string GetReturnURL(IPublishedContent return_node, IInvoice invoice)
        {

            string inv_number = HttpContext.Current.Server.UrlEncode(AesEncrypt(invoice.InvoiceNumber.ToString()));

            string return_url = HttpContext.Current.Server.UrlEncode(
                                    String.Format("http://{0}{1}?inv={2}",
                                        HttpContext.Current.Request.ServerVariables["HTTP_HOST"],
                                        return_node.Url,
                                        inv_number));
            
            string desc = HttpContext.Current.Server.UrlEncode(
                            invoice.Items.Where(x => x.LineItemType == LineItemType.Product).Select(x => x.Name).Aggregate((a, x) => a + "," + x));
            if (desc.Length > 255)
                desc = desc.Substring(0, 255);

            var control_node = UmbracoContext.Current.ContentCache.GetAtRoot()
                                    .DescendantsOrSelf("Sklep")
                                    .DescendantsOrSelf("StatusDotpay")
                                    .SingleOrDefault();

            //var control_node = UmbracoContext.Current.PublishedContentRequest.PublishedContent.Parent.Children.Where(x => x.DocumentTypeAlias == "StatusDotpay").FirstOrDefault();

            string control_url = "";
            if (control_node != null)
            {
                control_url =  "&URLC=" + HttpContext.Current.Server.UrlEncode("http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + control_node.Url);
            }

            string dotpay_client_id = ConfigurationManager.AppSettings["dotpay_clientid"];

            return ConfigurationManager.AppSettings["dotpay_url"] +
                String.Format("/?id={0}&control={1}&opis={2}&kwota={3}&typ=0&URL={4}{5}", 
                dotpay_client_id,
                inv_number, 
                desc, 
                invoice.Total.ToString("0.00").Replace(",", "."), 
                return_url, 
                control_url);
        }




        //------------------------    
        // Update status (URLC)
        //------------------------   

        public static string UpdateInvoiceStatus()
        {                        
            System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;

            string t_status = request["t_status"];
            string pin = ConfigurationManager.AppSettings["dotpay_pin"];
            string r_md5 = request["md5"];
            string r_control = request["control"];
            string r_id = request["id"];
            string r_t_id = request["t_id"];            
            string r_amount = request["amount"];
            string r_email = request["email"];
            string r_service = request["service"];
            string r_code = request["code"];
            string r_username = request["username"];
            string r_password = request["password"];


            try
            {               
                string dotpay_serverip = ConfigurationManager.AppSettings["dotpay_serverip"];
                if (String.Compare(request.ServerVariables["REMOTE_ADDR"], dotpay_serverip) != 0)
                {
                    throw new Exception(String.Format("Invalid server IP {0} ({1})", request.ServerVariables["REMOTE_ADDR"], dotpay_serverip));
                }


                string dotpay_client_id = ConfigurationManager.AppSettings["dotpay_clientid"];
                if (String.Compare(r_id, dotpay_client_id, true) != 0)
                {
                    throw new Exception(String.Format("Invalid clientID {0} ({1})", r_id, dotpay_client_id));
                }
                
                if (String.IsNullOrEmpty(r_control))
                {
                    throw new Exception("Empty parameter: control");
                }
                string s_inv_number = AesDecrypt(r_control);
                LogI(String.Format("Invoice {0} status {1} (t_id={2} a={3})", s_inv_number, t_status, r_t_id, r_amount));

                if (t_status == "1"
                   || t_status == "3"
                   || t_status == "5"
                   )
                {                
                    return "OK";
                }


            
                //weryfikacja hash
                string to_hash = pin + ":" + r_id + ":" + r_control + ":" + r_t_id + ":" + r_amount + ":" + r_email + ":" +
                                 r_service + ":" + r_code + ":" + r_username + ":" + r_password + ":" + t_status;
                string n_md5;
                using (MD5 md5 = MD5.Create())
                {
                    byte[] bytes = md5.ComputeHash(Encoding.ASCII.GetBytes(to_hash));
                    n_md5 = BitConverter.ToString(bytes).Replace("-", "");
                }
                if (String.Compare(n_md5, r_md5, true) != 0)
                {
                    throw new Exception (String.Format("MD5 error, {0} => {1} != {2}", to_hash, n_md5, r_md5));
                }


                //pobierz fakture                            
                int inv_number;
                if (!Int32.TryParse(s_inv_number, out inv_number))
                {
                    throw new Exception(String.Format("Invalid invoice number: {0}", s_inv_number));
                }
                IInvoice invoice = MerchelloContext.Current.Services.InvoiceService.GetByInvoiceNumber(inv_number);
                if (invoice == null)
                {
                    throw new Exception (String.Format("Invoice not found, number={0}", inv_number));
                }


                //new payment          
                
                //FIX decimal.Parse in merchello
                System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
                try
                {                  
                    decimal amount = Decimal.Parse(r_amount, System.Globalization.CultureInfo.InvariantCulture);
                    if (t_status == "4")
                        amount *= -1;
                    IPayment payment = invoice.Payments().Where(x => x.PaymentMethodKey == CheckoutController.GetPaymentKey(CheckoutController.WfdifPaymentMethod.Dotpay)).FirstOrDefault();
                    if (payment == null)
                    {
                        var payment_res = invoice.AuthorizePayment(CheckoutController.GetPaymentKey(CheckoutController.WfdifPaymentMethod.Dotpay));
                        if (payment_res == null || !payment_res.Payment.Success)
                        {
                            throw new Exception(String.Format("AuthorizePayment error ({0})", amount.ToString()));
                        }
                        payment = payment_res.Payment.Result;
                    }
                    var attempt = invoice.CapturePayment(payment, CheckoutController.GetPaymentKey(CheckoutController.WfdifPaymentMethod.Dotpay), amount, null);
                    if (attempt == null || !attempt.Payment.Success)
                    {
                        throw new Exception(String.Format("CapturePayment error ({0})", amount.ToString()));
                    }


                    //invoice status
                    decimal total_paid = invoice.AppliedPayments().Sum(x => x.Amount);                    
                    var new_status = (total_paid >= invoice.Total ?
                        MerchelloContext.Current.Services.InvoiceService.GetInvoiceStatusByKey(Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Paid)
                        : MerchelloContext.Current.Services.InvoiceService.GetInvoiceStatusByKey(Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Partial));
                    if (new_status != invoice.InvoiceStatus)
                    {
                        invoice.InvoiceStatus = new_status;
                        MerchelloContext.Current.Services.InvoiceService.Save(invoice);
                    }
                }
                finally
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                }
            }
            catch (Exception ex)
            {
                LogE(null, ex);
            }
            return "OK";//"ERROR" powoduje powtórzenie komunikatu z dotpay 
        }

        
        //------------------------    
        // EN/DE-crypt
        //------------------------          
        private static string AESKEY = "ALW@GFS@127FDFST@_";

        public static string AesEncrypt(string clearText)
        {
            byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(AESKEY, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string AesDecrypt(string cipherText)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(AESKEY, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }            
            return cipherText;
        }


        //------------------------    
        // Helpers
        //------------------------   
        private static void LogI(string msg)
        {
            LogHelper.Info(typeof(DotpayHelper), msg);
        }

        private static void LogE(string msg, Exception ex)
        {
            LogHelper.Error(typeof(DotpayHelper), msg, (ex == null ? new Exception() : ex));
        }
    }
}
