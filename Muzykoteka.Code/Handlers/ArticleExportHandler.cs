﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using Umbraco.Core.Services;
using Umbraco.Core.Models;
using DevExpress.XtraRichEdit;
using Umbraco.Web;
using System.Text.RegularExpressions;


namespace Muzykoteka.Code
{
    public class ArticleExportHandler : IHttpHandler
    {
        
        public bool IsReusable
        {
            get { return false;  }
        }


        private HttpContext _currentContext;
        private HttpContext CurrentContext
        {
            get
            {
                return _currentContext;
            }
            set {
                _currentContext = value;
            }        
        }

        /*
        private bool _isDOC;
        private bool IsDOC
        {
            get
            {
                return _isDOC;
            }
            set
            {
                _isDOC = value;
            }
        }
        */
        private int ImgWidth
        {
            get
            {
                return 730; //IsDOC ? 730 : 800;
            }
        }


        public void ProcessRequest(HttpContext context)
        {
            
            
            CurrentContext = context;

            //IsDOC = (context.Request.QueryString["t"] == "doc");
            int nodeId;
            if (!Int32.TryParse(context.Request.QueryString["id"], out nodeId))
            {                
                nodeId = -1;                
                return;
            }

            bool bPrint = (context.Request.QueryString["t"] == "print");

            IPublishedContent node = Umbraco.Web.UmbracoContext.Current.ContentCache.GetById(nodeId);
            if (node == null)
            {
                return;
            }
/*
            if (Helpers.Help.GetLang(nodeId) == "en")
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-US");
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            }
            else
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("pl-PL");
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pl-PL");
            }
            */
            RichEditDocumentServer server = new RichEditDocumentServer();

            BuildDocument(server, node);
            
            using (var memoryStream = new MemoryStream())
            {
                if (bPrint)
                    server.ExportToPdf(memoryStream, new DevExpress.XtraPrinting.PdfExportOptions() { ShowPrintDialogOnOpen = true });
                else
                    server.ExportToPdf(memoryStream);

                context.Response.Clear();
                
                string filename = Regex.Replace(node.Name, "<[^>]*(>|$)", string.Empty) + ".pdf";
                string contentDisposition;
                if (bPrint)
                {
                    if (context.Request.UserAgent != null && context.Request.UserAgent.ToLowerInvariant().Contains("android"))
                        contentDisposition = "inline; filename=\"" + MakeAndroidSafeFileName(filename) + "\"";
                    else
                        contentDisposition = "inline; filename*=UTF-8''" + Uri.EscapeDataString(filename);
                }
                else
                {
                    if (context.Request.UserAgent != null && context.Request.UserAgent.ToLowerInvariant().Contains("android"))
                        contentDisposition = "attachment; filename=\"" + MakeAndroidSafeFileName(filename) + "\"";
                    else
                        contentDisposition = "attachment; filename*=UTF-8''" + Uri.EscapeDataString(filename);
                }
                context.Response.AddHeader("Content-Disposition", contentDisposition);
                
                context.Response.ContentType = "application/pdf";

                context.Response.BinaryWrite(memoryStream.ToArray());
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
           
            context.Response.End();
        }
      
     
        private void BuildDocument(RichEditDocumentServer server, IPublishedContent node) {
            
            server.CreateNewDocument();
            IPublishedContent picture_node = node;
            string tytul = node.GetPropertyValue<string>("tytul") ?? "";
            
            string lead = PrepareText(node.HasValue("lead") ? (string)node.GetProperty("lead").DataValue : "", node);

            string tresc = PrepareText(node.HasValue("tresc") ? (string)node.GetProperty("tresc").DataValue : "", node);

            if (node.DocumentTypeAlias == "ArtykulGlowna")
            {
                int? linkNodeId = node.GetPropertyValue<int?>("artykul");
                if (linkNodeId != null)
                {
                    var linked = UmbracoContext.Current.ContentCache.GetById((int)linkNodeId);
                    if (linked != null)
                    {
                        picture_node = linked;
                        tresc = PrepareText(linked.HasValue("tresc") ? (string)linked.GetProperty("tresc").DataValue : "", linked);
                        if (String.IsNullOrEmpty(lead))
                        {
                            lead = PrepareText(linked.HasValue("lead") ? (string)linked.GetProperty("lead").DataValue : "", linked);
                        }
                        if (String.IsNullOrEmpty(tytul))
                        {
                            tytul = linked.GetPropertyValue<string>("tytul") ?? "";
                        }
                    }
                }
            }

            string text;

            if (picture_node.GetPropertyValue<bool?>("pokazZdjecie") == true) {                
                int? zdjecieArtykulowe = picture_node.GetPropertyValue<int?>("zdjecieArtykulowe");
                string img_src = null;
                        
                if (zdjecieArtykulowe != null) {
                    var media = UmbracoContext.Current.ContentCache.GetById((int)zdjecieArtykulowe);
                    if (media != null)
                    {
                        img_src = media.Url;
                    }
                }
                else if (picture_node.HasValue("zdjecie")) {
                   
                    img_src = picture_node.GetCropUrl(propertyAlias: "zdjecie", imageCropMode: Umbraco.Web.Models.ImageCropMode.Max);
                }
  
                if (!String.IsNullOrEmpty(img_src)) {
                    string img_path = "http://" + CurrentContext.Request.Url.Host + img_src;
                    text = String.Format("<img style=\"width: {0}px\" src=\"{1}\" />", ImgWidth, img_path);
                    server.Document.AppendHtmlText(text);
                }
            }          
            text = "<br /><br /><h1>" + tytul + "</h1>"
                    + "<h2 style=\"margin: 20px 0 40px 0\">" + lead + "</h2>"
                    + "<div>" + tresc + "</div>";

            //string text_color = "style=\"color: " + Helpers.Help.GetKolorTekstu(node.GetPropertyValue<string>("kolorTekstu")) + " !important\"";
            //if (!IsDOC)
            //    text = String.Format("<table><tr><td style=\"width: 50px\"></td><td {1}>{0}</td><td style=\"width: 50px\"></td></tr></table>", text, text_color);
            //else
             
            text = String.Format("<div>{0}</div>", text);

            server.Document.AppendHtmlText(text);

            foreach (var _section in server.Document.Sections)
            {                
                _section.Page.PaperKind = System.Drawing.Printing.PaperKind.A4;
                _section.Page.Landscape = false;
                _section.Margins.Left = 100;
                _section.Margins.Right = 100;
                _section.Margins.Top = 100;
                _section.Margins.Bottom = 100;    
                _section.PageNumbering.NumberingFormat = DevExpress.XtraRichEdit.API.Native.NumberingFormat.CardinalText;
                _section.PageNumbering.Start = 0;
            }
        }


        private string PrepareText(string tresc, IPublishedContent node)
        {            
            //UMBRACO MACRO
            if (String.IsNullOrEmpty(tresc))
            {
                return "";
            }
            int macroStart = tresc.IndexOf("<?UMBRACO_MACRO", 0);
            while (macroStart >= 0)
            {
                int macroEnd = tresc.IndexOf("/>", macroStart + 15);
                if (macroEnd > 0)
                {
                    string original = tresc.Substring(macroStart, macroEnd - macroStart + 2);
                    string replacement = GetMacroReplacement(original);
                    
                    tresc = tresc.Replace(original, replacement);
                    //tresc = tresc.Remove(macroStart, original.Length).Insert(macroStart, replacement);
                    macroStart = tresc.IndexOf("<?UMBRACO_MACRO", macroStart + replacement.Length);
                }
                else
                {
                    macroStart = -1;
                }
            }

            //przeformatować starą galerie
            tresc = ReformatOldGalleries(tresc);

            //poprawione sciezki w img src
            tresc = tresc.Replace("src=\"/", " style=\"max-width: " + ImgWidth + "px\" src=\"http://" + CurrentContext.Request.Url.Host + "/");

            
            
            /*


            //DATA PUBLIKACJI
            tresc += String.Format("<br /><br /><table><tr><td align=\"right\" width=\"{1}px\">{0}</td></tr></table>",
                UmbracoContext.Current.Application.Services.LocalizationService
                        .GetDictionaryItemByKey("Data publikacji")
                        .Translations
                        .Where(x => x.Language.CultureInfo.TwoLetterISOLanguageName == System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)                        
                        .Select(x=>x.Value).SingleOrDefault() 
                    + ": " + Helpers.Help.GetPublicationDate(node).ToString("dd '/' MM '/' yyyy"),
                ImgWidth);
            */
            return tresc;
        }


        private string GetAttribute(string text, string attribute)
        {  
            Regex r = new Regex(attribute + "=\"([^\"]*)\"", RegexOptions.IgnoreCase);            
            Match m = r.Match(text);
            if (m.Success && m.Groups.Count > 1)
                return m.Groups[1].Value;
            else
                return "";
        }


        private string GetMacroReplacement (string macro_text) {
            
            string ret = "";

            string macroAlias = GetAttribute(macro_text, "macroAlias").ToLower();

            try
            {
                switch (macroAlias)
                {
                    case "cytat":
                        string text = GetAttribute(macro_text, "text").Replace("&gt;", ">").Replace("&lt;", "<");
                        ret += String.Format("<p style=\"font-size: 1.5em;  margin: 30px 50px\"><em>\"{0}\"</em></p>", text);
                        break;

                    case "galeria":
                        int[] media_ids = GetAttribute(macro_text, "zdjecia").Split(',').Select<string, int>(x => Convert.ToInt32(x)).ToArray();
                        foreach (var media_id in media_ids)
                        {
                            var media = Umbraco.Web.UmbracoContext.Current.MediaCache.GetById(media_id);
                            if (media != null)
                            {
                                string alt = media.HasValue("alt") ? media.GetPropertyValue<string>("alt") : "";
                                string url_path = "http://" + CurrentContext.Request.Url.Host + media.Url;
                                //ret += String.Format("<img style=\"width: {1}px\" src=\"{0}\" /><br />", url_path, ImgWidth);

                                ret += String.Format("<img style=\"width: {1}px\" src=\"{0}\" /><br />", url_path, ImgWidth);
                                if (!String.IsNullOrWhiteSpace(alt)) {
                                    ret += String.Format("<table><tr><td align=\"right\" width=\"{0}px\">{1}</td></tr></table>", ImgWidth, alt);
                                }
                            }
                        }

                        break;


                    case "obraz":
                        string podpis = GetAttribute(macro_text, "podpis");
                        string media_sid = GetAttribute(macro_text, "media");
                        int mid;
                        if (Int32.TryParse(media_sid, out mid))
                        {
                            var media = Umbraco.Web.UmbracoContext.Current.MediaCache.GetById(mid);
                            if (media != null)
                            {
                                string url_path = "http://" + CurrentContext.Request.Url.Host + media.Url;
                                ret += String.Format("<img style=\"width: {1}px\" src=\"{0}\" /><br /><table><tr><td align=\"right\" width=\"{1}px\">{2}</td></tr></table>", url_path, ImgWidth, podpis);
                            }
                        }
                        break;
                }

            }
            catch
            {}
            return ret;
             
        }


        private string ReformatOldGalleries(string tresc)
        {
            string title_pattern = "<span class=\"title\">[^<]*</span>";
            tresc = Regex.Replace(tresc, title_pattern, string.Empty);

            tresc = tresc.Replace("<a class=\"show js-fullscreen\">zobacz galerię | <span class=\"js-counter-total\"></span></a>", "");

            tresc = tresc.Replace("<a class=\"close js-closefs\"><img src=\"/Images/close-fs.png\" alt=\"\" /></a>", "");
            tresc = tresc.Replace("<a class=\"close js-closefs\"><img src=\"/Images/close-fs.png\" alt=\"\"></a>", "");

            tresc = tresc.Replace("<div class=\"counter\">", "<div style=\"display: none\">");
            //

            tresc = tresc.Replace("<div class=\"nav\">", "<div style=\"display: none\">");

         //   tresc = tresc.Replace("<a class=\"show js-fullscreen\">", "<a style=\"display: none\">");


            tresc = tresc.Replace("<a class=\"thumb js-thumb\">", "<a class=\"thumb js-thumb\"><br />");

            tresc = tresc.Replace("<span class=\"rb\">", "<br /><span style=\"display: block;text-align:center; width: 100%\">");

         //   string fs_pattern = "<a class=\"show js-fullscreen.*</a>";
         //   tresc = Regex.Replace(tresc, fs_pattern, string.Empty);

            return tresc;
        }


        private static readonly Dictionary<char, char> AndroidAllowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ._-+,@£$€!½§~'=()[]{}0123456789".ToDictionary(c => c);
        private string MakeAndroidSafeFileName(string fileName)
        {
            char[] newFileName = fileName.ToCharArray();
            for (int i = 0; i < newFileName.Length; i++)
            {
                if (!AndroidAllowedChars.ContainsKey(newFileName[i]))
                    newFileName[i] = '_';
            }
            return new string(newFileName);
        }
    }
}
