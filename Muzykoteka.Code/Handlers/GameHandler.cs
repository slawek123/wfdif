﻿using System;
using System.IO;
using System.Web;
using Umbraco.Core.Services;
using Umbraco.Core.Models;

namespace Muzykoteka.Code
{
    public class GameHandler : IHttpHandler
    {
        public GameHandler()
        { }

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var user = new HttpContextWrapper(HttpContext.Current).User;

            string sNodeId = Path.GetFileNameWithoutExtension(context.Request.FilePath);
            int nodeId;
            if (!Int32.TryParse(sNodeId, out nodeId))
            {
                context.Response.Status = "403 Forbidden";
                context.Response.StatusCode = 403;
                return;
            }

            Umbraco.Core.ApplicationContext ctx = Umbraco.Core.ApplicationContext.Current;
            IContentService csrv = ctx.Services.ContentService;
            IContent c = csrv.GetById(nodeId);
            if (c == null)
            {
                context.Response.Status = "403 Forbidden";
                context.Response.StatusCode = 403;
                return;
            }
            
            var memberService = ctx.Services.MemberService;
            var publicAccessService = ctx.Services.PublicAccessService;            
            var member = memberService.GetByUsername(user.Identity.Name);                
            var rolesList = (member == null ? new string[] { } : System.Web.Security.Roles.GetRolesForUser(member.Username));
            
            bool isAllowed = publicAccessService.HasAccess(c.Id, csrv, rolesList);                           
            if (isAllowed)
            {
                string requestedFile = context.Server.MapPath(Path.GetDirectoryName(context.Request.FilePath) + "/index.html");
                context.Response.ContentType = "text/html";
                context.Response.TransmitFile(requestedFile);
                context.Response.End();                
            }
            else
            {
                context.Response.Status = "403 Forbidden";
                context.Response.StatusCode = 403;
            }
        }
    }
}
