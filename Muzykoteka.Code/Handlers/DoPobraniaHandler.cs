﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Web;
using System.IO;
using System.Web;
using ICSharpCode.SharpZipLib.Zip;
using Umbraco.Core.Services;
using Umbraco.Core.Models;
using Newtonsoft.Json;



namespace Muzykoteka.Code
{
    public class DoPobraniaHandler : IHttpHandler
    {

        private const int MAX_READFILE_BUFFER = 0x100000;

        public bool IsReusable
        {
            get { return false;  }
        }

        public void ProcessRequest(HttpContext context)
        {
            List<string> _addedFiles = new List<string> ();            
            int nodeId;
            if (!Int32.TryParse(context.Request.QueryString["id"], out nodeId))
                nodeId = -1;

            context.Response.Clear();
            context.Response.AddHeader("content-disposition", "attachment; filename=do_pobrania.zip");
            context.Response.ContentType = "application/zip";
            
            using (ZipOutputStream zipOutput = new ZipOutputStream(context.Response.OutputStream))
            {
                zipOutput.SetLevel(9);                
                Umbraco.Core.ApplicationContext ctx =  Umbraco.Core.ApplicationContext.Current; 
                IMediaService msrv = ctx.Services.MediaService;
                IContentService csrv = ctx.Services.ContentService;                                
                var memberService = ctx.Services.MemberService;
                var publicAccessService = ctx.Services.PublicAccessService;
                var memberShipHelper = new Umbraco.Web.Security.MembershipHelper(Umbraco.Web.UmbracoContext.Current);
                var member = memberService.GetById(memberShipHelper.GetCurrentMemberId());
                var rolesList = (member == null ? new string[] { } : System.Web.Security.Roles.GetRolesForUser(member.Username));

                IContent c = csrv.GetById(nodeId);
                string mids = (string)c.GetValue("doPobrania");
                var d_nodes = c.Children().Where(x => x.ContentType.Name == "DoPobraniaChroniony");
                foreach (var content in d_nodes)
                {
                    if (publicAccessService.HasAccess(content.Id, csrv, rolesList)) {
                        var v = (string)content.GetValue("doPobrania");
                        if (v != null)
                            mids += "," + v;
                    }
                }

                string[] media_ids = mids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);                    
                foreach (string sid in media_ids)
                {
                    int mid;
                    if (Int32.TryParse(sid, out mid))
                    {
                        IMedia m = msrv.GetById(mid);
                        string crop_data = m.Properties["umbracoFile"].Value.ToString();
                        string rel_path;
                        if (crop_data.Contains("\"src\":") || crop_data.Contains("src:"))
                        {
                            dynamic json = JsonConvert.DeserializeObject<dynamic>(crop_data);
                            rel_path = json.src;
                        }
                        else
                        {
                            rel_path = crop_data;
                        }                                                
                        string filePath = context.Server.MapPath(rel_path);
                        string fileName = Path.GetFileNameWithoutExtension(filePath);
                        string fileExtension = Path.GetExtension(filePath);
                        string zipFilePath = fileName;
                        int counter = 1;
                        while (_addedFiles.Contains(zipFilePath + fileExtension))
                            zipFilePath = fileName + "_" + counter++;
                        zipFilePath += fileExtension;
                        _addedFiles.Add(zipFilePath);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {                           
                            ICSharpCode.SharpZipLib.Zip.ZipEntry zipEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(zipFilePath);
                            zipOutput.PutNextEntry(zipEntry);

                            int buffer_size = (fileStream.Length > MAX_READFILE_BUFFER ? MAX_READFILE_BUFFER:(int)fileStream.Length);
                            byte[] buffer = new byte[buffer_size];                            
                            int offset = 0;
                            while (offset < fileStream.Length) {
                                int read = fileStream.Read(buffer, 0, buffer_size);                            
                                zipOutput.Write(buffer, 0, read);    
                                offset += read;
                            }

                            fileStream.Close();

                            buffer = null;
                        }
                    }
                }
                zipOutput.Finish();
                zipOutput.Flush();
                zipOutput.Close();
                zipOutput.Dispose();
            }                        
            context.Response.End();
        }      
    }
}
