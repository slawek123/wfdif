﻿using System;

namespace Muzykoteka.Code.SimpilyForums
{
    public class LastPostInfo
    {
        public DateTime Date { get; set; }
        public int MemberId { get; set; }
        public string Name { get; set; }
    }
}
