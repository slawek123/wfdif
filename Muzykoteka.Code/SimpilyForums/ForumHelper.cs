﻿using System;
using System.Linq;

namespace Muzykoteka.Code.SimpilyForums
{
    public class ForumHelper
    {
        public static string GetRelativeDate(DateTime date)
        {
            // takes a date and displays a relative thing
            // i.e 10minutes ago, 1 day ago...

            if (date == DateTime.MinValue)
                return "Nigdy";

            var span = DateTime.Now.Subtract(date);

            if (span.Days > 0)
            {
                if (span.Days > 7)
                {
                    return date.ToString("dd MMM yyyy H:ss");
                }
                else if (span.Days == 1)
                {
                    return "Wczoraj";
                }
                else
                {
                    return string.Format("{0} dni temu", span.Days);
                }
            }
            else if (span.Hours > 0)
            {
                return string.Format("{0} {1} temu", span.Hours, span.Hours > 1 ? "Godzin" : "Godzinę");
            }
            else if (span.Minutes > 0)
            {
                return string.Format("{0} {1} temu", span.Minutes, span.Minutes > 1 ? "minut" : "minutę");
            }
            else
            {
                return "Przed chwilą";
            }
        }


    }
}
