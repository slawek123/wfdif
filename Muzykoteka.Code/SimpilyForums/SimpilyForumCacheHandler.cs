﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace Muzykoteka.Code.SimpilyForums
{
    public class SimpilyForumCacheHandler : ApplicationEventHandler
    {
        private int postContentTypeId;
        private int forumContentTypeId;

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var postType = ApplicationContext.Current.Services.ContentTypeService.GetContentType("Simpilypost");
            if (postType != null)
                postContentTypeId = postType.Id;

            var forumType = ApplicationContext.Current.Services.ContentTypeService.GetContentType("Simpilyforum");
            if (forumType != null)
                forumContentTypeId = forumType.Id;

            ContentService.Published += ContentServicePublished;
            ContentService.UnPublished += ContentServicePublished;
        }

        void ContentServicePublished(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<IContent> e)
        {
            // when something is published, (if it's a ForumPost)
            // clear the relevant forum cache.
            // we do it in two steps because more than one post in a forum 
            // may have been published, so we only need to clear the cache
            // once.

            List<string> invalidCacheList = new List<string>();

            foreach (var item in e.PublishedEntities)
            {
                // is a forum post...
                if (item.ContentTypeId == postContentTypeId)
                {
                    // get parent Forum.
                    invalidCacheList = AddParentForumCaches(item, invalidCacheList);
                }
            }

            // clear the cache for any forums that have had child pages published...
            foreach (var cache in invalidCacheList)
            {
                LogHelper.Info<SimpilyForumCacheHandler>("Clearing Forum Info Cache: {0}", () => cache);
                ApplicationContext.Current.ApplicationCache.RuntimeCache.ClearCacheByKeySearch(cache);
            }

        }

        private List<string> AddParentForumCaches(IContent item, List<string> cacheList)
        {

            var parent = item.Parent();

            if (parent != null)
            {
                if (parent.ContentTypeId == forumContentTypeId || parent.ContentTypeId == postContentTypeId)
                {
                    var cache = string.Format("simpilyforum_{0}", parent.Id);
                    if (!cacheList.Contains(cache))
                        cacheList.Add(cache);

                    cacheList = AddParentForumCaches(parent, cacheList);
                }
            }

            return cacheList;
        }
    }
}
