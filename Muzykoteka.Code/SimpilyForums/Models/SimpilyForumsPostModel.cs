﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Muzykoteka.Code.SimpilyForums.Models
{

    public class SimpilyForumsPostModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }

        [DisplayName("Title")]
        public string Title { get; set; }

        [Required]
        [AllowHtml]
        [DisplayName("Reply")]
        public string Body { get; set; }

        [Required]
        public int AuthorId { get; set; }
    }
}
