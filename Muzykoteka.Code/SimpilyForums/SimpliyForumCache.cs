﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Caching;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Muzykoteka.Code.SimpilyForums
{
    public static class SimpliyForumCache
    {
        public static SimpilyForumCacheItem GetForumInfo(this IPublishedContent item)
        {
            var cacheName = string.Format("simpilyforum_{0}", item.Id);
            var cache = UmbracoContext.Current.Application.ApplicationCache;
            var forumInfo = cache.GetCacheItem<SimpilyForumCacheItem>(cacheName);
            //if (forumInfo != null)
            //return forumInfo;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            // not in the cache, we have to make it.
            forumInfo = new SimpilyForumCacheItem();

            var posts = item.DescendantsOrSelf().Where(x => x.IsVisible() && x.DocumentTypeAlias == "Simpilypost");

            forumInfo.Count = posts.Count();
            if (posts.Any())
            {
                var lastPost = posts.OrderByDescending(x => x.UpdateDate).FirstOrDefault();
                forumInfo.latestPost.Date = lastPost.UpdateDate;
                forumInfo.latestPost.MemberId = lastPost.GetPropertyValue<int>("postAuthor");
                forumInfo.latestPost.Name = lastPost.GetPropertyValue<string>("postCreator");
            }

            cache.InsertCacheItem<SimpilyForumCacheItem>(cacheName, CacheItemPriority.Default, () => forumInfo);

            sw.Stop();
            LogHelper.Info<SimpilyForumCacheHandler>("Updated Cache for {0} [{1}] found {2} items in {2}ms",
                () => item.Name, () => item.Id, () => forumInfo.Count, () => sw.ElapsedMilliseconds);

            return forumInfo;
        }
    }
}
