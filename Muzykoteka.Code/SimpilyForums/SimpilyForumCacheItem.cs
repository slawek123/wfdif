﻿using System;

namespace Muzykoteka.Code.SimpilyForums
{
    public class SimpilyForumCacheItem : IComparable
    {
        public SimpilyForumCacheItem()
        {
            latestPost = new LastPostInfo();
        }
        public int Count { get; set; }
        public LastPostInfo latestPost { get; set; }

        public int CompareTo(object obj)
        {
            return /*latestPost.CompareTo(((SimpilyForumCacheItem)obj).latestPost);*/-1;
        }
    }
}
