﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Muzykoteka.Code
{
    public static class Extensions
    {
        public static string ToAmountString(this decimal d)
        {
            return d.Equals(Decimal.Truncate(d)) ? d.ToString("0 zł") : d.ToString("0.00 zł");
        }
        public static string EncodeBase64(this string value)
        {
            var valueBytes = Encoding.UTF8.GetBytes(value);
            return Convert.ToBase64String(valueBytes);
        }

        public static string DecodeBase64(this string value)
        {
            string s = value.Trim().Replace(" ", "+").Replace("-", "").Replace('_', '/');
            if (s.Length % 4 > 0)
                s = s.PadRight(s.Length + 4 - s.Length % 4, '=');
            var result = Encoding.UTF8.GetString(Convert.FromBase64String(s));
            return result;
        }
        public static bool EqualsOIC(this string value, string value2)
        {
            return value.Equals(value2, StringComparison.OrdinalIgnoreCase);
        }
        public static string Serialize(this object value) => JsonConvert.SerializeObject(value);
        public static T Deserialize<T>(this string value) => JsonConvert.DeserializeObject<T>(value);
        public static string GetBWCropUrl(this IPublishedContent mediaItem, string propertyAlias, string cropAlias, bool isGreyscale)
        {
            string ret = mediaItem.GetCropUrl(propertyAlias, cropAlias);
            if (!String.IsNullOrEmpty(ret) && isGreyscale)
            {
                ret += "&filter=greyscale";
            }
            return ret;
        }
    }
}
