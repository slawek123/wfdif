﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muzykoteka.Code.Models
{
    public class MAMAssetItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public MAMAssetItemType AssetType { get; set; }
        public string DASH_URL { get; set; }
        public string DASH_Key { get; set; }
        public string HLS_URL { get; set; }
        public string HLS_Key { get; set; }
        public string HLS_IV { get; set; }
    }
    public class MAMRequestData
    {
        public string Token { get; set; }
        public MAMAssetItem Asset { get; set; }
    }
    public class MAMAssetItemType
    {
        public string ID { get; set; }
        public string Name{ get; set; }
    }
}
