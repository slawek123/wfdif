﻿namespace Muzykoteka.Code.Models.ViewModels
{        
    public class InvoiceSummary 
    {
        public decimal Total { get; set; }
        public int Number { get; set; }
        public string Status { get; set; }
        public System.Guid PaymentMethod { get; set; }
    }
}