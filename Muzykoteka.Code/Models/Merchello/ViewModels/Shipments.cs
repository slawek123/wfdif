﻿using System;
using System.Collections.Generic;

namespace Muzykoteka.Code.Models.ViewModels
{
    public class Shipments 
    {
        public List<MKeyVal> methods;
        public Guid CurrentShipmentMethod;
    }
}