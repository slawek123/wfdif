using System;
namespace Muzykoteka.Code.Models
{    
    public class BasketViewModel
    {
        public decimal TotalPrice { get; set; }

        public decimal TotalShipment { get; set; }

        public decimal TotalQuantity { get; set; }

        public BasketViewLineItem[] Items { get; set; }

        public Guid? ShipmentMethod;

        public decimal TotalDiscount { get; set; }

        public string DiscountCode { get; set; }

        //public string Lang;        ??
    }
}