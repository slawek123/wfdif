using System;
using System.Linq;
using Merchello.Core;
using Merchello.Core.Models;
using Merchello.Web.Workflow;
using Merchello.Core.Checkout;
using Umbraco.Web;

namespace Muzykoteka.Code.Models
{
    /// <summary>
    /// Convert Merchello Product object to Displayable Product
    /// </summary>
    public static class BasketViewModelExtensions
    {
        public static BasketViewModel ToBasketViewModel(this IBasket basket, ICheckoutManagerBase checkoutManager)
        {
            decimal total_shipment = 0;
            Guid shipping_method_key = Guid.Empty;
                                     
            //var checkoutManager = GetCheckoutManagerSafe(); //basket.SalePreparation();
            //foreach (var shipQuote in preparation.ItemCache.Items.Where(x => x.LineItemType == LineItemType.Shipping))
            foreach (var shipQuote in checkoutManager.Context.ItemCache.Items.Where(x => x.LineItemType == LineItemType.Shipping))
            {
                shipping_method_key = shipQuote.ExtendedData.GetShipMethodKey();
                total_shipment += shipQuote.Price;
            }            

            decimal total_discount = 0;
            string discount_code = "";
            foreach (var di in basket.Items.Where(x=>x.LineItemType == LineItemType.Discount)) {
                total_discount += di.Price;
                discount_code = di.Sku;
            }

            return new BasketViewModel()
            {
                TotalPrice = basket.TotalBasketPrice,
                TotalQuantity = basket.TotalQuantityCount - (String.IsNullOrEmpty(discount_code) ? 0:1),
                Items = basket.Items.Where(x=>x.LineItemType == LineItemType.Product).Select(item => item.ToBasketViewLineItem()).OrderBy(x => x.Name).ToArray(),
                TotalShipment = total_shipment,
                TotalDiscount = total_discount, 
                ShipmentMethod = shipping_method_key,
                DiscountCode = discount_code
            };

        }

        /// <summary>
        /// Used to display extended data - for example purposes only
        /// </summary>
        /// <param name="extendedData"></param>
        /// <returns></returns>
        private static string DictionaryToString(ExtendedDataCollection extendedData)
        {
            var extendedDataAsString = string.Empty;

            foreach (var dataItem in extendedData)
            {
                extendedDataAsString += dataItem.Key + ":" + dataItem.Value + ";";
            }

            return extendedDataAsString;
        }

        /// <summary>
        /// Utility extension to map a <see cref="ILineItem"/> to a BasketViewLine item.
        /// Notice that the ContentId is pulled from the extended data. The name can either 
        /// be the Merchello product name via lineItem.Name or the Umbraco content page
        /// name with umbracoHelper.Content(contentId).Name
        /// 
        /// </summary>
        /// <param name="lineItem">The <see cref="ILineItem"/> to be mapped</param>
        /// <returns><see cref="BasketViewLineItem"/></returns>
        private static BasketViewLineItem ToBasketViewLineItem(this ILineItem lineItem)
        {                                    
            return new BasketViewLineItem()
            {
                Key = lineItem.Key,                
                ProductKey = Guid.Parse(lineItem.ExtendedData["productKey"]),
                ExtendedData = DictionaryToString(lineItem.ExtendedData),                
                Name = lineItem.Name,
                Sku = lineItem.Sku,
                UnitPrice = lineItem.Price,
                TotalPrice = lineItem.TotalPrice,
                Quantity = lineItem.Quantity
            };

        }
    }
}
