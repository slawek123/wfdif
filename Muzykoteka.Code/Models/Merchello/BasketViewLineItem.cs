using System;

namespace Muzykoteka.Code.Models
{
    public class BasketViewLineItem
    {       
        public Guid Key { get; set; }

        public Guid ProductKey { get; set; }

        public string Name { get; set; }

        public string ExtendedData { get; set; }

        public string Sku { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal TotalPrice { get; set; }

        public int Quantity { get; set; }
    }
}