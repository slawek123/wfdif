﻿using Merchello.Core;
using Merchello.Core.Models;
using System;
namespace Muzykoteka.Code.Models
{
    using System.ComponentModel.DataAnnotations;
    
    /// <summary>
    /// Summary description for AddressModel
    /// </summary>
    public class AddressModel
    {
        // for anon or mem customer
        public Guid CustomerKey { get; set; }

        // address line 1
        public string Address1 { get; set; }

        // address line 2
        public string Address2 { get; set; }

        // country code 
        public string CountryCode { get; set; }

        // email (no regex format enforced)
        public string Email { get; set; }

        // city, town, village
        public string Locality { get; set; }
        
        // customer name
        public string Name { get; set; }

        // phone
        public string Phone { get; set; }

        // postal code
        public string PostalCode { get; set; }
    }

    public static class AddressModelExtensions
    {
        public static IAddress ToAddress(this AddressModel address)
        {
            return new Address()
            {               
                
                Address1 = address.Address1,
                Address2 = address.Address2,
                CountryCode = address.CountryCode,
                Email = address.Email,
                Locality = address.Locality,
                Name = address.Name,               
                Phone = address.Phone,
                PostalCode = address.PostalCode,
                Region = "", //bez tego sie wykrzacza przy FullFill
            };
        }
    }
}