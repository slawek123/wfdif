﻿using Merchello.Core;
using Merchello.Core.Models;
namespace Muzykoteka.Code.Models
{
    public enum ShipmentZone
    {        
        Odbior,
        Poland,
        Europe,
        NorthAmerica,
        SouthAmerica,
        Australasia,
    }

    public class BothAddressModel 
    {
        public AddressModel shipment { get; set; }
        public AddressModel billing  { get; set; }
        public bool IsDelivery  { get; set; }
        public ShipmentZone DeliveryZone { get; set; }
    }
}
