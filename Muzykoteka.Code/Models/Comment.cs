﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartBlog
{

    public class Comment
    {
        [Required(ErrorMessage = "Błąd pobierania posta")]
        public Int32 intId { get; set; }

        [Display(Name = "Nazwa:")]
        [Required(ErrorMessage = "Nazwa jest wymagana")]
        public String strName { get; set; }

        [Display(Name = "Strona internetowa:")]
        public String strWebsite { get; set; }

        [Display(Name = "Email:")]
        [Required(ErrorMessage = "Email Jest Wymagany")]
        [RegularExpression(@"^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$", ErrorMessage = "Nie poprawny adress Email")]
        public String strEmail { get; set; }

        [Display(Name = "Data:")]
        public String strDate { get; set; }

        [Display(Name = "Komentarz:")]
        [Required(ErrorMessage = "Treść komentarza jest wymagana")]
        public String strComment { get; set; }

        [Display(Name = "Security Question: What is 2 + 2?")]
        [Required(ErrorMessage = "Security question is required")]
        [RegularExpression(@"^4$", ErrorMessage = "Invalid answer to security question")]
        public String strSecurity { get; set; }
    }
}
