﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Muzykoteka.Code
{
    [TableName("custom_hit_counter")]    
    [ExplicitColumns]
    public class VisitCounter
    {        
        [Column("node_id")]
        public int node_id { get; set; }

        [Column("views")]
        public int views { get; set; }
    }
}
   