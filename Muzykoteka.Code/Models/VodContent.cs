﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muzykoteka.Code.Models
{
    public class ItemDRM
    {
        public string Key { get; set; }
        public string KID { get; set; }
        public string IV { get; set; }
        public string Seed { get; set; }
    }

    public class DRM
    {
        public ItemDRM Fairplay { get; set; }
        public ItemDRM CENC { get; set; }
    }

    public class assetURL
    {
        public string DASH { get; set; }
        public string HLS { get; set; }
    }

    public class VodContent
    {
        public string inputPath { get; set; }
        public int id { get; set; }
        public string status { get; set; }
        public DRM DRM { get; set; }
        public assetURL assetURL { get; set; }
    }
}
