﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Muzykoteka.Code.Models
{
    public class EmailInfo
    {
        public string sender { get; set; }
        public string Emails { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Error { get; set; }
        public bool Ok { get; set; }
    }
    public class AddLesson
    {
        public int id { get; set; }
        public int parentId { get; set; }
        public IPublishedContent CurrentContent { get; set; }
        public string LessonName { get; set; }
        public string Articles { get; set; }
        public string Emails { get; set; }
        public DateTime LifeTime { get; set; }
        public string EmailText { get; set; }
        public List<byte[]> Attachment { get; set; }
        public string Error { get; set; }
        public bool Ok { get; set; }
        public bool SendEmail { get; set; }
        public bool SaveChecked { get; set; }
    }
    public class EdytujUczniow
    {
        public int id { get; set; }
        public List<IPublishedContent> UczniowieIstniejący { get; set; }
        public string NowiUczniowie { get; set; }
        public DateTime LeftTime { get; set; }
        public string Error { get; set; }
        public bool Ok { get; set; }
        public string EmailText { get; set; }
    }
}
