﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muzykoteka.Code.Models
{
    public class NinatekaEntity
    {
        public int Id { get; set; }
        public string Codename { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }        
        public string [,] Metadata { get; set; }                
    }

    public class NinatekaCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
