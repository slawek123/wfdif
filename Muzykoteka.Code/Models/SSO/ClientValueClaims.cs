﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class ClientValueClaims
    {
        public string ClientID { get; set; }
        public List<ClientValueClaimItem> Items { get; set; } = new List<ClientValueClaimItem>();
    }
}
