﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class ChangePassword :SSObase
    {
        [Required(ErrorMessage ="Obecne asło jest wymagene")]
        public string OldPassword { get; set; }
        [Required(ErrorMessage ="Nowe Hasło jest wymagane")]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "Hasła nie są takie same")]
        public string RepPassword { get; set; }

    }
}
