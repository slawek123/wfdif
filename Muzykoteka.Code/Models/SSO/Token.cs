﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class Token
    {
        public string client_id { get; set; }
        public string grant_type { get; set; }
        [Required(ErrorMessage = "Wpisz login")]
        public string username { get; set; }
        [Required(ErrorMessage = "Wpisz hasło")]
        public string password { get; set; }
        public string token { get; set; }
        public string code { get; set; }
        public string redirect_uri { get; set; }
        public string refresh_token { get; set; }
        public string CurrentPage { get; set; }
    }
}
