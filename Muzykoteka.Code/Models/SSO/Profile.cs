﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class Profile : UserRole
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NIP { get; set; }
        public string RIK { get; set; }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Akceptacja regulaminu oraz polityki prywatności jest wymagana")]
        public bool Agreement { get; set; }
        public ClientValueClaims Agreements { get; set; }
    }
}
