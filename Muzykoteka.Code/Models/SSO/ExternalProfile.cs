﻿using System;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class ExternalProfile : Profile
    {

        public string Provider { get; set; }
        public string ProviderKey { get; set; }
    }
}
