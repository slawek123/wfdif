﻿using System;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class ClientValueClaimItem
    {
        public string Claim { get; set; }
        public bool Active { get; set; }
    }
}
