﻿using System;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class ForgotEmail :SSObase
    {
        public string email { get; set; }
        public string token { get; set; }
    }
}
