﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class RessetPassword
    {
        public string ClientID { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string code { get; set; }
    }
}
