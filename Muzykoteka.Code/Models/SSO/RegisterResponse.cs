﻿using System;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class RegisterResponse
    {
        public string error { get; set; }
        public string error_description { get; set; }
    }
}
