﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class LoginResponse
    {
        public string access_token { get; set; }
        public string refresh_token { get; set; }
    }
}
