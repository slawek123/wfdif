﻿using System;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class ExternalCallback
    {
        public string code { get; set; }
        public string scope { get; set; }
        public string session_state { get; set; }
    }
}
