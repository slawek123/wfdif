﻿using System;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class UserRole : SSObase
    {
        public string InstitutionName { get; set; }
        public string InstitutionType { get; set; }
        public string InstitutionAdress { get; set; }
        public string InstitutionPhone { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public bool IsTeacher { get; set; }
        public bool IsStudent { get; set; }
        public bool IsPupil { get; set; }
        public bool IsPro { get; set; }

    }
}
