﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muzykoteka.Code.Models.SSO
{
    public class Register : UserRole
    {
        [Required(ErrorMessage = "Adres E-mail jest wymagany")]
        public string email { get; set; }
        [Required(ErrorMessage = "Login jest wymagany")]
        public string userName { get; set; }
        [Required(ErrorMessage = "Hasło jest wymagane")]
        public string password { get; set; }
        [Required(ErrorMessage = "Powtórz hasło jest wymagane")]
        [Compare("password", ErrorMessage = "Hasła nie są takie same")]
        public string confirmPassword { get; set; }
        [Required(ErrorMessage = "Imię jest wymagane")]
        public string firstName { get; set; }
        public string lastName { get; set; }
        
        public string token { get; set; }
        public ClientValueClaims agreements { get; set; }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Akceptacja regulaminu oraz polityki prywatności jest wymagana")]
        public bool regulamin { get; set; }
    }
}
