﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class FrontReset
    {
        [Required(ErrorMessage = "Adres E-mail jest wymagany")]
        [DataType(DataType.EmailAddress)]
        public string Prop1 { get; set; }
        [Required(ErrorMessage = "Hasło jest wymagane")]
        [DataType(DataType.Password)]
        public string Prop2 { get; set; }
        [Required(ErrorMessage = "Powtórz hasło jest wymagane")]
        [Compare("Prop2", ErrorMessage = "Hasła nie są takie same")]
        public string RePassword2 { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string code2 { get; set; }
    }
}
