﻿using System;
using System.Linq;

namespace Muzykoteka.Code.Models.SSO
{
    public class SsoUserInfo
    {
        public int nbf { get; set; }
        public int exp { get; set; }
        public string iss { get; set; }
        public string client_id { get; set; }
        public string sub { get; set; }
        public int auth_time { get; set; }
        public string idp { get; set; }
        public object role { get; set; }
        public string preferred_username { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public bool email_verified { get; set; }
        public string family_name { get; set; }
        public string given_name { get; set; }
        public string institution_name { get; set; }
        public string institution_type { get; set; }
        public string institution_phone { get; set; }
        public string institution_address { get; set; }
        public string province { get; set; }
        public string district { get; set; }
        public string hasPassword { get; set; }
        public string[] scope { get; set; }
        public string[] amr { get; set; }
        public string WaitingRole { get; set; }
        public string WaitingRole_teacher_ClientID { get; set; }
    }
}
