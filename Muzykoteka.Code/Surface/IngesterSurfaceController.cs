﻿using Muzykoteka.Code.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web;

namespace Muzykoteka.Code.Surface
{
    public class IngesterSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        [System.Web.Http.HttpPost]
        public ActionResult Video()
        {
            Stream req = Request.InputStream;
            req.Seek(0, SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            var AllowedIp = UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault(x => x.DocumentTypeAlias == "Zasoby")
                .GetPropertyValue<string[]>("adresIp");
            if (AllowedIp != null && AllowedIp.Contains(Request.UserHostAddress))
            {
                LogHelper.Info(typeof(IngesterSurfaceController), $"Incoming Message from {Request.UserHostAddress} was accepted Message Text:" +
                    $"{Environment.NewLine} {json.Replace("{", "{{").Replace("}", "}}")}");
                if (!string.IsNullOrEmpty(json))
                    Response.StatusCode = CreateArticle(json, "StreamVOD");
                else
                {
                    LogHelper.Warn(typeof(IngesterSurfaceController), "Empty Message");
                    Response.StatusCode = 400;
                }
            }
            else
            {
                LogHelper.Warn(typeof(IngesterSurfaceController), $"Incoming Message from {Request.UserHostAddress} was rejected: Not Allowed Ip AddressMessage Text:" +
                    $"{Environment.NewLine} {json.Replace("{", "{{").Replace("}", "}}")}");
                Response.StatusCode = 401;
            }
            return null;
        }
        [System.Web.Http.HttpPost]
        public ActionResult Audio()
        {
            Stream req = Request.InputStream;
            req.Seek(0, SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            var AllowedIp = UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault(x => x.DocumentTypeAlias == "Zasoby")
                .GetPropertyValue<string[]>("adresIp");
            if (AllowedIp != null && AllowedIp.Contains(Request.UserHostAddress))
            {
                LogHelper.Info(typeof(IngesterSurfaceController), $"Incoming Message from {Request.UserHostAddress} was accepted Message Text:" +
                    $"{Environment.NewLine} {json.Replace("{", "{{").Replace("}", "}}")}");
                if (!string.IsNullOrEmpty(json))
                    Response.StatusCode = CreateArticle(json, "StreamAOD");
                else
                {
                    LogHelper.Warn(typeof(IngesterSurfaceController), "Empty Message");
                    Response.StatusCode = 400;
                }
            }
            else
            {
                LogHelper.Warn(typeof(IngesterSurfaceController), $"Incoming Message from {Request.UserHostAddress} was rejected: Not Allowed " +
                    $"Ip AddressMessage Text:{Environment.NewLine} {json.Replace("{", "{{").Replace("}", "}}")}");
                Response.StatusCode = 401;
            }
            return null;
        }
        [System.Web.Http.HttpPost]
        public ActionResult Video360()
        {
            Stream req = Request.InputStream;
            req.Seek(0, SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            var AllowedIp = UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault(x => x.DocumentTypeAlias == "Zasoby")
                .GetPropertyValue<string[]>("adresIp");
            if (AllowedIp != null && AllowedIp.Contains(Request.UserHostAddress))
            {
                LogHelper.Info(typeof(IngesterSurfaceController), $"Incoming Message from {Request.UserHostAddress} was accepted Message Text:" +
                    $"{Environment.NewLine} {json.Replace("{", "{{").Replace("}", "}}")}");
                if (!string.IsNullOrEmpty(json))
                    Response.StatusCode = CreateArticle(json, "StreamVOD360");
                else
                {
                    LogHelper.Warn(typeof(IngesterSurfaceController), "Empty Message");
                    Response.StatusCode = 400;
                }
            }
            else
            {
                LogHelper.Warn(typeof(IngesterSurfaceController), $"Incoming Message from {Request.UserHostAddress} was rejected: Not Allowed " +
                    $"Ip AddressMessage Text:{Environment.NewLine} {json.Replace("{", "{{").Replace("}", "}}")}");
                Response.StatusCode = 401;
            }
            return null;
        }
        [System.Web.Http.HttpPost]
        public ActionResult Error()
        {
            Stream req = Request.InputStream;
            req.Seek(0, SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            var AllowedIp = UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault(x => x.DocumentTypeAlias == "Zasoby")
                .GetPropertyValue<string[]>("adresIp");
            if (AllowedIp != null && AllowedIp.Contains(Request.UserHostAddress))
            {
                LogHelper.Error(typeof(IngesterSurfaceController), $"Error Message {json.Replace("{", "{{").Replace("}", "}}")}", new Exception());
                Response.StatusCode = 200;
            }
            else
            {
                LogHelper.Warn(typeof(IngesterSurfaceController), $"Incoming Message from {Request.UserHostAddress} was rejected: Not Allowed " +
                    $"Ip AddressMessage Text:{Environment.NewLine} {json.Replace("{", "{{").Replace("}", "}}")}");
                Response.StatusCode = 401;
            }
            return null;
        }
        private int CreateArticle(string json, string type)
        {
            VodContent vodContent;
            try
            {
                vodContent = JsonConvert.DeserializeObject<VodContent>(json);
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(IngesterSurfaceController), "Invalid Content", ex);
                return 400;
            }
            LogHelper.Info(typeof(IngesterSurfaceController), $"Create new content");
            var newContent = Services.ContentService.CreateContent(name: vodContent.id + vodContent.inputPath, parentId: 37151, contentTypeAlias: type, userId: -1);
            newContent.SetValue("streamURL_DASH", vodContent.assetURL.DASH);
            newContent.SetValue("streamURL_HLS", vodContent.assetURL.HLS);
            newContent.SetValue("kluczDASH", vodContent.DRM.CENC.Key);
            newContent.SetValue("kluczHLS", vodContent.DRM.Fairplay.Key);
            newContent.SetValue("ivectorHLS", vodContent.DRM.Fairplay.IV);
            newContent.SetValue("dASH_KID", vodContent.DRM.CENC.KID);
            newContent.SetValue("dASH_SEED", vodContent.DRM.CENC.Seed);
            newContent.SetValue("hLS_KID", vodContent.DRM.Fairplay.KID);
            newContent.SetValue("inputPath", vodContent.inputPath);
            newContent.SetValue("incomingId", vodContent.id);
            newContent.SetValue("status", vodContent.status);
            LogHelper.Info(typeof(IngesterSurfaceController), $"Save new content");
            Services.ContentService.Save(newContent);
            LogHelper.Info(typeof(IngesterSurfaceController), $"Publish new content");
            Services.ContentService.Publish(newContent);
            return 201;
        }
    }
}
