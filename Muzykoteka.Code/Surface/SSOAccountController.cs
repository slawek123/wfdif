﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using Muzykoteka.Code.Models.SSO;
using System.Net.Http;
using System.Net;
using System.IO;
using Muzykoteka.Code.Services;
using Umbraco.Web.Mvc;
using System.Web;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System.Text;
using System.Web.Security;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Microsoft.AspNet.Identity;
using System.Collections;
using Newtonsoft.Json.Linq;
using Examine;
using Umbraco.Core.Logging;
using System.Web.Configuration;

namespace Muzykoteka.Code.Surface
{
    [JsonCamelCaseFormatter]
    public class SSOAccountController : SurfaceController
    {
        public readonly ProxySSO _proxySSO;
        public SSOAccountController()
        {
            _proxySSO = new ProxySSO();
        }
        [System.Web.Http.HttpPost]
        public ActionResult Token(Token model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();
            var result = SendToken(model);
            if (result != null)
            {
                var base64Url = result.access_token.Split('.')[1];
                var decoded = base64Url.DecodeBase64().Deserialize<SsoUserInfo>();
                UpdateUser(decoded, result.access_token, result.refresh_token); 
                var cookie = Request.Cookies.Get("ReturnCookie");
                if (cookie != null)
                {
                    var returnUrl = HttpUtility.UrlDecode(cookie.Value.ToString());
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Set(cookie);
                    return Redirect(returnUrl);
                }
                if (Url.IsLocalUrl(model.CurrentPage) && !model.CurrentPage.EqualsOIC("/login/"))
                {
                    return Redirect(HttpUtility.UrlDecode(model.CurrentPage));
                }
                return Redirect("/");
            }
            return CurrentUmbracoPage();
        }
        [System.Web.Http.HttpPost]
        public ActionResult RegisterItem([FromBody] Register model, string returnUrl)
        {
            ValidRole(model);
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }
            RegisterResponse result;
            HttpStatusCode status;
            (result, status) = _proxySSO.Register(model, returnUrl, Request.Headers["Accept-Language"]);
            try
            {
                if (status == HttpStatusCode.OK)
                {
                    TempData["Error"] = false;
                }
                else
                {
                    TempData["Error"] = true;
                    TempData["Message"] = result.error_description;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error<SSOAccountController>(model.Serialize(), ex);
                TempData["Error"] = true;
                TempData["Message"] = "Ups coś poszło nie tak";

            }
            return CurrentUmbracoPage();
        }
        [System.Web.Http.HttpPost]
        public ActionResult ForgotPassword([FromBody] ForgotEmail model, string returnUrl)
        {
            object result;
            HttpStatusCode status;
            (result, status) = _proxySSO.ForgotPassword(model, model.token, Request.Headers["Accept-Language"]);
            TempData["Ok"] = true;
            return CurrentUmbracoPage();
        }
        [System.Web.Http.HttpPost]
        public ActionResult ResetPassword(FrontReset model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();
            object result;
            HttpStatusCode status;
            var model2 = new RessetPassword()
            {
                code = model.code2,
                email = model.Prop1,
                password = model.Prop2
            };
            (result, status) = _proxySSO.ResetPassword(model2, Request.Headers["Accept-Language"]);
            if (status == HttpStatusCode.OK)
            {
                TempData["Ok"] = true;
            }
            else
            {
                TempData["Message"] = "Nie udało się zresetować hasła. sprawdź wprowadzone dane i spróbuj ponownie";

            }
            return CurrentUmbracoPage();
        }
        [System.Web.Http.HttpPost]
        public ActionResult ProfileItem(Profile model)
        {
            if (SaveProfile(model))
            {
                TempData["Error"] = false;
            }
            return CurrentUmbracoPage();
        }
        public ActionResult ExternalRegister(ExternalProfile model)
        {
            ValidRole(model);
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();
            object result;
            HttpStatusCode status;
            (result, status) = _proxySSO.ExtRegister(model);
            if (status == HttpStatusCode.OK)
            {
                TempData["Error"] = false;
                TempData["Provider"] = model.Provider;
            }
            else
            {
                TempData["Error"] = true;
                TempData["Message"] = "coś Poszło nie tak!";
            }
            return CurrentUmbracoPage();
        }
        public ActionResult UpdateProfile(Profile model)
        {
            ValidRole(model);
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();
            if (SaveProfile(model))
            {
                TempData["Message"] = "Zmiany zostały zapisane";
                return CurrentUmbracoPage();
            }
            TempData["Message"] = "Coś poszło nie tak";
            TempData["Error"] = true;
            return CurrentUmbracoPage();
        }
        public ActionResult ExternalCallback(ExternalCallback model)
        {
            string returnUrl = "/";
            var result = SendToken(new Token()
            {
                grant_type = "authorization_code",
                code = model.code,
                redirect_uri = WebConfigurationManager.AppSettings["sso_redirect"]
            });
            if (result != null)
            {
                var cookie = Request.Cookies.Get("ReturnCookie");
                if (cookie != null)
                {
                    returnUrl = HttpUtility.UrlDecode(cookie.Value.ToString());
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Set(cookie);
                }
                var base64Url = result.access_token.Split('.')[1];
                var decoded = base64Url.DecodeBase64().Deserialize<SsoUserInfo>();
                UpdateUser(decoded, result.access_token, result.refresh_token);
            }
            else
            {
                var login = Umbraco.TypedContentAtRoot().FirstOrDefault().Children().FirstOrDefault(x => x.DocumentTypeAlias == "Account").Children().FirstOrDefault(x => x.DocumentTypeAlias == "Login");
                returnUrl = login.Url;
                TempData["Message"] = "Ups! Coś poszło nie tak";
            }
            return Redirect(returnUrl);
        }
        public ActionResult ChangePassword(ChangePassword model)
        {
            if (!ModelState.IsValid) return CurrentUmbracoPage();
            TokenResponse result;
            HttpStatusCode status;
            var member = Services.MemberService.GetByUsername(HttpContext.Profile.UserName);
            var token = member.GetValue<string>("token");
            var refresh = member.GetValue<string>("adresat");
            var tokeResult = SendToken(new Token
            {
                grant_type = "refresh_token",
                refresh_token = refresh
            });
            if (tokeResult != null)
            {
                var base64Url = tokeResult.access_token.Split('.')[1];
                var decoded = base64Url.DecodeBase64().Deserialize<SsoUserInfo>();
                token = tokeResult.access_token;
                refresh = tokeResult.refresh_token;
                UpdateUser(decoded, token, refresh);
                (result, status) = _proxySSO.ChangePassword(model, token);
                if (status == HttpStatusCode.OK)
                {
                    TempData["Ok"] = true;
                }
                else
                {
                    TempData["Ok"] = false;
                    switch (result.error)
                    {
                        case "PasswordMismatch":
                            ModelState.AddModelError("OldPassword", "Błędne hasło");
                            break;
                        default:
                            TempData["Message"] = "Ups coś poszło nie tak spróbuj ponownie później";
                            break;
                    }
                }
            }
            return CurrentUmbracoPage();
        }
        public ActionResult DeleteAccount()
        {
            object result;
            HttpStatusCode status;
            var member = Services.MemberService.GetByUsername(HttpContext.Profile.UserName);
            var token = member.GetValue<string>("token");
            var refresh = member.GetValue<string>("adresat");
            var tokeResult = SendToken(new Token
            {
                grant_type = "refresh_token",
                refresh_token = refresh
            });
            if (tokeResult != null)
            {
                var base64Url = tokeResult.access_token.Split('.')[1];
                var decoded = base64Url.DecodeBase64().Deserialize<SsoUserInfo>();
                token = tokeResult.access_token;
                refresh = tokeResult.refresh_token;
                UpdateUser(decoded, token, refresh);
                (result, status) = _proxySSO.Delete(token);
                if (status == HttpStatusCode.OK)
                {
                    HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                    HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    FormsAuthentication.SignOut();
                    // Do some stuff here, then return the base method
                    return Redirect("/");
                }
                else
                {
                    TempData["OK"] = false;
                    TempData["Message"] = "UPS! coś poszło nie tak.";
                }
            }
            return CurrentUmbracoPage();
        }
        private TokenResponse SendToken(Token model)
        {

            TokenResponse result;
            HttpStatusCode status;
            if (string.IsNullOrEmpty(model.grant_type))
                model.grant_type = "password";
            (result, status) = _proxySSO.Token(model, Request.Headers["Accept-Language"]);
            if (status == HttpStatusCode.OK)
                return result;
            if (result.error.EqualsOIC("invalid_grant"))
            {
                TempData["Message"] = "Niepoprawny login lub hasło";
            }
            else
            {
                TempData["Message"] = "Ups coś poszło nie tak spróbuj ponownie później";
            }
            return null;
        }
        private bool SaveProfile(Profile model)
        {
            var member = Services.MemberService.GetByUsername(HttpContext.Profile.UserName);
            var token = member.GetValue<string>("token");
            var refresh = member.GetValue<string>("adresat");
            var tokeResult = SendToken(new Token
            {
                grant_type = "refresh_token",
                refresh_token = refresh
            });
            if (tokeResult != null)
            {
                var base64Url = tokeResult.access_token.Split('.')[1];
                var decoded = base64Url.DecodeBase64().Deserialize<SsoUserInfo>();
                token = tokeResult.access_token;
                refresh = tokeResult.refresh_token;
                UpdateUser(decoded, token, refresh);
                object result;
                HttpStatusCode status;
                (result, status) = _proxySSO.Profile(model, token, Request.Headers["Accept-Language"]);
                var newTokeResult = SendToken(new Token
                {
                    grant_type = "refresh_token",
                    refresh_token = refresh
                });
                if (status == HttpStatusCode.OK)
                {
                    var newbase64Url = newTokeResult.access_token.Split('.')[1];
                    decoded = newbase64Url.DecodeBase64().Deserialize<SsoUserInfo>();
                    token = newTokeResult.access_token;
                    refresh = newTokeResult.refresh_token;
                    UpdateUser(decoded, token, refresh);
                    return true;
                }
                else
                {
                    token = tokeResult.access_token;
                    refresh = tokeResult.refresh_token;
                    UpdateUser(decoded, token, refresh);
                    return false;
                }
            }
            return false;
        }
        private void UpdateUser(SsoUserInfo decoded, string access, string refresh)
        {

            string name = (string.IsNullOrEmpty(decoded.name) ? decoded.email : decoded.name);
            IEnumerable<string> aroles = null;
            if (decoded.role != null)
            {
                if (decoded.role.GetType().Name == typeof(JArray).Name)
                {
                    aroles = ((JArray)decoded.role).Values<string>().Select(x => (x == "pupil" ? "Uczeń" : (x == "teacher" ? "Nauczyciel" : (x == "student" ? "Student" : null)))).ToList();
                }
                else
                {
                    var item = (string)decoded.role;
                    aroles = new string[] { item == "pupil" ? "Uczeń" : (item == "teacher" ? "Nauczyciel" : (item == "student" ? "Student" : null)) };
                }

                aroles = aroles.Where(x => x != null);
            }
            var memberService = Services.MemberService;
            var member = memberService.GetByEmail(decoded.email);
            if (member != null && member.Comments != decoded.sub)
            {
                if (member.Comments == name)
                {
                    member.Comments = decoded.sub;
                }
                else
                {
                    var test = Umbraco.TypedContentAtRoot().First();
                    var item = test.Descendants("Users").FirstOrDefault(x => x.Name == member.Id.ToString());
                    if (item != null)
                    {
                        var del = Services.ContentService.GetById(item.Id);
                        Services.ContentService.Delete(del, 0);
                    }
                    Services.MemberService.Delete(member);
                    member = null;
                }
            }
            if (member == null)
            {
                member = memberService.CreateMemberWithIdentity(decoded.email, decoded.email, name, "member");
                member.RawPasswordValue = "*";
                member.Comments = decoded.sub;
                memberService.Save(member);
            }

            memberService.AssignRole(member.Id, "Standardowy");
            if (!aroles?.Contains("Uczeń") == true)
                memberService.DissociateRole(member.Id, "Uczeń");
            if (!aroles?.Contains("Nauczyciel") == true)
                memberService.DissociateRole(member.Id, "Nauczyciel");
            if (!aroles?.Contains("Student") == true)
                memberService.DissociateRole(member.Id, "Student");

            if (aroles?.Any() == true)
                memberService.AssignRoles(new[] { member.Id }, aroles.ToArray());
            member.Name = name;
            member.SetValue("token", access);
            member.SetValue("adresat", refresh);
            if (decoded.WaitingRole_teacher_ClientID == WebConfigurationManager.AppSettings["sso_client_id"])
                member.SetValue("teacherWait", !string.IsNullOrEmpty(decoded.WaitingRole));
            else
                member.SetValue("teacherWait", false);
            memberService.Save(member);
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            FormsAuthentication.SetAuthCookie(decoded.email, false);
        }
        private bool ValidRole(UserRole userRole)
        {
            var returnValue = false;
            if (userRole.IsPupil || userRole.IsStudent)
                returnValue = true;
            else if (userRole.IsTeacher)
            {
                var type = string.IsNullOrEmpty(userRole.InstitutionType);
                var district = string.IsNullOrEmpty(userRole.District);
                if (!type && !district)
                    returnValue = true;
                if (type)
                {
                    ModelState.AddModelError("InstitutionType", "Wybierz typ instytucji");
                    returnValue = false;
                }
                if (district)
                {
                    ModelState.AddModelError("District", "Wybierz typ instytucji");
                    returnValue = false;
                }
            }
            else
            {
                ModelState.AddModelError("IsTeacher", "Kim jesteś?");
                returnValue = false;
            }
            return returnValue;
        }
        public void Dispose()
        {
            foreach(var item in TempData.Keys)
            {
                TempData.Remove(item);
            }
            base.Dispose();
        }
    }
    public class LogoutController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();
            // Do some stuff here, then return the base method
            return Redirect("/");
        }
    }
    public class AccountController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            return Redirect("/");
        }
    }
}
