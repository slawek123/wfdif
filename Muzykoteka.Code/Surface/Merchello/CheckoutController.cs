﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using Merchello.Core;
using Merchello.Core.Gateways.Payment;
using Merchello.Core.Models;
using Merchello.Web;       
using Umbraco.Core;
using Umbraco.Web.Mvc;
using System.Configuration;
using Merchello.Web.Workflow;
using Umbraco.Web;
using Umbraco.Core.Models;
using Muzykoteka.Code.Models;
using Muzykoteka.Code.Models.ViewModels;


namespace Muzykoteka.Code { 

    [PluginController("MerchelloWfdifStore")]
    public class CheckoutController : MerchelloSurfaceContoller
    {

        const string NO_INVOICE_TXT = "(bez faktury)";

        public CheckoutController()
            : this(MerchelloContext.Current)
        { }

        public CheckoutController(IMerchelloContext merchelloContext)
            : base(merchelloContext)
        { }

        public ActionResult GetBasketCounter()
        {           
            bool bHaveDiscount = Basket.Items.Where(x => x.LineItemType == LineItemType.Discount).Any();
            ViewBag.Counter = Basket.TotalQuantityCount - (bHaveDiscount ? 1 : 0);
            return PartialView("BasketCounter");            
        }

        [HttpGet]
        public ActionResult DisplayBasketStatus()
        {
            return PartialView("BasketStatus", Basket.ToBasketViewModel(GetGetCheckoutManagerSafe()));
        }


        [ChildActionOnly]
        public ActionResult DisplayBasket()
        {
            return PartialView("Koszyk", Basket.ToBasketViewModel(GetGetCheckoutManagerSafe()));
        }
        

        [HttpGet]
        public ActionResult RemoveItemFromBasket(Guid lineItemKey, int parentId)
        {
            Basket.RemoveItem(lineItemKey);
            Basket.Save();
            return RedirectToUmbracoPage(parentId);
        }

        [HttpGet]
        public ActionResult RemoveAllFromBasket(int parentId)
        {
            var paymentMethod = Payment.GetPaymentGatewayMethodByKey(CheckoutController.GetPaymentKey(CheckoutController.WfdifPaymentMethod.Pobranie)).PaymentMethod;            
            Basket.GetCheckoutManager().Payment.SavePaymentMethod(paymentMethod);
            Basket.Empty();
            Basket.Save();
            return RedirectToUmbracoPage(parentId);
        }



        [HttpGet]
        public ActionResult RenderShipmentMethods()
        {                                   
            Shipments model = new Shipments();

            model.methods = new List<MKeyVal>();

            string[] ship_countries = new string[] { "PL" };//wyrzucic albo z web.configa, "US" };

            foreach (string c in ship_countries)
            {
                //### bez ktorejs wartosc jest exception, ktorej?
                var address = new Address()
                {
                    CountryCode = c,
                    Address1 = "x",
                    AddressType = AddressType.Shipping,
                    Email = "x",
                    Address2 = "x",
                    Locality = "PL",
                    IsCommercial = false,
                    Name = "x",
                    Organization = "x",
                    Phone = "123 123 123",
                    PostalCode = "02-001",
                    Region = "x",
                };
                var shipments = Basket.PackageBasket(address);
                foreach (var shipment in shipments)
                {
                    var quotes = shipment.ShipmentRateQuotes();

                    foreach (var quote in quotes)
                    {
                        string name = quote.ShipMethod.Name + " &#8208; " + quote.Rate.ToAmountString();                     
                        if (!model.methods.Where(x => String.Compare((string)x.Id, quote.ShipMethod.Key.ToString()) == 0).Any())
                        {
                            model.methods.Add(new MKeyVal()
                            {
                                Name = name,
                                Id = quote.ShipMethod.Key.ToString(),
                            });
                        }
                    }
                }
            }            
            model.CurrentShipmentMethod = GetCurrentShipmentMethod(Basket);
            return PartialView("Wysylka", model);
        }


        [HttpGet]
        public ActionResult SaveShipment(string key)
        {
            _SaveShipment(new Guid(key));
            return PartialView("BasketTotal", Basket.ToBasketViewModel(GetGetCheckoutManagerSafe()));
        }

        private void _SaveShipment(Guid gkey)
        {            
            var checkoutManager = GetGetCheckoutManagerSafe();
            var shiping = checkoutManager.Shipping;
            shiping.ClearShipmentRateQuotes();
                      
            bool bShipmentFound = false;
            for (int i = 0; i < 2 && !bShipmentFound; i++)
            {
                var address = new Address()
                {
                    CountryCode = (i == 0 ? "PL" : "XX"),
                    Address1 = "x",
                    AddressType = AddressType.Shipping,
                    Email = "x",
                    Address2 = "x",
                    Locality = "PL",
                    IsCommercial = false,
                    Name = "x",
                    Organization = "x",
                    Phone = "123 123 123",
                    PostalCode = "02-001",
                    Region = "x",
                };

                var shipments = Basket.PackageBasket(address);
                foreach (var s in shipments)
                {
                    var quotes = s.ShipmentRateQuotes();
                    foreach (var q in quotes)
                    {
                        if (q.ShipMethod.Key == gkey)
                        {
                            bShipmentFound = true;
                            shiping.SaveShipmentRateQuote(q);
                            var ship_item = checkoutManager.Context.ItemCache.Items.Where(x => x.LineItemType == LineItemType.Shipping).FirstOrDefault();
                            ship_item.ExtendedData.SetValue("ZONE", q.ShipMethod.Name.Substring(0, 1));
                            break;
                        }
                    }
                }
            }
            if (!bShipmentFound)
            {
                throw new Exception("Shipment method not found");
            }
        }


        [HttpGet]
        public ActionResult DisplayBasketTotal(int? clcid)
        {            
            return PartialView("BasketTotal", Basket.ToBasketViewModel(GetGetCheckoutManagerSafe()));// GetBasketTotalModel());
        }


        [HttpGet]
        public ActionResult UpdateQuantity(Guid key, int q)
        {
            Basket.UpdateQuantity(key, q);            
            Guid ship_method = GetCurrentShipmentMethod(Basket);
            Basket.Save();
            if (ship_method != Guid.Empty)
            {
                _SaveShipment(ship_method);
            }
            var item = Basket.Items.Where(x => x.Key == key).FirstOrDefault();
            if (item != null)
            {
                UpdateDiscount();
                return Content(item.TotalPrice.ToAmountString());
            }
            else
                return Content("?");
        }

        public enum WfdifPaymentMethod
        {
            Osobisty,
            Przelew,
            Dotpay,
            Pobranie
        }

        
        public static Guid GetPaymentKey(WfdifPaymentMethod pm)
        {
            string key =
               (pm == WfdifPaymentMethod.Dotpay ? "merchello_payment_dotpay_key" : 
               (pm == WfdifPaymentMethod.Osobisty ? "merchello_payment_osobisty_key" :
               (pm == WfdifPaymentMethod.Pobranie ? "merchello_payment_pobranie_key" :
               (pm == WfdifPaymentMethod.Przelew ? "merchello_payment_przelew_key" :
               ""))));
            return new Guid(ConfigurationManager.AppSettings[key]);
        }


        public static Guid GetShipmentKey(ShipmentZone zone)
        {
            string key =
                (zone == ShipmentZone.Odbior ? "merchello_shipment_osobisty_key" :
                ((zone == ShipmentZone.Poland ? "merchello_shipment_PL_key" :
                "")));
                /*
                (zone == ShipmentZone.Australasia ? "merchello_shipment_AA_key" :
                (zone == ShipmentZone.Europe ? "merchello_shipment_EU_key" :
                (zone == ShipmentZone.NorthAmerica ? "merchello_shipment_NA_key" :                
                
                (zone == ShipmentZone.SouthAmerica ? "merchello_shipment_SA_key" :
                ""))))));*/
            return new Guid(ConfigurationManager.AppSettings[key]);
        }





        private static Guid GetCurrentShipmentMethod(IBasket basket)
        {
            string zone;
            return GetCurrentShipmentMethod(basket, out zone);
        }

        private static Guid GetCurrentShipmentMethod(IBasket basket, out string zone)
        {
            Guid ret = Guid.Empty;
            zone = null;

            foreach (var shipQuote in basket.Items.Where(x=>x.LineItemType == LineItemType.Shipping))
            {
                ret = shipQuote.ExtendedData.GetShipMethodKey();
                zone = shipQuote.ExtendedData.GetValue("ZONE");
            }
            return ret;
        }
        

        public ActionResult DisplayBuyButton(AddItemModel product)
        {
            return PartialView("BuyButton", product);
        }

                
        public ActionResult AddToBasket(AddItemModel model)
        {            
            var extendedData = new ExtendedDataCollection();
            extendedData.SetValue("productKey", model.ProductKey.ToString());
            var product = Services.ProductService.GetByKey(model.ProductKey);
            if (product.SalePrice < product.Price)
            {
                extendedData.SetValue("ProdDiscount", "1");                
            }
            Basket.AddItem(product, product.Name, 1, extendedData);
            Basket.Save();

            bool bHaveDiscount = Basket.Items.Where(x => x.LineItemType == LineItemType.Discount).Any();
            ViewBag.Counter = Basket.TotalQuantityCount - (bHaveDiscount ? 1 : 0);
            return PartialView("BasketCounter");
        }


        public static List<MKeyVal> GetPaymentMethods(IBasket basket)      
        {            
            Guid current_shipment_method = GetCurrentShipmentMethod(basket);
            var ret = new List<MKeyVal>();
            Guid exceptKey;
            exceptKey = (current_shipment_method == GetShipmentKey(ShipmentZone.Odbior) ? new Guid() : GetPaymentKey(WfdifPaymentMethod.Osobisty));
            return MerchelloContext.Current.Gateways.Payment.GetPaymentGatewayMethods().Where(x => x.PaymentMethod.Key != exceptKey && x.PaymentMethod.Key != GetPaymentKey(WfdifPaymentMethod.Pobranie)).Select(x => new MKeyVal()
            {
                Id = x.PaymentMethod.Key.ToString(),
                Name = x.PaymentMethod.Name
            }).ToList();
            
        }


        public static List<MKeyVal> GetVODPaymentMethods()
        {            
            var ret = new List<MKeyVal>();
            
            return MerchelloContext.Current.Gateways.Payment.GetPaymentGatewayMethods()
                .Where(x => x.PaymentMethod.Key != GetPaymentKey(WfdifPaymentMethod.Pobranie) && x.PaymentMethod.Key != GetPaymentKey(WfdifPaymentMethod.Osobisty))
                .Select(x => new MKeyVal()
            {
                Id = x.PaymentMethod.Key.ToString(),
                Name = x.PaymentMethod.Name
            }).ToList();
        }


        public static Guid GetCurrentPaymentMethod(IBasket basket)
        {
            IPaymentMethod pm = GetGetCheckoutManagerSafe(basket).Payment.GetPaymentMethod();
            if (pm == null)
                return Guid.Empty;
            else
                return pm.Key;
        }
        

        [ChildActionOnly]
        public ActionResult RenderAddressForm()
        {         
            string current_ship_zone;

            //sprawdzenie czy w koszyku produkty do wyslania (ew tylko vod)
            var address = new Address()
            {
                CountryCode = "PL",
                Address1 = "x",
                AddressType = AddressType.Shipping,
                Email = "x",
                Address2 = "x",
                Locality = "PL",
                IsCommercial = false,
                Name = "x",
                Organization = "x",
                Phone = "123 123 123",
                PostalCode = "02-001",
                Region = "x",
            };
            var shipments = Basket.PackageBasket(address);
            if (shipments.Count() <= 0)
            {
                return new EmptyResult();
            }
            

            Guid current_shipment = GetCurrentShipmentMethod(Basket, out current_ship_zone);

            BothAddressModel am = new BothAddressModel();
            am.billing = new AddressModel();
            am.shipment = new AddressModel();
            am.IsDelivery = (current_shipment != GetShipmentKey(ShipmentZone.Odbior));
            am.shipment.CountryCode = am.billing.CountryCode = "PL";

            if (am.IsDelivery)
            {
                if (current_ship_zone == "A")
                    am.DeliveryZone = ShipmentZone.Europe;
                else if (current_ship_zone == "B")
                    am.DeliveryZone = ShipmentZone.NorthAmerica;
                else if (current_ship_zone == "C")
                    am.DeliveryZone = ShipmentZone.SouthAmerica;
                else if (current_ship_zone == "D")
                    am.DeliveryZone = ShipmentZone.Australasia;
                else //if (current_shipment == GetShipmentKey(ShipmentZone.Poland))
                    am.DeliveryZone = ShipmentZone.Poland;
            }

            var customer = CurrentCustomer;
            if (!customer.IsAnonymous)
            {
                ICustomer c = customer as ICustomer;
                foreach (var a in c.Addresses)
                {
                    if (a.AddressType == AddressType.Shipping || a.AddressType == AddressType.Billing)
                    {
                        AddressModel set_adr = (a.AddressType == AddressType.Shipping ? am.shipment : am.billing);

                        if (!String.IsNullOrEmpty(a.FullName))
                        {
                            set_adr.Name = a.FullName;
                        }
                        else
                        {
                            set_adr.Name = (c.FirstName + " " + c.LastName).Trim();
                            if (String.IsNullOrEmpty(am.shipment.Name))
                            {
                                //Nina.Code.NinaMemberProfile profile = (Nina.Code.NinaMemberProfile)System.Web.HttpContext.Current.Profile;
                                //set_adr.Name = (profile.imie + " " + profile.nazwisko).Trim();
                            }
                        }

                        set_adr.Email = c.Email;
                        if (String.IsNullOrEmpty(am.shipment.Email))
                        {
                            System.Web.Security.MembershipUser u = System.Web.Security.Membership.GetUser();
                            am.shipment.Email = u.Email;
                        }

                        set_adr.Address1 = a.Address1.Replace(NO_INVOICE_TXT, "");
                        set_adr.PostalCode = a.PostalCode;
                        set_adr.Locality = a.Locality;
                        set_adr.Phone = a.Phone;
                        set_adr.Address2 = (String.IsNullOrEmpty(a.Address2) ? a.Address2 : a.Address2.Replace("NIP:","").Trim()); //Label 'Nip:' dodawany przy zapisie, zeby pokazac adminowi merchello ze adress2 to nip
                        set_adr.CountryCode = (String.IsNullOrEmpty(a.CountryCode) ? "PL" : a.CountryCode);
                    }
                }
            }
            return PartialView("Address", am);
        }


        [HttpGet]
        public ActionResult SavePayment(string key)
        {
            var paymentMethod = Payment.GetPaymentGatewayMethodByKey(new Guid(key)).PaymentMethod;
            GetGetCheckoutManagerSafe().Payment.SavePaymentMethod(paymentMethod);            
            return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
        }



        [HttpGet]
        public ActionResult CheckVODDiscount(string p, string c)
        {            
            var node = GetDiscountCode(c);
            if (node == null)
            {
                return Content("");                
            }
            else
            {
                decimal discount = node.GetPropertyValue<decimal?>("kwota") ?? 0;
                var product = Services.ProductService.GetByKey(new Guid(p));
                if (product == null)
                {
                    return Content("");
                }
                else
                {
                    decimal price = ((product.SalePrice??0) < discount ? 0 : (product.SalePrice??0) - discount);
                    return Content(price.ToAmountString());
                }                
            }                
        }


        [HttpPost]        
        public ActionResult CheckoutVOD(string productKey, string paymentKey, string kodRabatowy)
        {
            var checkoutManager = GetGetCheckoutManagerSafe();
            var paymentMethod = Payment.GetPaymentGatewayMethodByKey(new Guid(paymentKey)).PaymentMethod;
            checkoutManager.Payment.SavePaymentMethod(paymentMethod);

            var product = Services.ProductService.GetByKey(new Guid(productKey));
            
            if (!checkoutManager.Payment.IsReadyToInvoice())
            {
                System.Web.Security.MembershipUser u = System.Web.Security.Membership.GetUser();
                string _email = (u == null ? null : u.Email);
                                
                var adr = new Address()
                {

                    Address1 = " ",
                    Address2 = " ",
                    CountryCode = "PL",
                    Email = _email,
                };
                checkoutManager.Customer.SaveBillToAddress(adr);
                if (!checkoutManager.Payment.IsReadyToInvoice())
                {
                    throw new Exception("Invalid invoice data");
                }
            }            

            IInvoice invoice = checkoutManager.Payment.PrepareInvoice();

            var new_it = new InvoiceLineItem(LineItemType.Product, product.Name, product.Sku, 1, product.SalePrice??0, new ExtendedDataCollection());
            invoice.Items.Add(new_it);
            invoice.Total = new_it.Price;

            if (!String.IsNullOrEmpty(kodRabatowy))
            {
                var node = GetDiscountCode(kodRabatowy);
                if (node == null)
                {
                    throw new Exception("Nieprawidłowy kod rabatowy");
                }
                decimal discount = node.GetPropertyValue<decimal?>("kwota") ?? 0;                
                var discountItem = new InvoiceLineItem(LineItemType.Discount, "Rabat " + kodRabatowy, kodRabatowy, 1, -discount);
                invoice.Items.Add(discountItem);
                invoice.Total += discountItem.Price;
                ExpireDiscountCode(kodRabatowy);
            }

            string email = (CurrentCustomer as ICustomer).LoginName;
                                                            
            IPaymentResult attempt = invoice.AuthorizePayment(paymentMethod.Key);
            attempt = invoice.CapturePayment(attempt.Payment.Result, paymentMethod.Key, 0, null);

            Merchello.Core.Notification.Trigger("OrderConfirmation", attempt, new[] { email });

            var receipt = UmbracoContext.ContentCache.GetAtRoot()
                .DescendantsOrSelf("Sklep")
                .DescendantsOrSelf("Podsumowanie")                
                .SingleOrDefault();
                                
            if (paymentMethod.Key == GetPaymentKey(WfdifPaymentMethod.Dotpay))
            {                
                return Redirect(DotpayHelper.GetReturnURL(receipt, invoice));
            }
            else
            {
                return Redirect(string.Format("{0}?t=v&inv={1}",
                    receipt.Url,
                    (invoice == null ? "" : Server.UrlEncode(DotpayHelper.AesEncrypt(invoice.InvoiceNumber.ToString())))));
            }            
        }

        [HttpPost]
        public ActionResult SaveAddress(BothAddressModel model)
        {
            if (Basket.Items.Count <= 0)
            {
                throw new Exception("Basket is empty");
            }
                                    
            foreach (var it in Basket.Items)
            {
                var bitem = Basket.Items.Where(x => x.Sku == it.Sku).FirstOrDefault();
                if (bitem != null)
                    it.Quantity = bitem.Quantity;
            }
            
            bool bShipToAddressEmpty;
            if (bShipToAddressEmpty = (model.shipment == null))
            {
                System.Web.Security.MembershipUser u = System.Web.Security.Membership.GetUser();                
                model.shipment = new AddressModel()
                {
                    Address1 = " ",
                    Address2 = " ",
                    CountryCode = "PL",
                    Email = (u == null ? null : u.Email),
            };
            }
            if (String.IsNullOrEmpty(model.shipment.CountryCode))
                model.shipment.CountryCode = "PL";
            if (model.billing == null)
                model.billing = new AddressModel();
            if (String.IsNullOrEmpty(model.billing.Name))
            {
                //wykrzaczanie merchello, prawdopodobnie przez nule w adresie biling                
                model.billing.Name = model.shipment.Name;
                model.billing.Address1 = NO_INVOICE_TXT;
                model.billing.Address2 = "";
                model.billing.CountryCode = "";
                model.billing.Email = "";
                model.billing.Locality = "";
                model.billing.Phone = "";
                model.billing.PostalCode = "";                                
            }
            else
            {
                if (!String.IsNullOrEmpty(model.billing.Address2))
                {
                    model.billing.Address2 = "NIP: " + model.billing.Address2;
                }
            }

            var checkoutManager = GetGetCheckoutManagerSafe();
            if (!bShipToAddressEmpty)
                checkoutManager.Customer.SaveShipToAddress(model.shipment.ToAddress());
            checkoutManager.Customer.SaveBillToAddress(model.billing.ToAddress());
            
            IPaymentMethod paymentMethod = checkoutManager.Payment.GetPaymentMethod();                        
            IInvoice invoice;
            IPaymentResult attempt;
          
            if (!checkoutManager.Payment.IsReadyToInvoice()) //return RedirectToUmbracoPage(BasketPageId);
                throw new Exception("Invalid invoice data");
            
            invoice = checkoutManager.Payment.PrepareInvoice();
            
            //PrepareInvoice nie dodaje items z koszyka
            foreach (var it in Basket.Items)
            {                
                if (it.LineItemType == LineItemType.Product)
                {
                    var new_it = new InvoiceLineItem(LineItemType.Product, it.Name, it.Sku, it.Quantity, it.Price, it.ExtendedData);
                    invoice.Items.Add(new_it);
                    invoice.Total += it.Quantity * new_it.Price;
                }
                else if (it.LineItemType == LineItemType.Discount)
                {
                    var discount = new InvoiceLineItem(LineItemType.Discount, "Rabat " + it.Sku, it.Sku, 1, it.Price * -1);
                    invoice.Items.Add(discount);
                    invoice.Total += it.Price;
                    ExpireDiscountCode(it.Sku);
                }
            }

            //AUTHORIZE PAYMENT
            attempt = invoice.AuthorizePayment(paymentMethod.Key);
            attempt = invoice.CapturePayment(attempt.Payment.Result, paymentMethod.Key, 0, null);

            //NOTIFICATION               
            IAddress ship_to = checkoutManager.Customer.GetShipToAddress();
            if (ship_to != null && !String.IsNullOrEmpty(ship_to.Email))
            {
                Merchello.Core.Notification.Trigger("OrderConfirmation", attempt, new[] { ship_to.Email });
            }

            //UPDATE ADRES
            if (model.shipment != null)
            {
                foreach (var it in invoice.Items)
                {
                    if (it.LineItemType == LineItemType.Shipping)
                    {
                        it.ExtendedData.AddAddress(model.shipment.ToAddress(), AddressType.Shipping);
                        Services.InvoiceService.Save(invoice);
                        break;
                    }
                }
            }


            UpdateCustomerAddress(model.billing, AddressType.Billing);
            UpdateCustomerAddress(model.shipment, AddressType.Shipping);

            //receipt
            var receipt = UmbracoContext.PublishedContentRequest.PublishedContent.Parent.Children.Where(x => x.DocumentTypeAlias == "Podsumowanie").FirstOrDefault();
            if (paymentMethod.Key == GetPaymentKey(WfdifPaymentMethod.Dotpay))
            {
                ClearBasket();//moze nie być powrotu z dotpaya, zeby wyczyscic koszyk w podsumowaniu                
                return Redirect(DotpayHelper.GetReturnURL(receipt, invoice));
            }
            else
            {
                return Redirect(string.Format("{0}?inv={1}",
                    receipt.Url,
                    (invoice == null ? "" : Server.UrlEncode(DotpayHelper.AesEncrypt(invoice.InvoiceNumber.ToString())))));
            }
        }


        private void UpdateCustomerAddress(AddressModel adres, AddressType type)
        {
            var customer = CurrentCustomer;
            if (adres == null || customer.IsAnonymous || String.IsNullOrEmpty(adres.Address1) || String.IsNullOrEmpty(adres.Name))
                return;

            bool bFound = false;
            ICustomer c = customer as ICustomer;
            foreach (var a in c.Addresses)
            {
                if (a.AddressType == type)
                {
                    bFound = true;
                    a.FullName = adres.Name;
                    a.Address1 = adres.Address1;
                    a.CountryCode = adres.CountryCode;
                    a.Locality = adres.Locality;
                    a.Phone = adres.Phone;
                    a.PostalCode = adres.PostalCode;
                    a.Address2 = adres.Address2;
                }
            }
            if (!bFound)
            {
                string name = (type == AddressType.Shipping ? "Adres dostawy" :
                              (type == AddressType.Billing ? "Adres do faktury" :
                               ""));
                var new_adr = c.CreateCustomerAddress(adres.ToAddress(), name, type);
            }
            if (CurrentCustomer is ICustomer)
            {
                MerchelloContext.Current.Services.CustomerService.Save((ICustomer)CurrentCustomer);
            }
        }


        public void ClearBasket()
        {                        
            Basket.Empty();
            Basket.Save();
        }


        public ActionResult RenderInvoiceSummary(string invoiceNumber, string status)
        {
            string invoiceNumberDecoded = DotpayHelper.AesDecrypt(invoiceNumber);
            var invoice = Services.InvoiceService.GetByInvoiceNumber(Convert.ToInt32(invoiceNumberDecoded));
            if (invoice == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
            }

            var node = UmbracoContext.PublishedContentRequest.PublishedContent;

            Guid payment_method;
            if (status != null)
            {
                payment_method = GetPaymentKey(WfdifPaymentMethod.Dotpay);
            }
            else
            {
                IPaymentMethod pm = GetGetCheckoutManagerSafe().Payment.GetPaymentMethod();                
                if (pm != null)
                {
                    payment_method = pm.Key;
                }
                else
                {
                    throw new Exception("Payment method not found");
                }
            }

            InvoiceSummary model = new InvoiceSummary()
            {
                Total = invoice.Total,
                Number = invoice.InvoiceNumber,
                Status = status,
                PaymentMethod = payment_method,
            };

            ClearBasket();

            return PartialView("Podsumowanie", model);
        }


        public bool _SetDiscount(string c)
        {
            var node = GetDiscountCode(c);
            if (node == null)
            {
                return false;
            }
            else
            {
                Basket.Items.RemoveAll(x => x.LineItemType == LineItemType.Discount);
                decimal discount = CalcDiscountByCode(node.GetPropertyValue<decimal?>("kwota"), false, true);
                ItemCacheLineItem moneyOffItem = new ItemCacheLineItem(LineItemType.Discount, "Rabat", c, 1, -1 * discount);
                Basket.AddItem(moneyOffItem);
                Basket.Items.Where(x => x.Sku == c).FirstOrDefault().Price = -1 * discount;
                return true;
            }
        }

        private void UpdateDiscount()
        {
            var ditem = Basket.Items.Where(x => x.LineItemType == LineItemType.Discount).FirstOrDefault();
            if (ditem != null)
            {
                _SetDiscount(ditem.Sku);
            }
        }

        [HttpGet]
        public ActionResult SetDiscount(string c)
        {
            return Content(_SetDiscount(c) ? "1" : "0");
        }


        private decimal CalcDiscountByCode(decimal? amount, bool percent, bool bApplyToDiscountedProducts)
        {
            if (amount == null || amount <= 0)
                return 0;

            decimal total_to_discount = 0;
            var items = (bApplyToDiscountedProducts ?
                Basket.Items.Where(x => x.LineItemType == LineItemType.Product) :
                Basket.Items.Where(x => x.LineItemType == LineItemType.Product && !x.ExtendedData.Where(y => y.Key == "ProdDiscount").Any()));
            foreach (var it in items)
            {
                total_to_discount += it.TotalPrice;
            }
            if (total_to_discount <= 0)
                return 0;

            decimal to_discount = (percent ?
                total_to_discount * (decimal)amount / 100
                :
                Math.Min((decimal)amount, total_to_discount));

            return to_discount;
        }


        private IPublishedContent GetDiscountCode(string c)
        {
            return UmbracoContext.ContentCache.GetAtRoot()                   
                   .FirstOrDefault(x => x.DocumentTypeAlias == "KodyRabatowe")
                   .Descendants()
                   .FirstOrDefault(x => x.Name == c);
        }       


        private void ExpireDiscountCode(string c)
        {
            var node = GetDiscountCode(c);
            if (node != null)
            {
                var cs = UmbracoContext.Application.Services.ContentService;
                var cn = cs.GetById(node.Id);
                cs.UnPublish(cn);
            }
        }

        public static string UpdateDotpayStatus()
        {
            return DotpayHelper.UpdateInvoiceStatus();
        }

        [ChildActionOnly]
        public ActionResult DisplayOrders()
        {
            return PartialView("Zamowienia", GetMyOrders(GetGetCheckoutManagerSafe()));
        }


        public OrdersViewModel GetMyOrders(Merchello.Core.Checkout.ICheckoutManagerBase checkout) 
        {            
            var merchelloHelper = new MerchelloHelper();
            var inv = merchelloHelper.Query.Invoice.GetByCustomerKey(CurrentCustomer.Key);                
            return new OrdersViewModel()
            {
                Items = inv.ToArray()
            };
        }
    }
}
