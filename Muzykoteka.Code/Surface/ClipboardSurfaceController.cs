﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;
using System.Net;
using Umbraco.Core.Logging;
using System.Web.Mvc;
using System.Web.Security;
using System.Globalization;
using Umbraco.Core.Models;


namespace Muzykoteka.Code
{
    public class ClipboardSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        public ActionResult GetCounter()
        {
            var lst = GetCurrentClpbList();
            ViewBag.Counter = lst == null ? 0 : lst.Count;
            return PartialView("ClipboardCounter");
        }
        [HttpGet]
        public ActionResult AddContent(int id)
        {
            List<int> lst = GetCurrentClpbList();
            if (lst == null)
                lst = new List<int>();
            else
                CheckNonExistentIDs(ref lst);
            if (!lst.Contains(id))
            {
                lst.Add(id);
                UpdateList(lst);
            }
            else
            {
                lst.Remove(id);
                UpdateList(lst);
            }
            ViewBag.Counter = lst.Count;
            return PartialView("ClipboardCounter");
        }


        [HttpGet]
        public ActionResult DelContent(int id)
        {
            List<int> lst = GetCurrentClpbList();
            lst.Remove(id);
            CheckNonExistentIDs(ref lst);
            UpdateList(lst);
            ViewBag.Counter = lst.Count;
            return Content(id.ToString());
        }


        [HttpGet]
        public ActionResult DeleteAll()
        {
            List<int> lst = new List<int>();
            UpdateList(lst);
            ViewBag.Counter = lst.Count;
            return PartialView("ClipboardCounter");
        }


        private void UpdateList(List<int> lst)
        {
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (System.Web.HttpContext.Current.Profile != null)
                {
                    ((MuzykotekaMemberProfile)System.Web.HttpContext.Current.Profile).schowek = String.Join(",", lst.ToArray());
                }
            }
            else
            {
                Session["clpb"] = lst;
                var cookie_val = String.Join(",", lst.ToArray());
                if (Response.Cookies["mzclipboard"] == null)
                {
                    var c = new System.Web.HttpCookie("mzclipboard", cookie_val);
                    c.Expires = DateTime.Now.AddYears(20);
                    Response.Cookies.Add(c);
                }
                else
                {
                    Response.Cookies["mzclipboard"].Value = cookie_val;
                    Response.Cookies["mzclipboard"].Expires = DateTime.Now.AddYears(20);
                }
            }
        }


        private void CheckNonExistentIDs(ref List<int> lst)
        {
            var existing = Umbraco.TypedContent(lst.ToArray());
            lst = existing.Select(x => x.Id).ToList();
        }


        public static List<int> GetCurrentClpbList()
        {
            List<int> lst = null;
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                string schowek = System.Web.HttpContext.Current.Profile != null ? ((MuzykotekaMemberProfile)System.Web.HttpContext.Current.Profile).schowek : "";
                if (!String.IsNullOrEmpty(schowek))
                {
                    try
                    {
                        lst = schowek.Split(',').Select(x => int.Parse(x)).ToList();
                    }
                    catch { }
                }
            }
            else if (System.Web.HttpContext.Current.Session["clpb"] == null)
            {
                var c = System.Web.HttpContext.Current.Request.Cookies["mzclipboard"];
                if (c != null && !String.IsNullOrEmpty(c.Value))
                {
                    try
                    {
                        lst = c.Value.Split(',').Select(x => int.Parse(x)).ToList();
                    }
                    catch
                    {
                        lst = new List<int>();
                    }
                }
                else
                {
                    lst = new List<int>();
                }
                System.Web.HttpContext.Current.Session["clpb"] = lst;
            }
            else
            {
                lst = (List<int>)System.Web.HttpContext.Current.Session["clpb"];
            }
            return lst;
        }

        public static string GetShareContent()
        {
            var lst = GetCurrentClpbList();
            if (lst != null)
                return string.Join("|", lst);
            return "";
        }
    }
}
