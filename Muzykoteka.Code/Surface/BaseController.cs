﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web;
using Umbraco.Core.Services;
using Umbraco.Core.Logging;

namespace UmbracoIdentityServer.Client.Controllers
{
    public class BaseController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            LogHelper.Info<BaseController>("base controler strat");
            if (model.Content.DocumentTypeAlias == "RegisterExternal")
            {
                return base.Index(model);
            }
            if (HttpContext.Profile.IsAnonymous)
                return base.Index(model);
            var member = Services.MemberService.GetByUsername(HttpContext.Profile.UserName);
            LogHelper.Info<BaseController>($"loged member info {member}");
            if (member != null && (System.Web.Security.Roles.IsUserInRole("Uczeń") || System.Web.Security.Roles.IsUserInRole("Nauczyciel") || System.Web.Security.Roles.IsUserInRole("Student") || member.GetValue<bool>("teacherWait")))
                return base.Index(model);
            LogHelper.Info<BaseController>("member no role");
            var homeNode = model.Content.AncestorOrSelf(1);
            var node = homeNode?.Children.FirstOrDefault(x=>x.DocumentTypeAlias=="Account").Children().FirstOrDefault(x => x.DocumentTypeAlias == "RegisterExternal");
            if (node == null)
                return base.Index(model);
            return Redirect(node.Url);
        }
    }
}