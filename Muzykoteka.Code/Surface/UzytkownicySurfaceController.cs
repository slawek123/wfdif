﻿using Muzykoteka.Code.Surface;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace UmbracoIdentityServer.Client.Controllers
{
    public class UzytkownicyController : RenderMvcController
    {
        [HttpGet]
        public override ActionResult Index(Umbraco.Web.Models.RenderModel model)
        {
            var member = Members.GetCurrentMember();
            var currentNode = UmbracoContext.Current.PublishedContentRequest.PublishedContent;
            if (member != null)
            {
                if (!currentNode.Children.Any(x => x.DocumentTypeAlias == "NaukaArtykul"))
                {
                    var kurwa = Services.ContentService.CreateContent(name: "Lekcja", parentId: currentNode.Id, contentTypeAlias: "NaukaArtykul", userId: -1);
                    LogHelper.Info(typeof(IngesterSurfaceController), $"Save new content");
                    Services.ContentService.Save(kurwa);
                    LogHelper.Info(typeof(IngesterSurfaceController), $"Publish new content");
                    Services.ContentService.Publish(kurwa);
                }
                if (!currentNode.Children.Any(x => x.DocumentTypeAlias == "Utwory"))
                {
                    var kurwa = Services.ContentService.CreateContent(name: "Utwór", parentId: currentNode.Id, contentTypeAlias: "Utwory", userId: -1);
                    LogHelper.Info(typeof(IngesterSurfaceController), $"Save new content");
                    Services.ContentService.Save(kurwa);
                    LogHelper.Info(typeof(IngesterSurfaceController), $"Publish new content");
                    Services.ContentService.Publish(kurwa);
                }
                var memberService = Services.MemberService;
                string ShowLesson = Request.QueryString["ShowLesson"];
                if (memberService.GetMembersInRole("Nauczyciel").Any(x => x.Id == member.Id))
                {
                    var UserPage = currentNode.Children.FirstOrDefault(x => x.Name == member.Id.ToString());
                    if (UserPage != null)
                    {
                        return Redirect($"{UserPage.Url}\\{(ShowLesson != null ? "?ShowLesson=" + ShowLesson : "")}");
                    }
                    else
                    {
                        LogHelper.Info(typeof(IngesterSurfaceController), $"Create new content");
                        var newContent = Services.ContentService.CreateContent(name: member.Id.ToString(), parentId: currentNode.Id, contentTypeAlias: "Users", userId: -1);
                        newContent.SetValue("userId", member.Name);
                        LogHelper.Info(typeof(IngesterSurfaceController), $"Save new content");
                        Services.ContentService.Save(newContent);
                        LogHelper.Info(typeof(IngesterSurfaceController), $"Publish new content");
                        Services.ContentService.Publish(newContent);
                        return Redirect($"{currentNode.Url}\\{member.Id}{(ShowLesson != null ? "?ShowLesson=" + ShowLesson : "")}");
                    }
                }
            }
            var temp = $"{WebConfigurationManager.AppSettings["sso_host"]}/Account/Login";
            LogHelper.Info(typeof(UzytkownicyController), temp);
            return Redirect(temp);
        }
    }
}