﻿using System;
using System.Linq;
using Umbraco.Web;
using System.Web.Mvc;
using Merchello.Web.Models.ContentEditing;
using Merchello.Web;

namespace Muzykoteka.Code
{
    public enum EProductOrderStatus
    {
        Unknown = 0,
        Bought = 1,
        InProcess = 2,
        NoEntry = 3,
    }

    public class StoreSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        public static EProductOrderStatus IsProductBought(UmbracoContext ctx, string SKU)
        {                                    
            var customerContext = new CustomerContext(ctx);
            var currentCustomer = customerContext.CurrentCustomer;
            if (currentCustomer != null)
            {
                string s_expirt_after_days = System.Configuration.ConfigurationManager.AppSettings["merchello_vod_days_expire"];
                int i_expirt_after_days = 0;
                if (!String.IsNullOrEmpty(s_expirt_after_days))
                {
                    if (!Int32.TryParse(s_expirt_after_days, out i_expirt_after_days))
                        i_expirt_after_days = 0;
                }

                var merchelloHelper = new MerchelloHelper();
                var invoices = merchelloHelper.Query.Invoice.SearchByCustomer(currentCustomer.Key, 1, 10000);
                var invoices4Product = invoices.Items.OfType<InvoiceDisplay>().Where(x => x.Items.Where(y => y.Sku == SKU).Any()).ToList();                
                if (i_expirt_after_days > 0)
                {
                    var dt = DateTime.Now.AddDays(-i_expirt_after_days);
                    if (invoices4Product.Where(x => x.InvoiceDate >= dt && x.InvoiceStatusKey == Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Paid).Any())
                        return EProductOrderStatus.Bought;                                        
                }
                else
                {
                    if (invoices4Product.Where(x => x.InvoiceStatusKey == Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Paid).Any())
                        return EProductOrderStatus.Bought;
                }
                

                if (invoices4Product.Where(x => x.InvoiceStatusKey == Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Partial
                        || x.InvoiceStatusKey == Merchello.Core.Constants.DefaultKeys.InvoiceStatus.Unpaid).Any())
                {
                    return EProductOrderStatus.InProcess;
                }                
            }
            return EProductOrderStatus.NoEntry;
        }

        [HttpGet]
        public ActionResult GetPage(int page, int parentNodeId, int category)
        {
            //TODO: zapisac property w produkcje, zaleznie od doctypu/kategorii. To pozwoli na merchelloHelper.Query.Product.Search(term..., gdzie term='where (ta wlasciwosc/kategoria)=category). ???

            var itemsPerPage = 15;
            string sortBy = "nazwa";
            
            var merchelloHelper = new MerchelloHelper();                        
            var products = merchelloHelper.Query.Product.GetProductsOnSale(1, 10000, sortBy, Merchello.Core.Persistence.Querying.SortDirection.Ascending);
            var items = products.Items.OfType<ProductDisplay>();

            string docType = null;
            switch (category)
            {
                case 1:
                    docType = "ProduktDVD";
                    break;
                case 2:
                    docType = "ProduktKsiazka";
                    break;
                case 3:
                    docType = "ProduktKoszulka";
                    break;
                case 4:
                    docType = "SubskrypcjaVOD";
                    break;
            }
            if (docType != null)
            {
                items = items.Where(x => x.AsProductContent().DocumentTypeAlias == docType);
            }

            if (items.Count() <= 0)
            {
                return PartialView("EmptyList");
            }
            else
            {                
                ViewBag.ParentNodeId = parentNodeId;
                ViewBag.nodes = items;
                ViewBag.TotalPages = products.TotalPages;
                ViewBag.PageNum = page;
                return PartialView("Produkty");
            }
        }                
    }      
}
