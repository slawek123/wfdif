﻿using Muzykoteka.Code.Models;
using Muzykoteka.Code.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Muzykoteka.Code.Surface
{
    public class LessonController : SurfaceController
    {
        [HttpPost]
        public ActionResult CreateLesson(AddLesson lesson)
        {
            lesson.CurrentContent = Umbraco.TypedContent(lesson.parentId);
            string error = "";
            if (string.IsNullOrEmpty(lesson.LessonName))
            {
                lesson.Error = "Podaj tytuł lekcji";
                return PartialView("DodajLekcje", lesson);
            }
            try
            {
                if (lesson.id == 0)
                {
                    var id = PublishLesson(lesson);
                    if (lesson.Emails != null)
                    {
                        var Emails = lesson.Emails.Split(new char[] { ',', ';' });
                        var EmailsError = "";
                        foreach (var Email in Emails)
                        {
                            try
                            {
                                CreateLinks(new MailAddress(Email), id, lesson.LifeTime, lesson.SendEmail, lesson.EmailText, lesson.LessonName, lesson.LifeTime);
                            }
                            catch (Exception ex)
                            {
                                LogHelper.Error(typeof(LessonController), "Błąd tworzenia linków", ex);
                                EmailsError += Email + ",";
                            }
                        }
                        if (!string.IsNullOrEmpty(EmailsError))
                            lesson.Error = "Lekcja została zapisana jednak na podane E-mail nie udało się wysłać wiadomości" + EmailsError;
                        System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
                        stringBuilder.AppendLine($"Nauczyciel {Membership.GetUser().UserName} utworzył lekcje o nazwię {Umbraco.TypedContent(id).Name}.");
                        if (!string.IsNullOrEmpty(lesson.Emails))
                        {
                            stringBuilder.AppendLine($"Zaprosił uczniów o adresach {lesson.Emails}");
                            if (lesson.SendEmail)
                            {
                                stringBuilder.AppendLine($"oraz wysłał im wiadomość E-mail");
                            }
                            stringBuilder.Append($".");
                        }
                        EmailService.Notification("Powstała nowa lekcja", stringBuilder.ToString());
                    }
                }
                else
                {
                    UpdateLesson(lesson);
                    lesson.SaveChecked = true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(LessonController), "Can't Publish content", ex);
                lesson.Error = "Wystąpił błąd spróbuj ponownie później";
                return PartialView("DodajLekcje", lesson);
            }
            lesson.Ok = true;
            return PartialView("DodajLekcje", lesson);
        }
        [HttpPost]
        public ActionResult DodajUczniow(EdytujUczniow uczniow)
        {
            if (ItMyLesson(uczniow.id))
            {
                var main = Umbraco.TypedContent(uczniow.id);
                if (main != null)
                {
                    var Emails = uczniow.NowiUczniowie;
                    if (!string.IsNullOrEmpty(Emails))
                    {
                        var EmailsError = "";
                        foreach (var email in Emails.Split(new char[] { ',', ';' }))
                        {
                            try
                            {
                                var Mail = new MailAddress(email);
                                var item = main.Children.FirstOrDefault(x => x.Name == email);
                                if (item != null)
                                {
                                    var temp = Services.ContentService.GetById(item.Id);
                                    SendEmails(temp, Mail, item.Parent.Name, uczniow.LeftTime, uczniow.EmailText);
                                }
                                else
                                {
                                    CreateLinks(Mail, uczniow.id, uczniow.LeftTime, true, uczniow.EmailText, main.Name, uczniow.LeftTime);
                                }
                                uczniow.Ok = true;
                            }
                            catch (Exception ex)
                            {

                                LogHelper.Error(typeof(LessonController), "Błąd tworzenia linków", ex);
                                EmailsError += email + ",";
                                uczniow.Error = "Na podane adresy e-mail nie udało się wysłać wiadomości: " + EmailsError;
                            }
                        }
                        EmailService.Notification("Zaproszono nowych uczniów do lekcji", $"Nauczyciel {Membership.GetUser().UserName} do lekcji o nazwie {main.Name}, zaprosił uczniów o adresach {uczniow.NowiUczniowie} oraz wysłał im wiadomość E-mail.");
                    }
                    else
                    {
                        uczniow.Error = "Nie podano adresów e-mail";
                    }
                }
                else
                {
                    uczniow.Error = "Lekcja nie istnie";
                }
                uczniow.UczniowieIstniejący = Umbraco.TypedContent(uczniow.id).Children().ToList();
                return PartialView("EdytujUczniow", uczniow);
            }
            return ErrorPage(uczniow.id);
        }
        public ActionResult SendAgin(int id)
        {
            var main = Umbraco.TypedContent(id);
            if (main != null)
            {
                if (ItMyLesson(id))
                {
                    if (main.DocumentTypeAlias == "Uczniowie")
                    {
                        var item = Services.ContentService.GetById(id);
                        SendEmails(item, new MailAddress(item.Name), main.Parent.Name, item.GetValue<DateTime>("dataWaznosc"), "");
                        ViewBag.Ok = "Wysłano Pomyślnie";
                        EmailService.Notification("Wysłano zaprszenia dla ucznia do lekcji", $"Nauczyciel {Membership.GetUser().UserName} wysłał zaproszenie do lekcji o nazwie {main.Name}, dla ucznia o adresie {item.Name}.");
                        return PreparePupilEdit(item.ParentId, true);
                    }
                    else if (main.DocumentTypeAlias == "Lekcja")
                    {
                        var email = "";
                        foreach (var child in main.Children)
                        {
                            if (!string.IsNullOrEmpty(email))
                            {
                                email += "' ";
                            }
                            var item = Services.ContentService.GetById(child.Id);
                            email += item.Name;
                            SendEmails(item, new MailAddress(item.Name), main.Name, item.GetValue<DateTime>("dataWaznosc"), "");
                        }
                        ViewBag.Ok = "Wysłano Pomyślnie";
                        EmailService.Notification("Wysłano zaprszenia dla ucznia do lekcji", $"Nauczyciel {Membership.GetUser().UserName} wysłał zaproszenie do lekcji o nazwie {main.Name}, dla uczniów o adresach {email}.");
                        return PreparePupilEdit(id, true);
                    }
                }
                else
                {
                    return ErrorPage(id);
                }
            }
            return PreparePupilEdit(id, true, "Nie znaleziono");
        }
        public ActionResult Delete(int id)
        {
            var main = Umbraco.TypedContent(id);
            if (main != null)
            {
                if (ItMyLesson(id))
                {
                    if (main.DocumentTypeAlias == "Uczniowie")
                    {
                        var item = Services.ContentService.GetById(id);
                        int parentId = 0;
                        if (item != null)
                        {
                            parentId = item.ParentId;
                            Services.ContentService.Delete(item, 0);
                            ViewBag.Ok = "Usunięnto Pomyślnie";
                            return PreparePupilEdit(parentId, true);
                        }
                        else
                        {
                            return PreparePupilEdit(parentId, false, "Nie istnieje taki Uczeń");
                        }
                    }
                    else if (main.DocumentTypeAlias == "Lekcja")
                    {
                        foreach (var child in main.Children.ToList())
                        {
                            var item = Services.ContentService.GetById(child.Id);
                            Services.ContentService.Delete(item, 0);
                        }
                        ViewBag.Ok = "Usunięnto Pomyślnie";
                        return PreparePupilEdit(id, true);
                    }
                }
            }
            return ErrorPage(id);
        }
        public ActionResult UpdateDeadLine(int id, DateTime LifeTime)
        {
            ViewBag.Show = true;
            var main = Umbraco.TypedContent(id);
            if (main != null)
            {
                if (ItMyLesson(id))
                {
                    if (main.DocumentTypeAlias == "Uczniowie")
                    {
                        UpdateLink(id, LifeTime);
                        ViewBag.Ok = "Zapisano Pomyślnie";
                        return PreparePupilEdit(main.Parent.Id, true, null);
                    }
                    else if (main.DocumentTypeAlias == "Lekcja")
                    {
                        foreach (var item in main.Children.ToList())
                        {
                            ViewBag.Ok = "Zapisano Pomyślnie";
                            UpdateLink(item.Id, LifeTime);
                        }
                        return PreparePupilEdit(id, true, null);
                    }
                    else
                    {
                        return PreparePupilEdit(0, false, "Nie znany obiekt");
                    }
                }
                else
                {
                    return ErrorPage(id);
                }
            }
            return PreparePupilEdit(0, false, "Nie znaleziono");
        }
        public ActionResult DeleteLesson(int id)
        {
            var item = new EdytujUczniow()
            {
                id = id,
                LeftTime = DateTime.Now.AddDays(14),
            };
            var main = Umbraco.TypedContent(id);
            item.UczniowieIstniejący = main.Children().ToList();
            if (main != null)
            {
                if (ItMyLesson(id))
                {
                    if (main.DocumentTypeAlias == "Lekcja")
                    {
                        ViewBag.Ok = "Zapisano Pomyślnie";
                        Services.ContentService.Delete(Services.ContentService.GetById(main.Id), 0);
                        return Content("Ok");
                    }
                }
                else
                {
                    item.Error = "To nie twoja lekcja";
                }
            }
            return PartialView("UsuwanieLekcji", item);

        }
        private int PublishLesson(AddLesson lesson)
        {
            var newContent = Services.ContentService.CreateContent(name: lesson.LessonName, parentId: lesson.parentId, contentTypeAlias: "Lekcja", userId: -1);
            return saveContent(newContent, lesson);
        }
        private void UpdateLesson(AddLesson lesson)
        {
            var newContent = Services.ContentService.GetById(lesson.id);
            saveContent(newContent, lesson);
        }
        private int saveContent(Umbraco.Core.Models.IContent content, AddLesson lesson)
        {
            content.SetValue("nazwaLekcji", lesson.LessonName);
            content.SetValue("Artykuly", lesson.Articles);
            LogHelper.Info(typeof(IngesterSurfaceController), $"Save new content");
            Services.ContentService.Save(content);
            LogHelper.Info(typeof(IngesterSurfaceController), $"Publish new content");
            Services.ContentService.Publish(content);
            return content.Id;
        }
        private void CreateLinks(MailAddress mailAddress, int id, DateTime dateTime, bool SendEmail, string TeacherText, string name, DateTime date)
        {
            var newContent = Services.ContentService.CreateContent(name: mailAddress.Address, parentId: id, contentTypeAlias: "Uczniowie", userId: -1);
            newContent.SetValue("email", mailAddress.Address);
            //newContent.SetValue("status", "nie odwiedzony");
            newContent.SetValue("dataWaznosc", new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59, 99));
            LogHelper.Info(typeof(IngesterSurfaceController), $"Save new content");
            Services.ContentService.Save(newContent);
            LogHelper.Info(typeof(IngesterSurfaceController), $"Publish new content");
            Services.ContentService.Publish(newContent);
            if (SendEmail)
            {
                SendEmails(newContent, mailAddress, name, date, TeacherText);
            }
        }
        private void SendEmails(Umbraco.Core.Models.IContent content, MailAddress mailAddress, string name, DateTime date, string text)
        {
            var member = Members.GetCurrentMember();
            var memberservices = Services.MemberService;
            var url = Umbraco.TypedContent(content.Id).UrlAbsolute();
            url = url.Remove(url.Length - 1) + "?code=" + content.Key;
            var files = TakeAttachmetns();
            EmailService.ShareLesson(mailAddress, text, files, new MailAddress(memberservices.GetById(member.Id).Email), url, name, new DateTime(date.Year, date.Month, date.Day, 23, 59, 59));
        }
        private List<HttpPostedFileBase> TakeAttachmetns()
        {
            List<HttpPostedFileBase> files = new List<HttpPostedFileBase>();
            if (Request.Files != null && Request.Files.Count > 0)
            {

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    files.Add(Request.Files[i]);
                }
            }
            return files;
        }
        private ActionResult PreparePupilEdit(int lessonId, bool ok = true, string Error = null)
        {
            var Lesson = Umbraco.TypedContent(lessonId);
            var item = new EdytujUczniow()
            {
                id = lessonId,
                LeftTime = DateTime.Now.AddDays(14),
                UczniowieIstniejący = Lesson.Children().ToList(),
                Error = Error,
                Ok = ok
            };
            return PartialView("EdytujUczniow", item);
        }
        private bool ItMyLesson(int UserPageId)
        {
            var page = Umbraco.TypedContent(UserPageId);
            var user = Membership.GetUser();

            if (page != null && user != null)
            {
                var member = ApplicationContext.Services.MemberService.GetByUsername(user.UserName);
                page = page.AncestorOrSelf("Users");
                if (page != null && member != null && int.Parse(page.Name) == member.Id)
                {
                    return true;
                }
            }
            return false;
        }
        private ActionResult ErrorPage(int id)
        {
            return PreparePupilEdit(id, false, "To nie twoja lekcja");
        }
        private void UpdateLink(int id, DateTime dateTime)
        {
            var content = Services.ContentService.GetById(id);
            content.SetValue("dataWaznosc", new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59, 99));
            LogHelper.Info(typeof(IngesterSurfaceController), $"Save new content");
            Services.ContentService.Save(content);
            LogHelper.Info(typeof(IngesterSurfaceController), $"Publish new content");
            Services.ContentService.Publish(content);
        }
    }
}
