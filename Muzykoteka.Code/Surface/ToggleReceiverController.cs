﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;
using System.Net;
using Umbraco.Core.Logging;
using System.Web.Mvc;
using System.Web.Security;
using System.Globalization;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;

namespace Muzykoteka.Code
{
    public class ToggleReceiverController : SurfaceController
    {
        [HttpGet]
        public HttpStatusCodeResult SetIsTeacher(int isTeacher)
        {
            Session["receiver"] = isTeacher;
            if (Response.Cookies["receiver"] == null)
            {
                var c = new System.Web.HttpCookie("receiver", isTeacher.ToString());
                c.Expires = DateTime.Now.AddYears(20);
                Response.Cookies.Add(c);
            }
            else
            {
                Response.Cookies["receiver"].Value = isTeacher.ToString();
                Response.Cookies["receiver"].Expires = DateTime.Now.AddYears(20);
            }            
            return new HttpStatusCodeResult(HttpStatusCode.OK);     
        }        
    }
}
