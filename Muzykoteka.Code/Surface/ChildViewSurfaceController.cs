﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;
using System.Net;
using Umbraco.Core.Logging;
using System.Web.Mvc;
using System.Web.Security;
using System.Globalization;
using Umbraco.Core.Models;


namespace Muzykoteka.Code
{
    public class ChildViewSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {

        class ContentNVisits
        {
            public IPublishedContent c { get; set; }
            public int visits { get; set; }
        }



        [HttpGet]
        public ActionResult GetPage(int p, int parentNodeId, string filter, string sort, int t, string e, string k, string q = "", bool All = false)
        {
            bool isShared = false;
            int pageNumber = p;
            int pagesToReload = 3;
            string app_s = System.Configuration.ConfigurationManager.AppSettings["list_pages_to_reload"];
            if (app_s != null && Int32.TryParse(app_s, out pagesToReload))
            {
                if (pagesToReload < 1)
                    pagesToReload = 1;
            }

            int itemsPerPage = 30;
            app_s = System.Configuration.ConfigurationManager.AppSettings["list_items_per_page"];
            if (app_s != null && Int32.TryParse(app_s, out itemsPerPage))
            {
                if (itemsPerPage < 12)
                    itemsPerPage = 12;
            }

            if (t == 3)
            {
                return GetSearchPage(p, itemsPerPage, pagesToReload, filter, e, k);
            }

            IPublishedContent parentNode = null;
            if (All)
            {
                parentNode = Umbraco.TypedContent(parentNodeId).Parent;
            }
            else
            {
                parentNode = Umbraco.TypedContent(parentNodeId);
            }

            bool isClipboard = (parentNode.DocumentTypeAlias == "Schowek");
            t = isClipboard ? 6 : t;
            Dictionary<int, int> sort_order_by_id = null;

            IEnumerable<IPublishedContent> all_children;
            bool DrmOff = false;
            switch (t)
            {
                case 1:
                    {
                        if (filter == "polecany==1")
                        {
                            all_children = parentNode.AncestorOrSelf(1).Children(x => x.DocumentTypeAlias == "ArtykulyGlowna").FirstOrDefault().Children;
                        }
                        else
                        {
                            all_children = parentNode.AncestorOrSelf(1).Descendants().Where(x => x.HasProperty("tresc") && x.ContentType.Alias != "PozostaleArtykul");
                        }
                    }
                    break;
                case 2:
                    {
                        string sids = parentNode.GetPropertyValue<string>("artykuly");
                        if (!String.IsNullOrEmpty(sids))
                        {
                            int[] ids = sids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select<string, int>(x => Convert.ToInt32(x)).ToArray();
                            sort_order_by_id = new Dictionary<int, int>();
                            int idx = 0;
                            foreach (int id in ids)
                            {
                                sort_order_by_id.Add(id, idx++);
                            }
                            all_children = parentNode.Descendants().Where(x => ids.Contains(x.Id));
                        }
                        else
                        {
                            all_children = parentNode.Descendants().Where(x => x.Id == 0);
                        }
                    }
                    break;
                case 4:
                    {
                        List<IPublishedContent> tmp = new List<IPublishedContent>();
                        foreach (IPublishedContent content in parentNode.Children)
                        {
                            tmp.AddRange(content.Children.Where("Visible"));
                        }
                        all_children = tmp.OrderByDescending(x => x.UpdateDate);
                    }
                    break;
                case 5:
                    {
                        List<IPublishedContent> tmp = new List<IPublishedContent>();
                        tmp.Add(parentNode.FirstChild(x => x.DocumentTypeAlias == "Okladka"));
                        tmp.AddRange(parentNode.Children(x => x.DocumentTypeAlias == "Film"));
                        all_children = tmp;
                    }
                    break;
                case 6://isClipboard
                    {
                        List<int> contentIDs = new List<int>();
                        if (parentNode.Name == "Moje ulubione")
                        {
                            contentIDs = ClipboardSurfaceController.GetCurrentClpbList() ?? new List<int>();
                        }
                        else if (parentNode.Name == "Udostępniony schowek")
                        {
                            contentIDs = q.Split('a').Select(x => int.Parse(x)).ToList();
                            isShared = true;
                        }
                        all_children = parentNode.AncestorOrSelf(1)
                                                .Descendants()
                                                .Where(x => contentIDs.Contains(x.Id));
                    }
                    break;
                case 7:
                    {
                        var temp = new List<IPublishedContent>();
                        var articles = parentNode.Parent.GetPropertyValue<string>("Artykuly");
                        if (!string.IsNullOrEmpty(articles))
                        {
                            List<int> tmp = articles.Split(',').Select(x => int.Parse(x)).Distinct().ToList();
                            foreach (var item in tmp)
                            {
                                temp.Add(Umbraco.TypedContent(item));
                            }
                        }
                        all_children = temp;
                        DrmOff = true;
                    }
                    break;
                case 8:
                    {
                        var articles = parentNode.GetPropertyValue<string>("Artykuly");
                        if (!string.IsNullOrEmpty(articles))
                        {
                            List<int> tmp = articles.Split(',').Select(x => int.Parse(x)).ToList();
                            all_children = Umbraco.UmbracoContext.ContentCache.GetAtRoot().FirstOrDefault().Descendants().Where(x => tmp.Contains(x.Id));
                        }
                        else
                        {
                            all_children = new List<IPublishedContent>();
                        }

                    }
                    break;
                default:
                    if (All)
                    {
                        var temp = new List<IPublishedContent>();
                        foreach (var item in parentNode.Children)
                            temp.AddRange(item.Children);
                        all_children = temp;
                    }
                    else
                    {
                        all_children = parentNode.Children;
                    }
                    break;
            }

            //drewniane
            if (!String.IsNullOrEmpty(filter))
            {
                string[] a_filter = filter.Split('|');
                foreach (string f in a_filter)
                {
                    if (f.Contains("=="))
                    {
                        string[] kv = f.Split(new char[] { '=', '>', '<' }, StringSplitOptions.RemoveEmptyEntries);
                        int v = Convert.ToInt32(kv[1]);
                        all_children = all_children.Where(x => x.GetPropertyValue<int?>(kv[0]) == v);
                    }
                    else if (f.Contains('='))
                    {
                        string[] kv = f.Split(new char[] { '=', '>', '<' }, StringSplitOptions.RemoveEmptyEntries);
                        if (kv[0] == "dt") //data publikacji
                        {
                            Int64 msecs;
                            if (Int64.TryParse(kv[1], out msecs))
                            {
                                DateTime dt = new DateTime(1970, 1, 1);
                                dt = dt.AddMilliseconds(msecs);
                                if (f.Contains(">="))
                                {
                                    all_children = all_children.Where(x => (x.HasValue("dataPublikacji") ? x.GetPropertyValue<DateTime?>("dataPublikacji") : x.CreateDate) >= dt);
                                }
                                else if (f.Contains("<="))
                                {
                                    all_children = all_children.Where(x => (x.HasValue("dataPublikacji") ? x.GetPropertyValue<DateTime?>("dataPublikacji") : x.CreateDate) <= dt);
                                }
                                else if (f.Contains(">"))
                                {
                                    all_children = all_children.Where(x => (x.HasValue("dataPublikacji") ? x.GetPropertyValue<DateTime?>("dataPublikacji") : x.CreateDate) > dt);
                                }
                                else if (f.Contains("<"))
                                {
                                    all_children = all_children.Where(x => (x.HasValue("dataPublikacji") ? x.GetPropertyValue<DateTime?>("dataPublikacji") : x.CreateDate) < dt);
                                }
                                else if (f.Contains("="))
                                {
                                    all_children = all_children.Where(x => (x.HasValue("dataPublikacji") ? x.GetPropertyValue<DateTime?>("dataPublikacji") : x.CreateDate) == dt);
                                }
                            }
                        }
                        else if (kv[0] == "kategoriaId")
                        {
                            int[] vals = kv[1].Split(',').Select(int.Parse).ToArray();
                            all_children = all_children.Where(x => vals.Contains(x.Parent.Id));
                        }
                        else
                        {
                            all_children = all_children.Where(x => x.HasValue(kv[0]) && x.GetPropertyValue<string>(kv[0]).Contains(kv[1]));
                        }
                    }
                    else if (f.Contains("in"))
                    {
                        string[] kv = f.Split(new string[] { " in " }, StringSplitOptions.RemoveEmptyEntries);
                        string[] vals = kv[1].Trim(new char[] { ')', '(' }).Split(',');
                        if (kv[0] == "kategoriaId")
                        {
                            int[] ivals = vals.Select(int.Parse).ToArray();
                            all_children = all_children.Where(x => ivals.Contains(x.Parent.Id));
                        }
                        else
                        {
                            foreach (string v in vals)
                            {
                                all_children = all_children?.Where(x => x.GetPropertyValue<string>(kv[0])?.Split(',')?.Where(y => vals.Contains(y))?.Any() == true);
                            }
                        }
                    }
                    else if (f.Contains("lt"))
                    {
                        string[] kv = f.Split(new string[] { " lt " }, StringSplitOptions.RemoveEmptyEntries);
                        string[] vals = kv[1].Trim(new char[] { ')', '(' }).Split(',');
                        foreach (string v in vals)
                        {
                            all_children = all_children.Where(x => x.HasValue(kv[0]) && vals.Contains(x.GetPropertyValue<string>(kv[0]).Substring(0, 1).ToUpper()));
                        }
                    }
                }
            }

            if (!isClipboard)
            {
                if (parentNode.DocumentTypeAlias == "Nauka" || parentNode.Parent.DocumentTypeAlias == "Nauka")
                {
                    int currentReceiver = Helper.GetCurrentReceiver();
                    //if (currentReceiver != 0)
                    //{
                    //    var naukaNode = (parentNode.DocumentTypeAlias == "Nauka" ? parentNode : parentNode.Parent);
                    //    if ((currentReceiver == 1 && !naukaNode.GetPropertyValue<bool>("dlaNauczyciela"))
                    //        || (currentReceiver == 2 && !naukaNode.GetPropertyValue<bool>("dlaUcznia"))
                    //        || (currentReceiver == 3 && !naukaNode.GetPropertyValue<bool>("dlaStudenta")))
                    //    {
                    //        currentReceiver = 0;
                    //    }
                    //}
                    switch (currentReceiver)
                    {
                        case 0:
                            break;
                        case 1:
                            all_children = all_children.Where(x => x.GetPropertyValue<bool?>("dlaNauczyciela") == true);
                            break;
                        case 2:
                            all_children = all_children.Where(x => x.GetPropertyValue<bool?>("dlaUcznia") == true);
                            break;
                        case 3:
                            all_children = all_children.Where(x => x.GetPropertyValue<bool?>("dlaStudenta") == true);
                            break;
                    }
                }
            }

            if (all_children.Count() <= 0)
            {
                return PartialView("EmptyList");
            }
            else
            {
                if (parentNode.DocumentTypeAlias == "Kolekcja")
                {
                    all_children = all_children.OrderBy(x => x.SortOrder);
                }
                else if (parentNode.DocumentTypeAlias == "Uczniowie" || parentNode.DocumentTypeAlias == "Lekcja")
                {

                }
                else if (sort_order_by_id != null)
                {
                    all_children = all_children.OrderBy(o => sort_order_by_id[o.Id]);
                }
                else if (!String.IsNullOrEmpty(sort) && sort.Length >= 2)
                {
                    string sort_field = sort.Substring(1);
                    if (sort[0] == 'a')
                    {
                        switch (sort_field)
                        {
                            case "d":
                                all_children = all_children.OrderBy(o => (o.HasValue("dataPublikacji") ? o.GetPropertyValue("dataPublikacji") : o.CreateDate));
                                break;
                            case "s":
                                all_children = all_children.OrderBy(o => o.SortOrder);
                                break;
                            case "v":
                                var node_ids = all_children.Select<IPublishedContent, string>(x => x.Id.ToString()).ToArray();
                                string ids = String.Join(",", node_ids);
                                var visits = DatabaseContext.Database.Query<VisitCounter>("select n.Id as node_id, ISNULL(c.views,0) as views from umbracoNode n left join custom_hit_counter c on n.Id=c.node_id where n.id in (" + ids + ")").ToList();
                                all_children = all_children
                                    .Join<IPublishedContent, VisitCounter, int, ContentNVisits>(visits, x => x.Id, y => y.node_id, (x, y) => new ContentNVisits()
                                    {
                                        c = x,
                                        visits = y.views
                                    })
                                    .OrderBy(x => x.visits)
                                    .Select<ContentNVisits, IPublishedContent>(x => x.c);
                                break;
                            default:
                                all_children = all_children.OrderBy(o => o.GetPropertyValue(sort_field));
                                break;
                        }
                    }
                    else
                    {
                        switch (sort_field)
                        {
                            case "d":
                                all_children = all_children.OrderByDescending(o => (o.HasValue("dataPublikacji") ? o.GetPropertyValue("dataPublikacji") : o.CreateDate));
                                break;
                            case "s":
                                all_children = all_children.OrderByDescending(o => o.SortOrder);
                                break;
                            case "v":
                                var node_ids = all_children.Select<IPublishedContent, string>(x => x.Id.ToString()).ToArray();
                                string ids = String.Join(",", node_ids);
                                var visits = DatabaseContext.Database.Query<VisitCounter>("select n.Id as node_id, ISNULL(c.views,0) as views from umbracoNode n left join custom_hit_counter c on n.Id=c.node_id where n.id in (" + ids + ")").ToList();
                                all_children = all_children
                                    .Join<IPublishedContent, VisitCounter, int, ContentNVisits>(visits, x => x.Id, y => y.node_id, (x, y) => new ContentNVisits()
                                    {
                                        c = x,
                                        visits = y.views
                                    })
                                    .OrderByDescending(x => x.visits)
                                    .Select<ContentNVisits, IPublishedContent>(x => x.c);
                                break;
                            default:
                                all_children = all_children.OrderByDescending(o => o.GetPropertyValue(sort_field));
                                break;
                        }
                    }
                }
                if (isShared)
                    isClipboard = false;

                return PrepareResults(
                        all_children.Skip(itemsPerPage * (pageNumber - 1)).Take(itemsPerPage),
                        all_children.Count(),
                        p,
                        itemsPerPage,
                        pagesToReload,
                        isClipboard, DrmOff);
            }
        }


        private ActionResult GetSearchPage(int p, int itemsPerPage, int pagesToReload, string searchTerm, string epoka, string kategoria)
        {
            int pageNumber = p;

            List<int> searchResults;
            int recordCount;
            if (System.Configuration.ConfigurationManager.AppSettings["es_disabled"] != "1")
            {
                var esearch = Muzykoteka.Code.Services.ElasticSearchService.GetInstance();
                string[] a_epoka = (String.IsNullOrWhiteSpace(epoka) ? null : epoka.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                string[] a_kategoria = (String.IsNullOrWhiteSpace(kategoria) ? null : kategoria.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                searchResults = esearch.Search((p - 1) * itemsPerPage, itemsPerPage, searchTerm, a_epoka, a_kategoria, out recordCount);
            }
            else
            {
                searchResults = new List<int>();
                recordCount = 0;
            }


            Dictionary<int, int> sort_order_by_id = new Dictionary<int, int>();
            int idx = 0;
            foreach (int id in searchResults)
            {
                if (!sort_order_by_id.ContainsKey(id))
                {
                    sort_order_by_id.Add(id, idx++);
                }
            }

            IEnumerable<IPublishedContent> records = Umbraco.UmbracoContext.ContentCache
                .GetAtRoot()
                .FirstOrDefault()
                .Descendants()
                .Where(x => searchResults.Contains(x.Id))
                .OrderBy(o => sort_order_by_id[o.Id]);

            if (records.Count() <= 0)
            {
                return PartialView("EmptyList");
            }
            else
            {
                return PrepareResults(records, recordCount, p, itemsPerPage, pagesToReload, false, false);
            }
        }


        private ActionResult PrepareResults(IEnumerable<IPublishedContent> records, int recordCount, int pageNum, int itemsPerPage, int pagesToReload, bool isClipboard, bool drmoff)
        {
            ViewBag.nodes = records;
            ViewBag.TotalItems = recordCount;
            ViewBag.TotalPages = (int)Math.Ceiling((double)recordCount / (double)itemsPerPage);
            ViewBag.PageNum = pageNum;
            ViewBag.IsClipboard = isClipboard;
            ViewBag.AddNextPageLink = (pageNum > 0 && pageNum < ViewBag.TotalPages && pageNum % pagesToReload == 0);
            ViewBag.AddPrevPageLink = (pageNum > pagesToReload && (pageNum == ViewBag.TotalPages || ViewBag.AddNextPageLink));
            ViewBag.AddLoader = (pageNum < ViewBag.TotalPages && !ViewBag.AddNextPageLink);
            ViewBag.TotalBooks = (int)Math.Ceiling((double)ViewBag.TotalPages / (double)pagesToReload);
            ViewBag.CurrentBook = (int)Math.Ceiling((double)pageNum / (double)pagesToReload);
            ViewBag.DrmOff = drmoff;
            return PartialView("ChildViewRec");
        }
    }
}
