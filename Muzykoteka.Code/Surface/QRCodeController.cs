﻿using System;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace UmbracoIdentityServer.Client.Controllers
{
    public class QRCodeController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var id = Request.Url.AbsoluteUri.Substring(Request.Url.AbsoluteUri.IndexOf('?') + 1);
            if (!string.IsNullOrEmpty(id))
            {
                var guid = new Guid(id);
                var testy = ApplicationContext.Current.Services.EntityService.GetIdForKey(guid, UmbracoObjectTypes.Document);
                var content = Umbraco.TypedContent(testy.Result);
                return Redirect(content.Url);
            }
            return base.Index(model);
        }
    }
}