﻿using Muzykoteka.Code.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;

using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;

namespace Muzykoteka.Code.Surface
{
    public class MAMSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        const int newAssetsFolderID = 37151;
        static readonly string[] vodTypeAliases = new[] { "StreamVOD", "StreamAOD" };
        const string rootAlias = "KatalogVOD";
        const string VowosToken = "8f17066f-3a23-41d2-b562-ef9381f0899b";

        [System.Web.Http.HttpPost]
        public ActionResult GetAssets(int mode = 1)
        {
            var token = CreateToken(GetRequestData());
            if (token != null)
            {
                token.Mode = mode;
                SetResponseData(GetAllAssets(token));
            }
            return null;
        }
        [System.Web.Http.HttpPost]
        public ActionResult GetAsset(int id, int mode = 1)
        {
            var token = CreateToken(GetRequestData());
            if (token != null)
            {
                token.Mode = mode;
                var asset = Services.ContentService.GetById(id);
                if (asset != null)
                {
                    SetResponseData(CreateMAMAssetItem(asset, token));
                }
                else
                    Response.StatusCode = 400;
            }
            return null;
        }
        [System.Web.Http.HttpPost]
        public ActionResult AddAsset()
        {
            var token = CreateToken(GetRequestData());
            if (token != null)
            {
                CreateAsset(token);
                SetResponseData(token.Asset);
            }

            return null;
        }
        [System.Web.Http.HttpPost]
        public ActionResult EditAsset(int mode = 0)
        {
            var token = CreateToken(GetRequestData());
            if (token != null)
            {
                token.Mode = mode;
                EditAsset(token);
                SetResponseData(token.Asset);
            }
            return null;
        }
        [System.Web.Http.HttpGet]
        public ActionResult GetAssetTypes()
        {
            var token = CreateToken(GetRequestData());
            if (token != null)
            {
                SetResponseData(GetAssetTypes(token));
            }
            return null;
        }

        IEnumerable<MAMAssetItemType> GetAssetTypes(DataToken token)
        {
            foreach (var item in vodTypeAliases)
            {
                yield return new MAMAssetItemType()
                {
                    ID = item,
                    Name = item
                };
            }
        }
        IEnumerable<MAMAssetItem> GetAllAssets(DataToken token)
        {
            List<MAMAssetItem> list = new List<MAMAssetItem>(200);
            var root = UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault(x => x.DocumentTypeAlias == rootAlias);
            GetAllAssets(root, list, token);
            return list;
        }
        void GetAllAssets(IPublishedContent root, List<MAMAssetItem> list, DataToken token)
        {
            if (root == null)
                return;
            foreach (var item in root.Children)
            {
                if (IsVODType(item.DocumentTypeAlias))
                    list.Add(CreateMAMAssetItem(item, token));
                else
                    GetAllAssets(item, list, token);
            }
        }
        static bool IsVODType(string typeAlias)
        {
            for (int i = 0; i < vodTypeAliases.Length; i++)
            {
                if (vodTypeAliases[i].Equals(typeAlias))
                    return true;
            }
            return false;
        }
        private void CreateAsset(DataToken token)
        {
            MAMAssetItem item = token.Asset;
            var asset = Services.ContentService.CreateContent(name: item.Name, parentId: newAssetsFolderID, contentTypeAlias: item.AssetType.ID, userId: -1);
            SaveAssetData(asset, token);
        }
        private void EditAsset(DataToken token)
        {
            var asset = Services.ContentService.GetById(token.Asset.ID);
            if (asset != null)
            {
                SaveAssetData(asset, token);
            }
            else
                Response.StatusCode = 400;
        }
        private void SaveAssetData(IContent asset, DataToken token)
        {
            MAMAssetItem mamItem = token.Asset;
            asset.Name = mamItem.Name;
            asset.SetValue("inputPath", mamItem.Source ?? "");
            if (token.URLAllowed)
            {
                asset.SetValue("streamURL_DASH", mamItem.DASH_URL ?? "");
                asset.SetValue("streamURL_HLS", mamItem.HLS_URL ?? "");

                if (token.DRMAllowed)
                {
                    asset.SetValue("kluczDASH", mamItem.DASH_Key ?? "");
                    asset.SetValue("kluczHLS", mamItem.HLS_Key ?? "");
                    asset.SetValue("ivectorHLS", mamItem.HLS_IV ?? "");
                }
            }
            Services.ContentService.Save(asset);
            Services.ContentService.Publish(asset);
            mamItem.ID = asset.Id;
        }

        static MAMAssetItem CreateMAMAssetItem(IContent content, DataToken token)
        {
            var x = new MAMAssetItem()
            {
                ID = content.Id,
                Name = content.Name,
                AssetType = new MAMAssetItemType()
                {
                    ID = content.ContentType.Alias,
                    Name = content.ContentType.Name,
                }
            };
            x.Source = content.GetValue("inputPath")?.ToString();
            if (token.URLAllowed)
            {
                x.DASH_URL = content.GetValue("streamURL_DASH")?.ToString() ?? "";
                x.HLS_URL = content.GetValue("streamURL_HLS")?.ToString() ?? "";
                if (token.DRMAllowed)
                {
                    x.DASH_Key = content.GetValue("kluczDASH")?.ToString() ?? "";
                    x.HLS_Key = content.GetValue("kluczHLS")?.ToString() ?? "";
                    x.HLS_IV = content.GetValue("ivectorHLS")?.ToString() ?? "";
                }
            }
            return x;
        }
        static MAMAssetItem CreateMAMAssetItem(IPublishedContent content, DataToken token)
        {
            var x = new MAMAssetItem()
            {
                ID = content.Id,
                Name = content.Name,
                AssetType = new MAMAssetItemType()
                {
                    ID = content.ContentType.Alias,
                    Name = content.ContentType.Alias,
                },
            };
            x.Source = content.GetPropertyValue("inputPath")?.ToString();
            if (token.URLAllowed)
            {
                x.DASH_URL = content.GetPropertyValue("streamURL_DASH")?.ToString() ?? "";
                x.HLS_URL = content.GetPropertyValue("streamURL_HLS")?.ToString() ?? "";
                if (token.DRMAllowed)
                {
                    x.DASH_Key = content.GetPropertyValue("kluczDASH")?.ToString() ?? "";
                    x.HLS_Key = content.GetPropertyValue("kluczHLS")?.ToString() ?? "";
                    x.HLS_IV = content.GetPropertyValue("ivectorHLS")?.ToString() ?? "";
                }
            }
            return x;
        }


        MAMRequestData GetRequestData()
        {
            Stream req = Request.InputStream;
            req.Seek(0, SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            try
            {
                Response.StatusCode = 200;
                return JsonConvert.DeserializeObject<MAMRequestData>(json);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                LogHelper.Error(typeof(MAMSurfaceController), "Invalid RequestData", ex);
                return null;
            }
        }
        void SetResponseData(object data)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            Response.Write(JsonConvert.SerializeObject(data, Formatting.None, settings));
        }
        #region DataToken
        DataToken CreateToken(MAMRequestData requestData)
        {
            if (requestData == null)
                return null;
            string strToken = requestData.Token;
            if (!TokenValid(strToken))
            {
                Response.StatusCode = 403;
                return null;
            }
            return new DataToken
            {
                DRMAllowed = strToken == VowosToken,
                Asset = requestData.Asset
            };
        }

        bool TokenValid(string strToken)
        {
            return strToken == VowosToken;
        }
        class DataToken
        {
            bool dRMAllowed;
            public bool DRMAllowed
            {
                get => dRMAllowed && URLAllowed;
                set => dRMAllowed = value;
            }
            public bool URLAllowed { get { return Mode > 0; } }
            public int Mode { get; set; } = 1;
            public MAMAssetItem Asset { get; set; }
        }
        #endregion

    }
}
