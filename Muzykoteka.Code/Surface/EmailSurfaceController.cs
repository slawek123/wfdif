﻿using DevExpress.XtraRichEdit.Import.Html;
using Muzykoteka.Code.Models;
using Muzykoteka.Code.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using umbraco.presentation.plugins.tinymce3;
using Umbraco.Core.Logging;
using Umbraco.Web.Mvc;

namespace Muzykoteka.Code.Surface
{
    public class EmailSurfaceController : SurfaceController
    {
        [HttpPost]
        public ActionResult SharePage(EmailInfo emailInfo)
        {
            List<string> ErrorEmail = new List<string>();
            List<string> NotSend = new List<string>();
            try
            {
                if (string.IsNullOrEmpty(emailInfo.Name))
                {
                    emailInfo.Error = "Podpisz się";
                    emailInfo.Ok = false;
                    return PartialView("EmailShare", emailInfo);
                }
                if (string.IsNullOrEmpty(emailInfo.sender))
                {
                    emailInfo.Error = "Podaj swój E-mail";
                    emailInfo.Ok = false;
                    return PartialView("EmailShare", emailInfo);
                }
                MailAddress address = new MailAddress(emailInfo.sender);
                if (string.IsNullOrEmpty(emailInfo.Emails))
                {
                    emailInfo.Error = "Nie wpisano adresu do wysyłki";
                    emailInfo.Ok = false;
                    return PartialView("EmailShare", emailInfo);
                }
                var ValidEmail = EmailService.GetEmails(emailInfo.Emails, out ErrorEmail);
                if (address.Address == emailInfo.sender)
                {
                    EmailService.Share(ValidEmail, emailInfo.Url, emailInfo.Name, address, out NotSend);
                    if (ErrorEmail.Count > 0)
                    {
                        emailInfo.Error = $"Błędny Adresy Email: {string.Join(", ", ErrorEmail)}";
                    }
                    if (NotSend.Count > 0)
                    {
                        emailInfo.Error += $"Nie udało się wysłać wiadomości na Adresy E-mail: { string.Join(", ", NotSend)}";
                    }
                    emailInfo.Ok = true;
                }
                else
                {
                    emailInfo.Ok = false;
                    emailInfo.Error = "Błędny adres Email Nadawcy";
                }
            }
            catch (Exception ex)
            {
                emailInfo.Error = "Ups Coś poszło nie tak";
                emailInfo.Ok = false;
            }
            return PartialView("EmailShare", emailInfo);
        }
        [HttpPost]
        public ActionResult ContactUs(EmailInfo emailInfo)
        {
            throw new NotImplementedException();
            try
            {
                MailAddress SendFrom = new MailAddress(emailInfo.sender);
                if (SendFrom.Address == emailInfo.sender)
                {
                    if (EmailService.ContactUs(SendFrom, emailInfo.Emails))
                    {
                        emailInfo.Ok = true;
                    }
                    else
                    {
                        emailInfo.Error = "Nie udało się wysłać wiadomości spróbuj później";
                    }
                }
                else
                {
                    emailInfo.Ok = false;
                    emailInfo.Error = "Błędny adres E-mail";
                }
            }
            catch(Exception ex)
            {
                emailInfo.Error = "Błędny adres E-mail";
            }
            return PartialView("ContactUs", emailInfo);
        }
    }
}
