﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco;

namespace Muzykoteka.Code.Services
{
    public class NewsletterService
    {
        FreshmailService freshmailService;
        string listKey;
        

        public NewsletterService()
        {
            var formsRoot = UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault(x => x.DocumentTypeAlias == "Zasoby");
            if (formsRoot == null)
            {
                LogHelper.Error(typeof(NewsletterService), "Registration Forms(Registrations) node not found in root content", new ArgumentNullException());
            }
            listKey = formsRoot.GetPropertyValue<string>("listKey");        
            freshmailService = new FreshmailService(formsRoot.GetPropertyValue<string>("apiKey"), formsRoot.GetPropertyValue<string>("apiSecret"));
        }

        
        /// <returns>
        /// 0 - error
        /// 1 - success
        /// 2 - already subscribed
        /// </returns>
        public int Subscribe(string email)
        {
            int result = 0;
            if (!String.IsNullOrEmpty(email) && email.Contains("@"))
            {
                var response = freshmailService.SubscribeToList(email, listKey);
                if (response.ContainsKey("status"))
                {
                    if ((string)response["status"] == "OK")
                    {
                        result = 1;
                    }
                    else if ((string)response["status"] == "SUBSCRIBED")
                    {
                        result = 2;
                    }
                }
            }
            return result;
        }

        public object Lists()
        {
            return freshmailService.Lists();
        }
    }
}
