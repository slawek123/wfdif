﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using Umbraco;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Net;
using Umbraco.Core.Logging;
using System.Configuration;
using Newtonsoft.Json;
using System.Text.RegularExpressions;


namespace Muzykoteka.Code.Services
{
    public class ElasticSearchService
    {
        private enum ESIndex
        {
            site_pl
        };

        private enum ESFields
        {
            tytul,
            lead,
            tresc,            
            pliki,                        
            kategoria,
            doctype,
            slowniki
        };

        private string m_address;
        private string m_query;        
        private string m_query_must;
        private string m_query_must_match;
        

        private string m_property_mapping_pl;        
        private List<string> m_ignore_doctype;

        private NameValueCollection m_umbraco_to_es_dtypes;        



        delegate object GetValuePrototype(string alias);
        delegate bool HasPropertyPrototype(string alias);

        

        private static ElasticSearchService g_ES = null;
        public static ElasticSearchService GetInstance()
        {
            if (g_ES == null)
                g_ES = new ElasticSearchService();
            return g_ES;
        }
            


        private ElasticSearchService()
        {            
            m_address = ConfigurationManager.AppSettings["es_url"].ToString();

            m_ignore_doctype = ConfigurationManager.AppSettings["es_except_doctype"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim()).ToList();

            m_property_mapping_pl = PrepWebCfgJson(ConfigurationManager.AppSettings["es_property_mapping_pl"]);           

            m_query = PrepWebCfgJson(ConfigurationManager.AppSettings["es_query"]);
            m_query_must = PrepWebCfgJson(ConfigurationManager.AppSettings["es_query_must"]);
            m_query_must_match = PrepWebCfgJson(ConfigurationManager.AppSettings["es_query_must_match"]);                       

            MapUmbracoTypesToES();

            RebuildIndexIfNeeded();
        }
             


        public void Index(IContent c)
        {
            LogI("Indexing: " + c.Name);
            _Index(c);            
        }

        
        public void RemoveFromIndex(IContent c)
        {
            LogI("Remove from index: " + c.Name);
            string es_type_name = m_umbraco_to_es_dtypes[c.ContentType.Alias];
            if (es_type_name == null)
                return;
            ESIndex index = ESIndex.site_pl;
            try
            {
                es_request("DELETE", index, es_type_name + "/" + c.Id, null);
            }
            catch (WebException ex)
            {
                if (ex.Response != null && ex.Response is HttpWebResponse && (ex.Response as HttpWebResponse).StatusCode == HttpStatusCode.NotFound) {
                    ;//skip, nie wszystkie typu indeksujemy
                }
                else
                {
                    throw;
                }
            }
        }


        public List<int> Search(int from, int return_count, string search_term, string[] epoka, string [] kategoria, out int total_count)
        {

            string matches = ""; 
            
            if (kategoria != null && kategoria.Length > 0)
            {
                string term = String.Join(" ", kategoria);
                if (matches.Length > 0)
                    matches += ",";
                matches += m_query_must_match
                            .Replace("$FIELD$", "doctype")
                            .Replace("$TOSEARCH$", term);
            }

            if (epoka != null && epoka.Length > 0)
            {
                string term = String.Join(" ", epoka);
                if (matches.Length > 0)
                    matches += ",";
                matches += m_query_must_match
                            .Replace("$FIELD$", "kategoria")
                            .Replace("$TOSEARCH$", term);
            }

            string must_query = m_query_must.Replace("$MATCHES$", matches);
        
            string req_query = m_query
                .Replace("$1$", from.ToString())
                .Replace("$2$", return_count.ToString())
                .Replace("$0$", search_term)
                .Replace("$3$", must_query);


            total_count = 0;
            List<int> ret = new List<int>();

            try
            {
                string response = es_webreq_post(
                    ESIndex.site_pl,                    
                    "/_search",
                    req_query);

                if (!String.IsNullOrEmpty(response))
                {
                    dynamic entries = JsonConvert.DeserializeObject<dynamic>(response);

                    total_count = entries.hits.total;

                    foreach (var h in entries.hits.hits)
                    {
                        int id = h["_id"];                        
                        ret.Add(id);
                    }
                }
            }
            catch (WebException ex)
            {
                string resp = (ex.Response == null ? "" : new System.IO.StreamReader(ex.Response.GetResponseStream()).ReadToEnd());                
                LogE("Error searching for '" + search_term + "'. " + resp, ex);                
            }
            catch (Exception ex)
            {
                LogE("Error searching for '" + search_term, ex);                
            }
            return ret;
        }



        //-----------------------------------------------
        // PRIVATE
        //-----------------------------------------------
        private void LogE(string msg, Exception ex)
        {
            LogHelper.Error(typeof(ElasticSearchService), msg, (ex == null ? new ArgumentNullException() : ex));
        }


        private void LogI(string msg)
        {
            LogHelper.Info(typeof(ElasticSearchService), msg);
        }

        
        private void _Index(object content) {
            int id = (content is IPublishedContent ? ((IPublishedContent)content).Id : ((IContent)content).Id);
            string content_type_alias = (content is IPublishedContent ? ((IPublishedContent)content).ContentType.Alias : ((IContent)content).ContentType.Alias);

            string parent_type_alias = null;
            if (content is IPublishedContent && ((IPublishedContent)content).Parent != null)
                parent_type_alias = ((IPublishedContent)content).Parent.DocumentTypeAlias;
            else {
                var p = ((IContent)content).Parent();
                if (p!= null)
                    parent_type_alias = p.ContentType.Alias;
            }          
            __Index(content, content_type_alias, id);                        
        }


        private void __Index(object content, string content_type_alias, int id)
        {                                  
            ESIndex index = ESIndex.site_pl;
            if (!ESIndexExists(index))
            {
                //zapisanie na nieistniejącym indexie - utworzy index ale bez mapowania
                //Zrestartowac app.
                throw new Exception("Search index missing");
            }

            string es_type_name = m_umbraco_to_es_dtypes[content_type_alias];
            if (es_type_name == null)
            {
                return;
            }

            string index_query = "";            
            
            object parent_with_template = content;
                                        
            if (content is IPublishedContent)
            {
                while (((IPublishedContent)parent_with_template).TemplateId <= 0)
                    parent_with_template = ((IPublishedContent)parent_with_template).Parent;
                id = ((IPublishedContent)parent_with_template).Id;
                content_type_alias = ((IPublishedContent)parent_with_template).ContentType.Alias;
            }
            else
            {
                while (((IContent)parent_with_template).Template == null)
                    parent_with_template = ((IContent)parent_with_template).Parent();
                id = ((IContent)parent_with_template).Id;
                content_type_alias = ((IContent)parent_with_template).ContentType.Alias;
            }

            if (parent_with_template != content)
            {
                es_type_name = m_umbraco_to_es_dtypes[content_type_alias];
                if (es_type_name == null)
                    return;
            }
            

            foreach (ESFields field in (parent_with_template != content ? new object[] { ESFields.tresc } : Enum.GetValues(typeof(ESFields))))
            {
                string field_name = Enum.GetName(typeof(ESFields), field);
                string val = GetFieldValue(field, field_name, parent_with_template);
                if (!String.IsNullOrEmpty(val))
                {
                    if (!String.IsNullOrEmpty(index_query))
                        index_query += ",";
                    index_query += "\"" + field_name + "\":\"" + val + "\"";                    
                }
            }

            index_query = "{" + index_query + "}";

            try
            {
                es_request("PUT",
                    index,
                    es_type_name + "/" + id,
                    index_query);
            }
            catch { }
        }

        private string ReplaceTags(string s)
        {
            return Regex.Replace(s, "<[^>]*(>|$)", string.Empty); //!remove tags!    ToDo: ... but better render them in typeahead
        }

        private string SplitForJsonArray(string s) {            
            string[] ar = s.Split(new char[] { '"', ' ', '\'', '-', '/', '\\', '.', ',', ';', '(', ')', '[', ']', '{', '}'}, StringSplitOptions.RemoveEmptyEntries);
            return "[\"" + String.Join("\",\"", ar) + "\"]";
        }
          
        private string GetValueWithoutMacroRender(object content, string propertyName)
        {
            return (string)(content is IPublishedContent ? ((IPublishedContent)content).GetProperty(propertyName).DataValue : ((IContent)content).GetValue(propertyName));
        }

        private string GetFieldValue(
            ESFields field, 
            string field_name,             
            object content)
        {            
            string content_alias;
            string content_name;
            int content_level;
            GetValuePrototype getValue;            
            HasPropertyPrototype hasValue;
            int id;

            if (content is IContent)
            {
                id = ((IContent)content).Id;
                content_alias = ((IContent)content).ContentType.Alias;
                content_name = ((IContent)content).Name;
                getValue = ((IContent)content).GetValue;
                hasValue = ((IContent)content).HasProperty;
                content_level = ((IContent)content).Level;                
            }
            else
            {
                id = ((IPublishedContent)content).Id;
                content_alias = ((IPublishedContent)content).ContentType.Alias;
                content_name = ((IPublishedContent)content).Name;
                getValue = ((IPublishedContent)content).GetPropertyValue;                            
                hasValue = ((IPublishedContent)content).HasProperty;
                content_level = ((IPublishedContent)content).Level;
            }
           
            string ret = "";

            switch (field)
            {

                case ESFields.tytul:
                    if (hasValue("tytul"))
                        ret = (string)getValue("tytul");                                        
                    if (string.IsNullOrEmpty(ret))
                        ret = content_name;
                    break;



                case ESFields.lead:
                    if (hasValue("lead"))
                        ret = GetValueWithoutMacroRender(content, "lead");
                    if (hasValue("naglowek"))
                        ret += " " + GetValueWithoutMacroRender(content, "naglowek");
                    break;



                case ESFields.tresc:
                    if (hasValue("tresc"))
                        ret = GetValueWithoutMacroRender(content, "tresc");
                    if (hasValue("opis"))
                        ret += " " + GetValueWithoutMacroRender(content, "opis");
                    if (hasValue("opis2"))
                        ret += " " + GetValueWithoutMacroRender(content, "opis2");
                    if (hasValue("rezyser"))
                        ret += " " + (string)getValue("rezyser");
                    if (hasValue("kraj"))
                        ret += " " + (string)getValue("kraj");
                    if (hasValue("ekipa"))
                    {

                        try
                        {
                            var obj = getValue("ekipa");
                            if (obj != null)
                            {
                                if (obj is Dictionary<string, string>)
                                {
                                    var dict = (Dictionary<string, string>)obj;
                                    ret += " " + String.Join(" ", dict.Keys) + " " + String.Join(" ", dict.Values);
                                }
                                else
                                {
                                    ret += obj.ToString();
                                }
                            }
                        }
                        catch
                        {
                            //TODO: duplikaty kluczy wykrzaczają
                        }
                    }
                    if (hasValue("obsada"))
                    {                        
                        try
                        {
                            var obj = getValue("obsada");
                            if (obj != null)
                            {
                                if (obj is Dictionary<string, string>)
                                {
                                    var dict = (Dictionary<string, string>)obj;
                                    ret += " " + String.Join(" ", dict.Keys) + " " + String.Join(" ", dict.Values);
                                }
                                else
                                {
                                    ret += obj.ToString();
                                }
                            }
                        }
                        catch
                        {
                            //TODO: duplikaty kluczy wykrzaczają
                        }
                    }
                    break;

                case ESFields.slowniki:
                    if (content_alias == "NaukaArtykul")
                    {                       
                        ret = GetSlownikEntries(content, "przedmiot", "Przedmiot");
                        ret += " " + GetSlownikEntries(content, "rodzajSzkoly", "Szkola");                        
                    }
                
                    break;
                    


                case ESFields.pliki:

                    if (hasValue("doPobrania"))
                    {
                        string media_ids = (string)getValue("doPobrania");
                        if (!String.IsNullOrEmpty(media_ids))
                        {
                            string[] media_ids_a = media_ids.Split(',');
                            foreach (var sid in media_ids_a)
                            {
                                int mediaId;
                                if (!String.IsNullOrEmpty(sid) && Int32.TryParse(sid, out mediaId))
                                {
                                    var media = UmbracoContext.Current.MediaCache.GetById(mediaId);
                                    if (media != null)
                                    {
                                        if (!String.IsNullOrEmpty(ret))
                                            ret += " ";
                                        ret += media.Name;
                                    }
                                }
                            }
                        }
                    }
                    break;

                case ESFields.kategoria:
                    if (hasValue("kategoria"))
                    {
                        ret = GetSlownikEntries(content, "kategoria", "Kategoria");
                    }
                    break;

                case ESFields.doctype:
                    if (content_level > 2)
                    {
                        if (content is IContent) {
                            var main_cat = ((IContent)content).Ancestors().Where(x => x.Level == 2).SingleOrDefault();
                            if (main_cat != null)
                            {
                                ret = main_cat.Name;
                            }
                        }
                        else
                        {
                            var main_cat = ((IPublishedContent)content).Ancestor(2);
                            if (main_cat != null)
                            {
                                ret = main_cat.Name;
                            }
                        }
                    }                            
                    break;                
            }
            return (ret == null ? "":ret.Replace("\n", " ").Replace("\r", " ").Replace("\"", " ").Replace("\t", " ").Replace("\\&quot;","").Trim());
        }


        private IEnumerable<IPublishedContent> GetSlownikEntries(string key, string slownik_name)
        {
            try
            {
                if (String.IsNullOrEmpty(key))
                {
                    return null;
                }
                else
                {
                    string[] keys = key.Split(',');
                    return UmbracoContext.Current.ContentCache
                                    .GetAtRoot()
                                    .FirstOrDefault(x => x.DocumentTypeAlias == "Zasoby")
                                    .Children
                                    .FirstOrDefault(x => x.DocumentTypeAlias == "Slowniki")
                                    .Children
                                    .FirstOrDefault(x => x.Name == slownik_name)
                                    .Children
                                    .Where(x => keys.Contains(x.Id.ToString()));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "GetSlownikEntries: " + key + "*" + (slownik_name == null ? "" : slownik_name));
            }
        }


        private string GetSlownikEntries(object content, string value_name, string slownik_name)
        {
            string key = "";
            if (content is IPublishedContent)
            {
                key = (string)((IPublishedContent)content).GetProperty(value_name).DataValue;                
            }
            else if (content is IContent)
            {
                key = (string)((IContent)content).GetValue(value_name);                
            }
            var entries = GetSlownikEntries(key, slownik_name);
            if (entries == null)
                return "";
            return String.Join(" ",
                        entries
                        .Select(x => x.Name)
                        .ToList());
        }


        private void MapUmbracoTypesToES()
        {
            m_umbraco_to_es_dtypes = new NameValueCollection();
            m_umbraco_to_es_dtypes.Add("Film", "Film");
            m_umbraco_to_es_dtypes.Add("PozostaleArtykul", "PozostaleArtykul");
            m_umbraco_to_es_dtypes.Add("NaukaArtykul", "NaukaArtykul");
            /*
            var srv = Umbraco.Core.ApplicationContext.Current.Services.ContentTypeService;
            var dtypes = srv.GetAllContentTypes();
            foreach (var dt in dtypes)
            {                
                if (!m_ignore_doctype.Contains(dt.Alias))
                {
                    if (dt.PropertyTypeExists("tytul") ||
                        dt.PropertyTypeExists("lead"))                        
                    {
                        m_umbraco_to_es_dtypes.Add(dt.Alias, dt.Alias);
                    }
                }
            }
            */
        }


        private bool RebuildIndexIfNeeded()
        {
            try
            {
                foreach (ESIndex index_type in Enum.GetValues(typeof(ESIndex)))
                {
                    if (!ESIndexExists(index_type))
                    {
                        BuildIndex(index_type);
                        try
                        {
                            Reindex(index_type);
                        }
                        catch
                        {
                            DeleteESIndex(index_type);
                            throw;
                        }
                    }
                }
            }            
            catch (WebException ex)
            {
                string resp = (ex.Response == null ? "": new System.IO.StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
                LogE("Unable to prepare indexes: " + resp, ex);
                return false;
            }
            catch (Exception ex)
            {
                LogE("Unable to prepare indexes", ex);
                return false;
            }             
            return true;
        }


        private bool BuildIndex(ESIndex index)
        {
            LogI("Building index " + GetIndexName(index));

            string field_mapping = m_property_mapping_pl;
            string fields = "";
            foreach (string field_name in Enum.GetNames(typeof(ESFields)))
            {
                if (!String.IsNullOrEmpty(fields))
                    fields += ",";
                fields += field_name + ":" + field_mapping;
            }
            
            string mapping = "";
            foreach (string key in m_umbraco_to_es_dtypes.Keys)
            {
                if (!String.IsNullOrEmpty(mapping))
                    mapping += ",";
                mapping += String.Format("\"{0}\":{{\"properties\":{{{1}}} }}", m_umbraco_to_es_dtypes[key], fields);
            }
            mapping = "{\"mappings\": {" + mapping + "}}";

            es_request("PUT", index, null, mapping);
            return true;
        }


        private void Reindex(ESIndex index)
        {
            LogI("Rebuilding index ..." + index.ToString());
            DateTime dtStart = DateTime.Now;
            var root = UmbracoContext.Current.ContentCache.GetAtRoot().FirstOrDefault();
            foreach (string dt in m_umbraco_to_es_dtypes.Keys)
            {
                foreach (var node in root.Descendants(dt))
                {
                    if (node.TemplateId > 0)
                        _Index(node);
                }
            }                                     
            LogI(String.Format("Index rebuild finished in {0}", (DateTime.Now - dtStart)));
        }



        private void DeleteESIndex(ESIndex index)
        {
            es_request("DELETE", index, null, null);
        }


        private string GetIndexName(ESIndex index)
        {
            return Enum.GetName(typeof(ESIndex), index);
        }


        private bool ESIndexExists(ESIndex index)
        {
            bool exists = false;
            try
            {
                es_request("GET", index, null, null);
                exists = true;
            }
            catch { }
            return exists;
        }








        private string es_request(string method, ESIndex index, string url_ext, string body)
        {
            string response = null;
            string url = m_address + "/" + GetIndexName(index) + (String.IsNullOrEmpty(url_ext) ? "" : "/" + url_ext);
            using (WebClient client = new WebClient())
            {
                if (method == "GET")
                    response = client.DownloadString(url);
                else
                {
                    client.Encoding = Encoding.Unicode;
                    response = client.UploadString(
                        url,
                        method,
                        body == null ? "" : body);
                }
            }
            return (method == "DELETE" ? null : response);
        }





        private string es_webreq_post(ESIndex index, string url_ext, string body)
        {
            string url = m_address + "/" + GetIndexName(index) + (String.IsNullOrEmpty(url_ext) ? "" : "/" + url_ext);
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "POST";            
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(body);            
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length);
            os.Close();

            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }


        private string PrepWebCfgJson(string appSetting)
        {
            return appSetting == null ? "" :
                     appSetting
                     .Replace("\'", "\"")
                     .Replace("\n", "")
                     .Replace("\t", "")
                     .Replace("\r", "")
                     .Replace("  ", "");
        }        
    }
}
