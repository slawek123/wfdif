﻿using RestSharp;
using System;
using System.Linq;

namespace Muzykoteka.Code.Services
{
    public class RestComunication
    {
        public IRestResponse GetResponse(IRestClient Client, IRestRequest restRequest) => Client.Execute(restRequest);
        public IRestClient PrepareClient(string Url) => new RestClient(Url);
        public IRestRequest PrepareRequest(string EndPointUrl, Method method = Method.GET) => new RestRequest(EndPointUrl, method);
    }
}
