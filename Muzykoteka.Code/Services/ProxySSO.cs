﻿using Muzykoteka.Code.Models.SSO;
using System;
using System.Collections.Generic;
using System.Linq;
using Muzykoteka.Code.Models.SSO;
using RestSharp;
using Newtonsoft.Json;
using System.Web.Configuration;
using RestSharp.Authenticators;
using System.Net;

namespace Muzykoteka.Code.Services
{
    public class ProxySSO
    {
        private readonly string _baseUrl;
        private readonly RestComunication _comunication;
        private readonly string _clientId;
        private readonly string _claimName = "wfdifonlineRules";
        public ProxySSO()
        {
            _baseUrl = WebConfigurationManager.AppSettings["sso_host"];
            _clientId = WebConfigurationManager.AppSettings["sso_client_id"];
            //_clientId = "local";
            //_baseUrl = "http://localhost:5000/";
            _comunication = new RestComunication();
        }
        public (TokenResponse, HttpStatusCode) Token(Token model, string language)
        {
            model.client_id = _clientId;
            var request = _comunication.PrepareRequest("/connect/token", Method.POST);
            request.AddObject(model);
            request.AddOrUpdateHeader("Content-Type", "application /x-www-form-urlencoded");
            request.AddHeader("Accept-Language", language);
            var Client = _comunication.PrepareClient(_baseUrl);
            var Response = _comunication.GetResponse(Client, request);
            return (JsonConvert.DeserializeObject<TokenResponse>(Response.Content), Response.StatusCode);
        }
        public (RegisterResponse, HttpStatusCode) Register(Register model, string redirectUrl, string language)
        {
            model.ClientID = _clientId;
            model.agreements = AddClientValueClaims();
            var request = _comunication.PrepareRequest("/connect/register", Method.POST);
            request.AddJsonBody(model);
            request.AddQueryParameter("returnUrl", redirectUrl);
            request.AddHeader("Accept-Language", language);
            var Client = _comunication.PrepareClient(_baseUrl);
            var Response = _comunication.GetResponse(Client, request);
            return (JsonConvert.DeserializeObject<RegisterResponse>(Response.Content), Response.StatusCode);
        }
        public (object, HttpStatusCode) Profile(Profile model, string token, string language)
        {
            model.ClientID = _clientId;
            model.Agreements = AddClientValueClaims();
            var client = _comunication.PrepareClient(_baseUrl);
            client.Timeout = -1;
            var request = new RestRequest("connect/profile", Method.POST);
            //request.AddHeader("Authorization", token);
            //request.AddHeader("Content-Type", "application/json");
            //request.AddParameter("application/json", model, ParameterType.RequestBody);
            request.AddHeader("Authorization", $"Bearer {token}");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", model.Serialize(), ParameterType.RequestBody);
            IRestResponse Response = client.Execute(request);
            return (string.IsNullOrEmpty(Response.Content) ? null : JsonConvert.DeserializeObject(Response.Content), Response.StatusCode);
        }
        public (object, HttpStatusCode) ForgotPassword(ForgotEmail model, string returnUrl, string language)
        {
            model.ClientID = _clientId;
            var request = _comunication.PrepareRequest("/connect/forgotpassword", Method.POST);
            request.AddJsonBody(model);
            request.AddQueryParameter("returnUrl", returnUrl);
            request.AddHeader("Accept-Language", language);
            var Client = _comunication.PrepareClient(_baseUrl);
            var Response = _comunication.GetResponse(Client, request);
            return (JsonConvert.DeserializeObject<TokenResponse>(Response.Content), Response.StatusCode);
        }
        public (object, HttpStatusCode) ResetPassword(RessetPassword model, string language)
        {
            model.ClientID = _clientId;
            var request = _comunication.PrepareRequest("/connect/resetpassword", Method.POST);
            request.AddJsonBody(model);
            request.AddHeader("Accept-Language", language);
            var Client = _comunication.PrepareClient(_baseUrl);
            var Response = _comunication.GetResponse(Client, request);
            return (JsonConvert.DeserializeObject<TokenResponse>(Response.Content), Response.StatusCode);
        }
        public (TokenResponse, HttpStatusCode) ChangePassword(ChangePassword model,string token)
        {
            var request = _comunication.PrepareRequest("/connect/changepassword", Method.POST);
            model.ClientID = _clientId;
            request.AddHeader("Authorization", $"Bearer {token}");
            request.AddParameter("application/json", model.Serialize(), ParameterType.RequestBody);
            var Client = _comunication.PrepareClient(_baseUrl);
            var Response = _comunication.GetResponse(Client, request);
            return (JsonConvert.DeserializeObject<TokenResponse>(Response.Content), Response.StatusCode);
        }
        public (object, HttpStatusCode) ChangeEmail()
        {
            var request = _comunication.PrepareRequest("/connect/changeemail");
            var Client = _comunication.PrepareClient(_baseUrl);
            var Response = _comunication.GetResponse(Client, request);
            return (JsonConvert.DeserializeObject<TokenResponse>(Response.Content), Response.StatusCode);
        }
        public (object, HttpStatusCode) ExtRegister(ExternalProfile model)
        {
            model.ClientID = _clientId;
            model.Agreements = AddClientValueClaims();
            var request = _comunication.PrepareRequest("/connect/registerext", Method.POST);
            request.AddJsonBody(model);
            var Client = _comunication.PrepareClient(_baseUrl);
            var Response = _comunication.GetResponse(Client, request);
            return (JsonConvert.DeserializeObject<TokenResponse>(Response.Content), Response.StatusCode);
        }
        public (object,HttpStatusCode) Delete(string token)
        {
            var request = _comunication.PrepareRequest("/connect/deleteaccount", Method.POST);
            request.AddHeader("Authorization", $"Bearer {token}");
            var Client = _comunication.PrepareClient(_baseUrl);
            var Response = _comunication.GetResponse(Client, request);
            return (JsonConvert.DeserializeObject<TokenResponse>(Response.Content), Response.StatusCode);
        }
        private ClientValueClaims AddClientValueClaims()
        {
            return new ClientValueClaims()
            {
                Items = new List<ClientValueClaimItem>()
                {
                    new ClientValueClaimItem()
                    {
                        Active=true,
                        Claim=_claimName,
                    }
                }
            };
        }
    }
}
