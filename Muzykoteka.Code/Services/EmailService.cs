﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using umbraco;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Muzykoteka.Code.Services
{
    public static class EmailService

    {
        public static void Share(
            MailAddressCollection emails,
            string url,
            string name,
            MailAddress sender,
            out List<string> ErrorEmail)
        {
            var Template = UmbracoContext.Current.ContentCache.GetAtRoot()
                         .Where(x => x.DocumentTypeAlias == "Zasoby")
                         .FirstOrDefault()
                         .Children
                         .Where(x => x.DocumentTypeAlias == "eMaile")
                         .FirstOrDefault()
                         .Children.Where(x => x.Name == "Udostępnione")
                         .FirstOrDefault();
            var ErrorMail = new List<string>();
            string templateBody = Template.GetPropertyValue<string>("tresc");
            string templateSubject = Template.GetPropertyValue<string>("naglowek");
            if (!string.IsNullOrEmpty(templateBody) && !string.IsNullOrEmpty(templateSubject))
            {
                templateBody = templateBody.Replace("%NAME%", name);
                templateBody = templateBody.Replace("/%URL%", url);
                templateBody = templateBody.Replace("%URL%", url);
                foreach (var email in emails)
                {
                    try
                    {
                        SendEmail(email, templateBody, templateSubject, sender);
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error(typeof(EmailService), "Nie udało się wysłać dla " + email, ex);
                        ErrorMail.Add(email.Address);

                    }
                }
            }
            ErrorEmail = ErrorMail;
        }
        private static void SendEmail(MailAddress SendTo, string body, string subject, MailAddress SendFrom)
        {
            MailMessage mailMessage = new MailMessage()
            {
                Subject = subject,
                Body = "<!DOCTYPE html><html><body>" + body + "</body></html>",
                IsBodyHtml = true,
                SubjectEncoding = new UTF7Encoding()
            };
            mailMessage.To.Add(SendTo);
            mailMessage.ReplyToList.Add(SendFrom);
            SendEmail(mailMessage);

        }
        public static MailAddressCollection GetEmails(string emails, out List<string> errorEmail)
        {
            var OkEmails = new MailAddressCollection();
            List<string> ErrorEmail = new List<string>();
            foreach (var email in emails.Split(' '))
            {
                try
                {
                    MailAddress address = new MailAddress(email);
                    if (address.Address == email)
                    {
                        OkEmails.Add(address);
                    }
                }
                catch (Exception ex)
                {
                    ErrorEmail.Add(email);
                    LogHelper.Error(typeof(EmailService), "Błędny adress Email " + email, ex);
                }
            }
            errorEmail = ErrorEmail;
            return OkEmails;
        }
        public static bool ContactUs(MailAddress SendFrom, string Body)
        {
            IPublishedContent Template = UmbracoContext.Current.ContentCache.GetAtRoot()
                            .Where(x => x.DocumentTypeAlias == "Zasoby")
                            .FirstOrDefault()
                            .Children
                            .Where(x => x.DocumentTypeAlias == "eMaile")
                            .FirstOrDefault();
            MailAddress SendTo = new MailAddress(Template.GetPropertyValue<string>("email"));
            try
            {
                SendEmail(SendTo, Body, "Formluarz kontaktowy WFDiF", SendFrom);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(EmailService), "Nie udało się wysłać Wiadomości", ex);
                return false;
            }
        }
        public static bool ShareLesson(MailAddress SendFor, string body, List<HttpPostedFileBase> Files, MailAddress SendFrom, string Url, string name, DateTime date)
        {
            var Template = UmbracoContext.Current.ContentCache.GetAtRoot()
                         .Where(x => x.DocumentTypeAlias == "Zasoby")
                         .FirstOrDefault()
                         .Children
                         .Where(x => x.DocumentTypeAlias == "eMaile")
                         .FirstOrDefault()
                         .Children.Where(x => x.Name == "Lekcja")
                         .FirstOrDefault();
            var ErrorMail = new List<string>();
            string templateBody = Template.GetPropertyValue<string>("tresc");
            string templateSubject = Template.GetPropertyValue<string>("naglowek");
            if (!string.IsNullOrEmpty(templateBody) && !string.IsNullOrEmpty(templateSubject))
            {
                templateBody = templateBody.Replace("%LEKCJA%", name)
                    .Replace("/%URL%", Url)
                    .Replace("%URL%", Url)
                    .Replace("%TEXT%", body)
                    .Replace("%DATAWYKONANIA%", date.ToString("g"));

                MailMessage mailMessage = new MailMessage()
                {
                    Subject = templateSubject,
                    Body = "<!DOCTYPE html><html><body>" + templateBody + "</body></html>",
                    IsBodyHtml = true,
                    SubjectEncoding = new UTF7Encoding()
                };
                mailMessage.To.Add(SendFor);
                mailMessage.ReplyToList.Add(SendFrom);
                foreach (var item in Files)
                {
                    Attachment attachment = new Attachment(item.InputStream, name: item.FileName);
                    mailMessage.Attachments.Add(attachment);
                }
                SendEmail(mailMessage);

            }
            return false;
        }
        private static void SendEmail(MailMessage mailMessage)
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Send(mailMessage);
            LogHelper.Info(typeof(EmailService), "Email Send to " + mailMessage.To + " from " + mailMessage.From + " subject " + mailMessage.Subject);
        }
        public static void Notification(string Subject, string Body)
        {
            MailMessage mailMessage = new MailMessage()
            {
                Subject = Subject,
                Body = "<!DOCTYPE html><html><body>" + Body + "</body></html>",
                IsBodyHtml = true,
                SubjectEncoding = new UTF7Encoding()
            };
            IPublishedContent Template = UmbracoContext.Current.ContentCache.GetAtRoot()
                            .Where(x => x.DocumentTypeAlias == "Zasoby")
                            .FirstOrDefault()
                            .Children
                            .Where(x => x.DocumentTypeAlias == "eMaile")
                            .FirstOrDefault();
            MailAddress SendTo = new MailAddress(WebConfigurationManager.AppSettings["notification_email"]);
            mailMessage.To.Add(SendTo);
            SendEmail(mailMessage);
        }
    }
}
