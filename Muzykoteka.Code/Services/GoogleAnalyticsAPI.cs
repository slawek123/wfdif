﻿using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Muzykoteka.Code.Services
{

    public class GoogleAnalyticsAPI
    {
        public AnalyticsService Service { get; set; }

        public GoogleAnalyticsAPI(string keyPath, string accountEmailAddress)
        {
            var certificate = new X509Certificate2(keyPath, "notasecret", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);

            var credentials = new ServiceAccountCredential(
               new ServiceAccountCredential.Initializer(accountEmailAddress)
               {
                   Scopes = new[] { AnalyticsService.Scope.AnalyticsReadonly }
               }.FromCertificate(certificate));

            Service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credentials,
                ApplicationName = "WorthlessVariable"
            });
        }
        public AnalyticDataPoint GetAnalyticsData(string profileId, string[] dimensions, string[] metrics, string[] Filters, string[] Sort, DateTime startDate, DateTime endDate, int startIndex = 1, int Max = 10)
        {
            AnalyticDataPoint data = new AnalyticDataPoint();

                profileId = string.Format("ga:{0}", profileId);

            //Make initial call to service.
            //Then check if a next link exists in the response,
            //if so parse and call again using start index param.
            GaData response = null;

            var request = BuildAnalyticRequest(profileId, dimensions, metrics, startDate, endDate, startIndex, Sort, Filters, Max);
            response = request.Execute();
            data.ColumnHeaders = response.ColumnHeaders;
            data.Rows.AddRange(response.Rows);

            //} while (!string.IsNullOrEmpty(response.NextLink));

            return data;
        }
        public IList<Profile> GetAvailableProfiles()
        {
            try
            {
                var response = Service.Management.Profiles.List("~all", "~all").Execute();
                return response.Items;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private DataResource.GaResource.GetRequest BuildAnalyticRequest(string profileId, string[] dimensions, string[] metrics,
                                                                            DateTime startDate, DateTime endDate, int startIndex, string[] Sort, string[] Filters, int Max = 10)
        {
            DataResource.GaResource.GetRequest request = Service.Data.Ga.Get(profileId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), string.Join(",", metrics));
            if (dimensions != null)
                request.Dimensions = string.Join(",", dimensions);
            request.StartIndex = startIndex;
            if (Sort != null)
                request.Sort = string.Join(",", Sort);
            if (Filters != null)
                request.Filters = string.Join(",", Filters);
            request.MaxResults = Max;
            return request;
        }
        public class AnalyticDataPoint
        {
            public AnalyticDataPoint()
            {
                Rows = new List<IList<string>>();
            }

            public IList<GaData.ColumnHeadersData> ColumnHeaders { get; set; }
            public List<IList<string>> Rows { get; set; }
        }
    }
}
