﻿using FmAPI;
using FmAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Logging;
using Newtonsoft.Json;

namespace Muzykoteka.Code.Services
{
    public class FreshmailService
    {
        FmAPIWrapper Wrapper = new FmAPIWrapper();
        ConfigModel Config = new ConfigModel();

        public FreshmailService(string apiKey, string apiSecret)
        {
            Config.SetKey(apiKey);
            Config.SetSecret(apiSecret);
        }

        public IDictionary<string, object> SubscribeToList(string email, string listKey)
        {
            Config.SetContentType("application/json");
            return GetResponse("subscriber/add", new { email = email, list = listKey });
        }

        public IDictionary<string, object> Ping()
        {
            return GetResponse("ping");
        }

        public IDictionary<string, object> Lists()
        {
            return GetResponse("subscribers_list/lists");
        }

        IDictionary<string, object> GetResponse(string path, object data = null)
        {
            IDictionary<string, object> result = new Dictionary<string, object>();
            
            try
            {
                var response = data == null ? Wrapper.ExecuteRequest(path, Config) : Wrapper.ExecuteRequest(path, Config, data);

                if (response.IsSuccessful())
                {
                    WebExceptionStatus webExceptionStatus;
                    HttpStatusCode httpStatusCode;

                    if (response.GetWebExceptionStatus().HasValue)
                        webExceptionStatus = response.GetWebExceptionStatus().Value;
                    if (response.GetHttpStatusCode().HasValue)
                        httpStatusCode = response.GetHttpStatusCode().Value;

                    if (!response.IsResponseRaw())
                    {
                        result = (IDictionary<string, object>)response.GetResponse();                        
                    }
                    else
                    {
                        var rawResponse = response.GetRawResponse();
                        result = new Dictionary<string, object>() { {"status", "RAW"}, {"data", rawResponse} };
                    }                    
                }
                else
                {
                    IEnumerable<RestError> restErrors = response.GetRestErrors().Any() ? response.GetRestErrors() : new List<RestError>();
                    if (IsRegistered(restErrors))
                    {
                        result = new Dictionary<string, object>() { { "status", "SUBSCRIBED" }};
                    }
                    else
                    {
                        LogHelper.Error(typeof(FreshmailService), String.Format("Error getting response from FreshmailApi. Path: {0}, Errors: {1}", path, JsonConvert.SerializeObject(restErrors)), new Exception());
                    }                    
                }
                LogHelper.Debug(typeof(FreshmailService), "Response received from freshmail (parsed): " + JsonConvert.SerializeObject(result));
            }
            catch (Exception exception)
            {
                LogHelper.Error(typeof(FreshmailService), String.Format("Error getting response from FreshmailApi. Path: {0}", path), exception);
            }

            return result;
        }

        bool IsRegistered(IEnumerable<RestError> errors)
        {
            var alreadyRegisteredError = errors.FirstOrDefault(x => x.Code == "1304");
            return alreadyRegisteredError != null;
        }

    }
}
