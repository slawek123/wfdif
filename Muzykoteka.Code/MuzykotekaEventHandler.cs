﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Umbraco.Core;
using Umbraco.Core.Services;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Web;
using Umbraco;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Logging;
using Umbraco.Core.Dictionary;
using Umbraco.Core.Publishing;
using Newtonsoft.Json;
using Muzykoteka.Code.Services;
using Merchello.Core.Services;
using Merchello.Core.Events;
using Merchello.Core.Models;
using UmbracoIdentityServer.Client.Controllers;


using Umbraco.Web.Models.ContentEditing;
using Umbraco.Web.Mvc;

namespace Muzykoteka.Code
{
    public class MuzykotekaEventHandler : ApplicationEventHandler
    {
        UmbracoApplicationBase umbracoApplication;
        ApplicationContext applicationContext;
        ElasticSearchService esearch = null;

        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(BaseController));
        }
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            this.umbracoApplication = umbracoApplication;
            this.applicationContext = applicationContext;
            if (System.Configuration.ConfigurationManager.AppSettings["es_disabled"] != "1")
            {
                this.esearch = ElasticSearchService.GetInstance();
            }

            ContentService.Saving += content_saving;
            ContentService.Published += content_published;
            ContentService.UnPublished += content_unpublished;

            ShipmentService.StatusChanged += ShipmentServiceOnStatusChanged;
            ShipmentService.Saved += ShipmentServiceOnSaved;
        }

        private void ShipmentServiceOnSaved(IShipmentService sender, SaveEventArgs<IShipment> e)
        {
            foreach (var shipment in e.SavedEntities)
            {
                if (shipment.UpdateDate == shipment.CreateDate &&
                    shipment.ShipmentStatus.Key.Equals(Merchello.Core.Constants.DefaultKeys.ShipmentStatus.Shipped))
                {
                    var address = shipment.GetDestinationAddress();
                    if (!string.IsNullOrEmpty(address.Email))
                        Merchello.Core.Notification.Trigger("OrderShipped", shipment, new[] { address.Email });
                    shipment.UpdateDate = shipment.UpdateDate.AddMilliseconds(1);
                    sender.Save(shipment, false);
                }
            }
        }


        private void ShipmentServiceOnStatusChanged(IShipmentService sender, StatusChangeEventArgs<IShipment> e)
        {
            foreach (var shipment in e.StatusChangedEntities)
            {
                if (shipment.ShipmentStatus.Key.Equals(Merchello.Core.Constants.DefaultKeys.ShipmentStatus.Shipped))
                {
                    var address = shipment.GetDestinationAddress();
                    if (!string.IsNullOrEmpty(address.Email))
                    {

                        try
                        {
                            Merchello.Core.Notification.Trigger("OrderShipped", shipment, new[] { address.Email });
                        }
                        catch
                        {
                            //pierwsza zmiana statusu na Shiped udaje sie bez bledow, nastepne to Exception w kodzie merchello
                        }
                    }
                }
            }
        }


        private void content_saving(IContentService s, SaveEventArgs<IContent> e)
        {
            foreach (var n in e.SavedEntities)
            {
                if (n.HasProperty("licznikOdwiedzin"))
                {
                    var counter = Helper.GetVisitCounter(n.Id);
                    n.SetValue("licznikOdwiedzin", counter);
                }
            }
        }


        private void content_published(IPublishingStrategy s, PublishEventArgs<IContent> e)
        {
            try
            {
                if (esearch != null)
                {
                    foreach (var c in e.PublishedEntities)
                    {
                        var testy = c.GetValue<bool>("searchHide");
                        if (!testy)
                            esearch.Index(c);

                        else
                        {
                            esearch.RemoveFromIndex(c);
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }

        private void content_unpublished(IPublishingStrategy s, PublishEventArgs<IContent> e)
        {
            if (esearch != null)
            {
                foreach (var c in e.PublishedEntities)
                {
                    esearch.RemoveFromIndex(c);
                }
            }
        }
    }
}
