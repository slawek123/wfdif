﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Umbraco.Web;
using System.Net;
//using PG.UmbracoExtensions.Helpers;
using Umbraco.Core.Logging;
using System.Web.Http;
using Muzykoteka.Code.Services;

namespace Muzykoteka.Code.WebApi
{
    public class NewsletterController : Umbraco.Web.WebApi.UmbracoApiController
    {
        NewsletterService newsletterService;

        public NewsletterController()
        {
            newsletterService = new NewsletterService();
        }

        [HttpGet]
        public HttpResponseMessage Subscribe(string email)
        {
            int result = newsletterService.Subscribe(email);
            return (result > 0 ? 
                Request.CreateResponse(HttpStatusCode.OK, result) : 
                new HttpResponseMessage(HttpStatusCode.BadRequest));
        }

        [HttpGet]
        public HttpResponseMessage Lists()
        {
            var result = newsletterService.Lists();
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
