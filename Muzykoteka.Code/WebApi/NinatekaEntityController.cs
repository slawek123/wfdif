﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Umbraco.Web.Mvc;
using Muzykoteka.Code.Models;
using Umbraco.Web.Editors;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence.DatabaseModelDefinitions;
using Newtonsoft.Json;



namespace Muzykoteka.Code.WebApi
{
    [PluginController("vw")]
    public class NinatekaEntityController : UmbracoAuthorizedJsonController
    {

        private string m_ninateka_url;
        private string m_platform_codename;
        private static List<string> m_allowed_metadata = new List<string>() {
            "producent",
            "autor tekstu",
            "choreografia",
            "chór",
            "chórmistrz",
            "dyrygent",
            "fortepian",
            "głos",
            "głos recytujący",
            "instrumentalista",
            "kompozytor",
            "orkiestra",
            "produkcja",
            "prowadzący",
            "reżyser",
            "uczestnik",
            "wokalista",
            "zespół",
        };



        private static Dictionary<string,int> m_metadata_sort = new Dictionary<string,int>() {
            {"kompozytor",1},
            {"czas trwania",2},
            {"rok produkcji",3},
            {"producent",4},
            {"reżyser",5},
            {"reżyseria",5},
            {"wykonawcy",6},
            {"instrumentalista",7},
            {"głos",8},
            {"fortepian",9},
            {"orkiestra",10},
            {"dyrygent",11},
            {"chór",12},
            {"chórmistrz",13},
            {"zespół",14},
            {"wokalista",15},
            {"głos recytujący",16},
            {"autor tekstu",17},
            {"choreografia",18},
            {"prowadzący",19},
            {"epoka",20},
            {"gatunek",21},
            {"rok powstania",22},
            {"cykl",23},
            {"uczestnik",24},  
        };

        public NinatekaEntityController () {
            m_ninateka_url = System.Configuration.ConfigurationManager.AppSettings["ninateka_api_url"];
            m_platform_codename = System.Configuration.ConfigurationManager.AppSettings["ninateka_platform"];
        }


        public IEnumerable<NinatekaCategory> GetCategories()
        {
            string url = String.Format("{0}GetCategories?platformCodename={1}&categoryTypeCodename=maincategory", m_ninateka_url, m_platform_codename);
            string data;
            using (var w = new WebClient())
            {
                w.Encoding = System.Text.Encoding.UTF8;
                data = w.DownloadString(url);
            }
            dynamic json = JsonConvert.DeserializeObject<dynamic>(data);
            List<NinatekaCategory> ret = new List<NinatekaCategory>();
            foreach (var c in json.Categories)
            {
                if (c.IsVisible == true)
                {
                    ret.Add(new NinatekaCategory()
                    {
                        Id = c.Id,
                        Name = c.Name,
                    });
                }
            }
            return ret;
        }

        public PagedResult<NinatekaEntity> GetByCodename(string codename)
        {
            string url = String.Format("{0}GetMovie?movieCodename={1}&platformCodename={2}&movieflags=35",
                m_ninateka_url, codename, m_platform_codename);
                 
            string data;
            using (var w = new WebClient())
            {
                w.Encoding = System.Text.Encoding.UTF8;
                data = w.DownloadString(url);
            }
            dynamic json = JsonConvert.DeserializeObject<dynamic>(data);
            if (json != null && json.Success == true && json.Movie != null)
            {
                var items = new List<NinatekaEntity>();
                var entity = ConvertToNE(json.Movie);
                items.Add(entity);

                var pagedResult = new PagedResult<NinatekaEntity>(1, 1, 10);
                pagedResult.Items = items;
                return pagedResult;
            }
            else
            {
                return new PagedResult<NinatekaEntity>(0, 0, 0);
            }
        }


        public NinatekaEntity GetNEByCodename(string codename)
        {
            string url = String.Format("{0}GetMovie?movieCodename={1}&platformCodename={2}&movieflags=35",
                m_ninateka_url, codename, m_platform_codename);
                 
            string data;
            using (var w = new WebClient())
            {
                w.Encoding = System.Text.Encoding.UTF8;
                data = w.DownloadString(url);
            }
            dynamic json = JsonConvert.DeserializeObject<dynamic>(data);
            if (json != null && json.Success == true && json.Movie != null)
            {
                return ConvertToNE(json.Movie);
            }
            else
            {
                return null;
            }
        }

        
        public NinatekaEntity GetById(int mid)
        {
            string url = String.Format("{0}GetMovie?id={1}&platformCodename={2}&movieflags=35",
                m_ninateka_url, mid, m_platform_codename);

            string data;
            using (var w = new WebClient())
            {
                w.Encoding = System.Text.Encoding.UTF8;
                data = w.DownloadString(url);
            }
            dynamic json = JsonConvert.DeserializeObject<dynamic>(data);
            if (json != null && json.Success == true && json.Movie != null)
            {                
                return ConvertToNE(json.Movie);             
            }
            else
            {
                return null;
            }
        }


        public PagedResult<NinatekaEntity> GetAll(
            int pageNumber = 1,
            int pageSize = 0,
            string orderBy = "2",
            Direction orderDirection = Direction.Ascending,
            string filter = "",
            string categories = "",
            string selectedCodename = "")
        {                                    
            string filterQuery = string.IsNullOrEmpty(filter) ? "" : "&query=" + System.Web.HttpContext.Current.Server.UrlEncode(filter);
            string categoriesQuery = "";
            if (!String.IsNullOrEmpty(categories))
            {
                foreach (var s in categories.Split(new []{','}, StringSplitOptions.RemoveEmptyEntries))
                {
                    categoriesQuery += "&orCategoryIds=" + s;
                }
            }
            string orderByQuery = "";
            if (!String.IsNullOrEmpty(orderBy))
            {
                orderByQuery = "&sort=" + orderBy;
                if (orderDirection == Direction.Descending)
                    orderByQuery += "&desc=true";
            }


            string url = String.Format("{0}GetMovies?paid=false&page={1}&limit={2}&platformCodename={3}&movieFlags=35{4}{5}{6}",
                m_ninateka_url, pageNumber, pageSize, m_platform_codename, categoriesQuery, filterQuery, orderByQuery);
                           
            string data;
            using (var w = new WebClient()) {
                w.Encoding = System.Text.Encoding.UTF8;                
                data = w.DownloadString(url);                
            }
            dynamic json = JsonConvert.DeserializeObject<dynamic>(data);  

            int totalChildren = json.Pagination.TotalItems;

            List<NinatekaEntity> ret = new List<NinatekaEntity>();
            var movies = json.Movies;
            foreach (var movie in movies)
            {   
                var entity = ConvertToNE(movie);
                ret.Add(entity);
            }

            if (totalChildren == 0)
            {
                return new PagedResult<NinatekaEntity>(0, 0, 0);
            }

            var pagedResult = new PagedResult<NinatekaEntity>(totalChildren, pageNumber, pageSize);
            pagedResult.Items = ret;                
            return pagedResult;            
        }


        private class MetadataHelper
        {
            public string name { get; set; }
            public string value { get; set; }
            public int sort { get; set; }
        }

        private void AddMetadata(string k, string v, ref List<MetadataHelper> dict_cat)
        {
            int sort = 1000;            
            if (m_metadata_sort.ContainsKey(k.ToLower()))
            {
                sort = m_metadata_sort[k.ToLower()];
            }
            dict_cat.Add(new MetadataHelper()
            {
                name = k,
                value = v,
                sort = sort,
            });
        }


        private NinatekaEntity ConvertToNE(dynamic movie)
        {
            var entity = new NinatekaEntity()
            {
                Id = movie.Id,
                Codename = movie.Codename,
                Title = movie.Title,
                ImageUrl = null
            };
            if (movie.Images.Count > 0)
            {
                entity.ImageUrl = movie.Images[0].Url;
            }
            
            
            List<MetadataHelper> dict_cat = new List<MetadataHelper> ();
                        
            var categories = movie.Categories;
            if (categories != null)
            {
                foreach (var c in categories)
                {
                    string k = c.CategoryTypeName;
                    string v = c.Name;
                    if (!String.IsNullOrEmpty(k) && !String.IsNullOrEmpty(v))
                    {
                        var found = dict_cat.Where(x=>x.name == k).FirstOrDefault();
                        if (found != null)
                        {
                            found.value += "," + v;
                        }
                        else
                        {
                            if (m_allowed_metadata.Contains(k.ToLower()))
                            {
                                AddMetadata (k, v, ref dict_cat);
                            }                            
                        }
                    }
                }
            }

            var persons = movie.Persons;
            if (persons != null)
            {
                foreach (var p in persons)
                {
                    string k = p.RoleName;
                    string v = p.FirstName + " " + p.LastName;
                    
                    if (!String.IsNullOrEmpty(k) && !String.IsNullOrEmpty(v))
                    {
                        var found = dict_cat.Where(x=>x.name == k).FirstOrDefault();
                        if (found != null) {                        
                            found.value += "," + v;
                        }
                        else
                        {
                            AddMetadata (k, v, ref dict_cat);
                        }
                    }
                }
            }

            if (movie.Duration != null) {
                AddMetadata ("czas trwania", movie.Duration.ToString(), ref dict_cat);
            }
            if (movie.Year != null) {
                AddMetadata ("rok produkcji", movie.Year.ToString(), ref dict_cat);
            }               

            if (dict_cat.Count > 0)
            {
               
                entity.Metadata = new string[dict_cat.Count, 2];
                int idx = 0;
                foreach (var e in dict_cat.OrderBy(x=>x.sort))
                {
                    entity.Metadata[idx, 0] = e.name;
                    entity.Metadata[idx, 1] = e.value;
                    idx++;
                }
            }
            
            return entity;
        }
    }
}
