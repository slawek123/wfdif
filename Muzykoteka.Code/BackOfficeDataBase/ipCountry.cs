﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIOMatic.Attributes;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Muzykoteka.Code.BackOfficeDataBase
{
    [UIOMatic("ipCountry", "Dozwolone Kraje", "Kraj", FolderIcon = "icon-users", ItemIcon = "icon-user",
        RenderType = UIOMatic.Enums.UIOMaticRenderType.List,ReadOnly = false)]
    [TableName("ipCountry")]
    public class ipCountry
    {
        [Column("Id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        [Required]
        [UIOMaticListViewField(Name = "Nazwa kraju")]
        [Column("country_name")]
        [UIOMaticField(Name = "Nazwa kraju")]
        public string country_name { get; set; }

        [Required]
        [UIOMaticListViewField(Name = "Kod kraju")]
        [Column("country_iso_code")]
        [UIOMaticField(Name = "Kod kraju")]
        public string country_iso_code { get; set; }

        [Required]
        [UIOMaticListViewField(Name = "Dostępny")]
        [UIOMaticListViewFilter(Name = "Dostępny")]
        [Column("allowed")]
        [UIOMaticField(Name = "Dostępny", View = UIOMatic.Constants.FieldEditors.CheckBox)]
        public bool allowed { get; set; }

        [Required]
        [UIOMaticListViewField(Name = "Należy do Uni Europejskiej")]
        [UIOMaticListViewFilter(Name = "Należy do Uni Europejskiej")]
        [Column("is_in_european_union")]
        [UIOMaticField(Name = "Należy do Uni Europejskiej", View = UIOMatic.Constants.FieldEditors.CheckBox)]
        public bool is_in_european_union { get; set; }

        [Required]
        [Column("continent_name")]
        [UIOMaticField(Name = "kontynent")]
        [UIOMaticListViewFilter(Name = "kontynent")]
        public string continent_name { get; set; }

        //[Required]
        //[UIOMaticListViewFilter(Name = "Kod kontynentu")]
        //[Column("continent_code")]
        //[UIOMaticField(Name = "Kod kontynentu")]
        //public string continent_code { get; set; }

        public override string ToString()
        {
            return country_name;
        }

    }
    [UIOMaticAction("export", "Export", "~/App_Plugins/UIOMaticAddons/export.html")]

    public class Enable { }
}
