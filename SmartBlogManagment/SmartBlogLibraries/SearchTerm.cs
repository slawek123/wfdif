namespace SmartBlogLibraries
{
	public class SearchTerm
	{
		internal string DocumentType
		{
			get;
			set;
		}

		internal bool? Published
		{
			get;
			set;
		}
	}
}
