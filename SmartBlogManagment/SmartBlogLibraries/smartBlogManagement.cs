using SmartBlogLibraries.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using umbraco;
using umbraco.BusinessLogic;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using umbraco.DataLayer;

namespace SmartBlogLibraries
{
    public class smartBlogManagement : UserControl
    {
        private string defaultAuthor = "";

        private string moderatorCommentEmail = "";

        private string robotEmail = "";

        private bool sendApproveEmail = false;

        private bool masterAutoApproveComments = false;

        private bool masterDisableComments = false;

        private bool useSummaryInList = false;

        private bool facebookIntegration = false;

        protected ScriptManager ScriptManager1;

        //protected UpdatePanel commentListUpdater;

        //protected Button ApproveSelected;

        //protected Button DeleteSelected;

        //protected Button RefreshList;

        //protected Repeater rptComments;

        protected TextBox defaultAuthorName;

        //protected CheckBox approvalEmail;

        //protected TextBox commentEmailAddress;

        //protected TextBox robotEmailAddress;

        //protected CheckBox autoApproveComments;

        protected CheckBox disableComments;

        protected CheckBox useSummary;

        //protected CheckBox disableFacebookIntegration;

        protected Button saveSettings;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!base.IsPostBack)
            {
                LoadSettings();
                //showResults(e);
                defaultAuthorName.Text = defaultAuthor;
                //approvalEmail.Checked = sendApproveEmail;
                //commentEmailAddress.Text = moderatorCommentEmail;
                //autoApproveComments.Checked = masterAutoApproveComments;
                disableComments.Checked = masterDisableComments;
                useSummary.Checked = useSummaryInList;
                //robotEmailAddress.Text = robotEmail;
                //disableFacebookIntegration.Checked = facebookIntegration;
            }
        }

        public void LoadSettings()
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "/config/SmartBlog.config");
            defaultAuthor = xmlDocument.GetElementsByTagName("defaultAuthor")[0].InnerText;
            sendApproveEmail = bool.Parse(xmlDocument.GetElementsByTagName("sendApproveEmail")[0].InnerText);
            moderatorCommentEmail = xmlDocument.GetElementsByTagName("moderatorCommentEmail")[0].InnerText;
            //masterAutoApproveComments = bool.Parse(xmlDocument.GetElementsByTagName("autoApproveComments")[0].InnerText);
            masterDisableComments = bool.Parse(xmlDocument.GetElementsByTagName("masterDisableComments")[0].InnerText);
            useSummaryInList = bool.Parse(xmlDocument.GetElementsByTagName("useSummaryOnList")[0].InnerText);
            robotEmail = xmlDocument.GetElementsByTagName("robotEmail")[0].InnerText;
            facebookIntegration = bool.Parse(xmlDocument.GetElementsByTagName("disableFacebookIntegration")[0].InnerText);
        }

        protected void SaveSettings_Click(object sender, EventArgs e)
        {
            defaultAuthor = defaultAuthorName.Text;
            //sendApproveEmail = approvalEmail.Checked;
            //moderatorCommentEmail = commentEmailAddress.Text;
            //masterAutoApproveComments = autoApproveComments.Checked;
            masterDisableComments = disableComments.Checked;
            useSummaryInList = useSummary.Checked;
            //robotEmail = robotEmailAddress.Text;
            //facebookIntegration = disableFacebookIntegration.Checked;
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "/config/SmartBlog.config");
                xmlDocument.GetElementsByTagName("defaultAuthor")[0].InnerText = defaultAuthor;
                xmlDocument.GetElementsByTagName("sendApproveEmail")[0].InnerText = sendApproveEmail.ToString();
                xmlDocument.GetElementsByTagName("moderatorCommentEmail")[0].InnerText = moderatorCommentEmail;
                xmlDocument.GetElementsByTagName("autoApproveComments")[0].InnerText = masterAutoApproveComments.ToString();
                xmlDocument.GetElementsByTagName("masterDisableComments")[0].InnerText = masterDisableComments.ToString();
                xmlDocument.GetElementsByTagName("useSummaryOnList")[0].InnerText = useSummaryInList.ToString();
                xmlDocument.GetElementsByTagName("robotEmail")[0].InnerText = robotEmail.ToString();
                xmlDocument.GetElementsByTagName("disableFacebookIntegration")[0].InnerText = facebookIntegration.ToString();
                xmlDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "/config/SmartBlog.config");
            }
            catch (Exception)
            {
            }
            Global.GetConfig();
        }

        //protected void RefreshComments_Click(object sender, EventArgs e)
        //{
        //    showResults(e);
        //}

        //protected void showResults(EventArgs e)
        //{
        //    SearchTerm searchTerm = new SearchTerm();
        //    searchTerm.DocumentType = "SmartBlogComment";
        //    searchTerm.Published = false;
        //    rptComments.DataSource = GetAllDocuments(searchTerm);
        //    rptComments.DataBind();
        //}

        //protected void rptComments_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (rptComments.Items.Count < 1 && e.Item.ItemType == ListItemType.Footer)
        //    {
        //        Label label = (Label)e.Item.FindControl("lblEmptyData");
        //        label.Visible = true;
        //    }
        //}

        public void rptComments_ItemCommand(object Sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.ToString() == "DeleteComment" && e.CommandArgument.ToString() != "")
            {
                deleteComment(Convert.ToInt32(e.CommandArgument.ToString()));
            }
            else if (e.CommandName.ToString() == "ApproveComment" && e.CommandArgument.ToString() != "")
            {
                approveComment(Convert.ToInt32(e.CommandArgument.ToString()));
            }
            //showResults(null);
        }

        private bool deleteComment(int intId)
        {
            return Cms.DeleteContentNode(intId);
        }

        private void approveComment(int intId)
        {
            XmlDocument config = Global.GetConfig();
            IContentService contentService = ApplicationContext.Current.Services.ContentService;
            IContent byId = contentService.GetById(intId);
            contentService.PublishWithStatus(byId);
            if (!string.IsNullOrEmpty(byId.GetValue<string>("smartBlogEmail")))
            {
                string str = "";
                str += "To jest wiadomo�� automatyczna, prosz� nie odpowiada�.";
                str += "<br />---------------------------------------------------<br />";
                str = str + "<br />Witaj, " + byId.GetValue<string>("smartBlogName");
                str = str + "<br /><br />Tw�j komentarz dotycz�cy " + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + " zosta� zatwierdzony.";
                string text = str;
                str = text + "<a href='" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + library.NiceUrl(Umbraco.Core.Models.ContentExtensions.Parent(byId).Id) + "'>Kliknij tutaj</a> �eby go zobaczy�.";
                str += "<br /><br />pozdrowienia,";
                str += "<br />Support WFDiF";
                if (sendApproveEmail)
                {
                    string innerText = config.GetElementsByTagName("moderatorCommentEmail")[0].InnerText;
                    string innerText2 = config.GetElementsByTagName("robotEmail")[0].InnerText;
                    Mailing.SendEmail(byId.GetValue<string>("smartBlogEmail"), innerText2, "Comment Approved", str, innerText);
                }
            }
        }

        //protected void DeleteSelected_Click(object sender, EventArgs e)
        //{
        //    foreach (int item in getSelected())
        //    {
        //        deleteComment(item);
        //    }
        //    showResults(null);
        //}

        //protected void ApproveSelected_Click(object sender, EventArgs e)
        //{
        //    foreach (int item in getSelected())
        //    {
        //        Log.Add(LogTypes.Error, User.GetUser(0), -1, "approving item " + item);
        //        approveComment(item);
        //    }
        //    showResults(null);
        //}

        public static object getPropertyValue(int intId, string strProperty)
        {
            IPublishedContent publishedContent = Global.objUmbHelper.TypedContent(intId);
            return publishedContent.GetProperty(strProperty).Value;

        }

        public static object getPost(int intId)
        {
            return Global.objUmbHelper.TypedContent(intId).GetProperty("smartBlogComment");
        }

        //private List<int> getSelected()
        //{
        //    List<int> list = new List<int>();
        //    foreach (RepeaterItem item in rptComments.Items)
        //    {
        //        CheckBox checkBox = (CheckBox)item.FindControl("checkbox");
        //        if (checkBox.Checked)
        //        {
        //            list.Add(Convert.ToInt32(((HiddenField)item.FindControl("CommentId")).Value));
        //        }
        //    }
        //    return list;
        //}

        internal static IRecordsReader GetAllDocuments(SearchTerm searchTerm)
        {
            ISqlHelper sqlHelper = umbraco.BusinessLogic.Application.SqlHelper;
            List<IParameter> list = new List<IParameter>();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("SELECT  umbracoNode.Id,umbracoNode.Text, isNull(CMSDocument.Published,0) as Published, DocType.Text as nodeTypeName  ");
            stringBuilder.Append("from umbracoNode ");
            stringBuilder.Append("Left Join cmsDocument ON umbracoNode.Id = cmsDocument.NodeId and Published = 1 ");
            stringBuilder.Append("INNER JOIN cmsContent ON umbracoNode.Id = cmsContent.NodeId ");
            stringBuilder.Append("INNER JOIN umbracoNode DocType ON cmsContent.contentType = DocType.Id and DocType.nodeObjectType = 'a2cb7800-f571-4787-9638-bc48539a0efb'  ");
            stringBuilder.Append("INNER JOIN cmsContentType ON cmsContent.contentType = cmsContentType.NodeId ");
            stringBuilder.Append("WHERE (umbracoNode.nodeObjectType = 'c66ba18e-eaf3-4cff-8a22-41b16d66a972')");
            stringBuilder.Append("AND umbracoNode.ParentId <> -20");
            if (searchTerm.DocumentType != string.Empty)
            {
                stringBuilder.Append(" And cmsContentType.alias = @Alias ");
                list.Add(sqlHelper.CreateParameter("Alias", searchTerm.DocumentType));
            }
            if (searchTerm.Published.HasValue)
            {
                stringBuilder.Append(" And isNull(CMSDocument.Published,0) = @Published");
                list.Add(sqlHelper.CreateParameter("Published", searchTerm.Published.Value));
            }
            var item = sqlHelper.ExecuteReader(stringBuilder.ToString(), list.ToArray());
            return item;
        }
    }
}
